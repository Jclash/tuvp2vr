// ConsoleApplication1.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <string>

#include <omp.h>

#define _USE_MATH_DEFINES
#include <math.h>

#include <sstream> // Required for stringstreams

//#include "time.h"

//#include <sys/time.h>
#include <ctime>
//#include <sys/resource.h>

#include "stdafx.h"

#include "init.h"

#ifdef GO_MPI == 1
//#include <mpi.h>
#endif




/*
int N;
int M;

int N2;
int M2;

double grho;
double gksi;
*/

double *Ur0, *Ur1, *Ur2, *Ut0, *Ut1, *Ut2, T;

//struct params{
//
//	int N;
//	int M;
//
//	double alpha;
//	double betta;
//	double R;
//	double epsilon;
//	double dense;
//	double mu;
//	double lambda;
//	double const_tau;
//	double const_ks0;
//	double const_T0;
//	double const_ce;
//	double const_k;
//	double const_alpha;
//
//	bool temperature_Newton;
//	bool temperature_Dirihle;
//	double const_gamma;
//
//	double T;
//
//	double tau;
//
//	double *Ur0, *Ur1, *Ur2, *Ut0, *Ut1, *Ut2;
//
//	double *r_i, *thetta_j;
//
//	int sfreq;
//
//	int N2;
//	int M2;
//
//	double grho;
//	double gksi;
//
//	double eps_der;
//
//};

params a;

//int init_params(params& par, int N = 25){
//
//	par.N = N;
//	par.M = N;
//
//	par.sfreq = 7;
//
//	par.alpha = M_PI / 4;
//	par.betta = M_PI / 6;
//	par.R = 1;
//	par.epsilon = 0.1;
//	par.dense = 1.0;
//	par.mu = 1.0;
//	par.lambda = 1.0;//2.0 / 3.0*par.mu;
//	par.const_tau = 1000.0;
//	par.const_ks0 = 0.5;
//	par.const_T0 = 300.0;
//	par.const_ce = 0.0001;
//	par.const_k = 0.0000001	;
//	par.const_alpha = 0.000001;
//
//	par.T = 1.0;
//
//	par.grho = (par.R - par.epsilon) / par.N;
//	par.gksi = (2 * M_PI - 2 * par.alpha) / par.M;
//
//	par.tau = par.grho / 10.0; // *par.grho / 2.0;
//
//	par.temperature_Newton = true;
//	par.temperature_Dirihle = false;
//	par.const_gamma = 1.0;
//
//	par.eps_der = 0.0;
//
//	//par.r_i = new double[par.N];
//	//par.thetta_j = new double[par.M];
//
//	return 0;
//}

int print_params(params& par){
	using namespace std;

	cout << "N = " << par.N << endl;
	cout << "M = " << par.M << endl;
	cout << "rho = " << par.grho << endl;
	cout << "ksi = " << par.gksi << endl;
	cout << "tau = " << par.tau << endl;

	cout << "Physical constants:" << endl;
	cout << "dense_phys = " << par.dense_phys << endl; // kg/m^3
	cout << "R_phys = " << par.R_phys << endl;
	cout << "mu_phys = " << par.mu_phys << endl;
	cout << "lambda_phys = " << par.lambda_phys << endl;
	cout << "const_ce_phys = " << par.const_ce_phys << endl;
	cout << "const_k_phys = " << par.const_k_phys << endl;
	cout << "const_alpha_phys = " << par.const_alpha_phys << endl;
	cout << "const_tau_phys = " << par.const_tau_phys << endl;
	cout << "xi_phys = " << par.xi_phys << endl;
	cout << "const_T0_phys = " << par.const_T0_phys << endl;
	cout << "const_ks0_phys = " << par.const_ks0_phys << endl;

	cout << "const_tau0_phys = " << par.const_tau0_phys << endl;
	cout << "const_Sigma10_phys = " << par.const_Sigma10_phys << endl;

	cout << "T_phys = " << par.T_phys << endl;

	cout << endl;
	cout << "Constants in calculation:" << endl;
	cout << "dense = " << par.dense << endl; // kg/m^3
	cout << "R = " << par.R << endl;
	cout << "mu = " << par.mu << endl;
	cout << "lambda = " << par.lambda << endl;
	cout << "const_ce = " << par.const_ce << endl;
	cout << "const_k = " << par.const_k << endl;
	cout << "const_alpha = " << par.const_alpha << endl;
	cout << "const_tau = " << par.const_tau << endl;
	cout << "xi = " << par.xi << endl;
	cout << "const_T0 = " << par.const_T0 << endl;
	cout << "const_ks0 = " << par.const_ks0 << endl;

	cout << "const_gamma = " << par.const_gamma << endl;
	cout << "const_tau0 = " << par.const_tau0 << endl;
	cout << "T = " << par.T << endl;

	cout << endl;

	return 0;
}

double diffclock(double clock1, double clock2)
{
	//return ((clock2-clock1)*1000.0)/CLOCKS_PER_SEC;
	return ((clock2-clock1))/(double)CLOCKS_PER_SEC;
	//return clock2 - clock1;
}


double mclock(){
	return clock();
	//timeval tim;
	//gettimeofday(&tim, NULL);
	//return tim.tv_sec + (tim.tv_usec / 1000000.0);
}


std::string itos(int number){
	std::ostringstream oss;
	oss << number;
	return oss.str();
}

double ctg(double x){ 

	double eps = 1.0E-10;

	if (x < eps)
		return 10000;
	else
	  return cos(x) / sin(x); 

};

double sinn(double x){

	double eps = 1.0E-10;

	if (abs(x) < eps)
		return 1.0/eps;
	else
		return sin(x);

};

int save_vector(double* A, int N, const char* fname){
	using namespace std;
	ofstream out(fname, ios::out);

	string fname_str = fname;

	if (!out.fail()){
		for (int i = 0; i < N; i++) out << A[i] << endl;
		cout << "file " << fname_str << " was successfully created" << endl;
	}
	else{
		cout << "error creating output file" << endl;
	}

	out.close();

	return 0;
}

int save_grafic_2d_on01(double* A, int N, const char* fname){
	using namespace std;
	ofstream out(fname, ios::out);

	string fname_str = fname;

	if (!out.fail()){
		for (int i = 0; i < N; i++) out << (1.0*i) / (N - 1) << " " << A[i] << endl;
		cout << "file " << fname_str << " was successfully created" << endl;
	}
	else{
		cout << "error creating output file" << endl;
	}

	out.close();

	return 0;
}

int save_grafic_3d_onD(double* D, double* A, int N, const char* fname){
	using namespace std;
	ofstream out(fname, ios::out);

	string fname_str = fname;
	double val;
	double eps = 0.000000001;

	if (!out.fail()){
		for (int i = 0; i < N; i++){
			val = A[i];
			//if ((val < eps)&&(val > -eps)) val = 0.0;
			out << D[2 * i] << " " << D[2 * i + 1] << " " << val << endl;
		}
		//cout << "file " << fname_str << " was successfully created" << endl;
	}
	else{
		cout << "error creating output file" << endl;
	}

	out.close();

	return 0;
}

int save_grafic_2d_onD(double* D, double* A, int N, const char* fname){
	using namespace std;
	ofstream out(fname, ios::out);

	string fname_str = fname;
	double val;
	double eps = 0.000000001;

	if (!out.fail()){
		for (int i = 0; i < N; i++){
			val = A[i];
			//if ((val < eps)&&(val > -eps)) val = 0.0;
			out << D[i] << " " << val << endl;
		}
		//cout << "file " << fname_str << " was successfully created" << endl;
	}
	else{
		cout << "error creating output file" << endl;
	}

	out.close();

	return 0;
}



int save_grafic_1val_onD(double* D, double* A, int N, const char* fname){
	using namespace std;
	ofstream out(fname, ios::out);

	string fname_str = fname;
	double val;
	double eps = 0.000000001;

	if (!out.fail()){
		for (int i = 0; i < N; i++){
			val = A[i];
			//if ((val < eps)&&(val > -eps)) val = 0.0;
			out << D[i] << " " << val << endl;
		}
		//cout << "file " << fname_str << " was successfully created" << endl;
	}
	else{
		cout << "error creating output file" << endl;
	}

	out.close();

	return 0;
}


int save_grafic_2val_onD(double* D, double* A, int N, const char* fname){
	using namespace std;
	ofstream out(fname, ios::out);

	string fname_str = fname;
	double val;
	double eps = 0.000000001;

	if (!out.fail()){
		for (int i = 0; i < N; i++){
			//val = A[i];
			//if ((val < eps)&&(val > -eps)) val = 0.0;
			out << D[i] << " " << A[i * 2] << " " << A[i*2+1] << endl;
		}
		//cout << "file " << fname_str << " was successfully created" << endl;
	}
	else{
		cout << "error creating output file" << endl;
	}

	out.close();

	return 0;
}



double Fmax(double* F, int N, int* a){
	double res = F[0];
	*a = 0;
	for (int i = 1; i < N; i++){
		if (F[i] > res){
			res = F[i];
			*a = i;
		}
	}

	return res;
}

int save_function_scalar( double* F, double* D, int size, const char* funname, int k, bool printinfo){
	using namespace std;

	string fname_str = "step" + itos(k) + funname + ".txt";
	save_grafic_3d_onD(D, F, size, fname_str.c_str());
	if (printinfo) cout << "file " << fname_str << " was successfully created" << endl;

	return 0;

}



inline double r(int i){

	return a.epsilon + i*a.grho;

	/*
	if (i <= N2) return epsilon + i*grho*grho;
	if (i >= N-N2) return epsilon + grho*grho*N2 + grho*(N-2*N2) + (i-N-N2)*grho*grho;
	return epsilon + N2*grho*grho + (i-N2)*grho;
	*/
}

inline double thetta(int j){

	return a.alpha + j*a.gksi;


	/*
	if (j <= M2) return alpha + j*gksi*gksi;
	if (j >= M-M2) return alpha + gksi*gksi*M2 + gksi*(M-2*M2) + (j-M-M2)*gksi*gksi;
	return alpha + M2*gksi*gksi + (j-M2)*gksi;
	*/
}

inline int trunc_r(double p){

	for (int i = 0; i <= a.N; i++)
	if (r(i) > p) return i - 1;

	return a.N;
}

inline int trunc_thetta(double p){

	for (int i = 0; i <= a.M; i++)
	if (thetta(i) > p) return i - 1;

	return a.M;
}

inline double rho(int i){
	return r(i + 1) - r(i);
}

inline double ksi(int j){
	return thetta(j + 1) - thetta(j);
}

inline double aijrd(int i, int j){
	int M = a.M;
	return (Ur1[(i + 1)*(M + 1) + j] - Ur1[i*(M + 1) + j]) / rho(i);
}

inline double aijtd(int i, int j){
	int M = a.M;
	return (Ur1[(i)*(M + 1) + j + 1] - Ur1[i*(M + 1) + j]) / ksi(j);
}

inline double bijrd(int i, int j){
	int M = a.M;
	return (Ut1[(i + 1)*(M + 1) + j] - Ut1[i*(M + 1) + j]) / rho(i);
}

inline double bijtd(int i, int j){
	int M = a.M;
	return (Ut1[(i)*(M + 1) + j + 1] - Ut1[i*(M + 1) + j]) / ksi(j);
}

inline double aijrs(int i, int j){
	int M = a.M;
	return (Ur1[(i)*(M + 1) + j] - Ur1[(i - 1)*(M + 1) + j]) / rho(i - 1);
}

inline double aijts(int i, int j){
	int M = a.M;
	return (Ur1[(i)*(M + 1) + j] - Ur1[i*(M + 1) + j - 1]) / ksi(j - 1);
}

inline double bijrs(int i, int j){
	int M = a.M;
	return (Ut1[(i)*(M + 1) + j] - Ut1[(i - 1)*(M + 1) + j]) / rho(i - 1);
}

inline double bijts(int i, int j){
	int M = a.M;
	return (Ut1[(i)*(M + 1) + j] - Ut1[i*(M + 1) + j - 1]) / ksi(j - 1);
}

inline double Add(int i, int j){
	int M = a.M;
	double mu = a.mu;
	double lambda = a.lambda;
	return r(i)*(2 * mu + lambda)*aijrd(i, j) + lambda*bijtd(i, j) + lambda*Ur1[(i)*(M + 1) + j];
}

inline double Asd(int i, int j){
	int M = a.M;
	double mu = a.mu;
	double lambda = a.lambda;
	return r(i)*(2 * mu + lambda)*aijrs(i, j) + lambda*bijtd(i, j) + lambda*Ur1[(i)*(M + 1) + j];
}

inline double Ads(int i, int j){
	int M = a.M;
	double mu = a.mu;
	double lambda = a.lambda;
	return r(i)*(2 * mu + lambda)*aijrd(i, j) + lambda*bijts(i, j) + lambda*Ur1[(i)*(M + 1) + j];
}

inline double Ass(int i, int j){
	int M = a.M;
	double mu = a.mu;
	double lambda = a.lambda;
	return r(i)*(2 * mu + lambda)*aijrs(i, j) + lambda*bijts(i, j) + lambda*Ur1[(i)*(M + 1) + j];
}


inline double Bdd(int i, int j){
	int M = a.M;
	double mu = a.mu;
	return mu*(1.0 / r(i)*aijtd(i, j) + bijrd(i, j) - Ut1[(i)*(M + 1) + j] / r(i));
}

inline double Bds(int i, int j){
	int M = a.M;
	double mu = a.mu;
	return mu*(1.0 / r(i)*aijtd(i, j) + bijrs(i, j) - Ut1[(i)*(M + 1) + j] / r(i));
}

inline double Bsd(int i, int j){
	int M = a.M;
	double mu = a.mu;
	return mu*(1.0 / r(i)*aijts(i, j) + bijrd(i, j) - Ut1[(i)*(M + 1) + j] / r(i));
}

inline double Bss(int i, int j){
	int M = a.M;
	double mu = a.mu;
	return mu*(1.0 / r(i)*aijts(i, j) + bijrs(i, j) - Ut1[(i)*(M + 1) + j] / r(i));
}

inline double Cdd(int i, int j){
	int M = a.M;
	double mu = a.mu;
	double lambda = a.lambda;
	return (2 * mu + lambda) / r(i)*(bijtd(i, j) + Ur1[(i)*(M + 1) + j]) + lambda*aijrd(i, j);
}

inline double Cds(int i, int j){
	int M = a.M;
	double mu = a.mu;
	double lambda = a.lambda;
	return (2 * mu + lambda) / r(i)*(bijtd(i, j) + Ur1[(i)*(M + 1) + j]) + lambda*aijrs(i, j);
}

inline double Csd(int i, int j){
	int M = a.M;
	double mu = a.mu;
	double lambda = a.lambda;
	return (2 * mu + lambda) / r(i)*(bijts(i, j) + Ur1[(i)*(M + 1) + j]) + lambda*aijrd(i, j);
}

inline double Css(int i, int j){
	int M = a.M;
	double mu = a.mu;
	double lambda = a.lambda;
	return (2 * mu + lambda) / r(i)*(bijts(i, j) + Ur1[(i)*(M + 1) + j]) + lambda*aijrs(i, j);
}

inline double S(int i, int j){
	int M = a.M;
	int N = a.N;

	if ((i > 0) && (i<N) && (j>0) && (j < M))
		return (rho(i - 1) + rho(i))*(ksi(j - 1) + ksi(j)) / 4;
	else if ((i == 0) && (j == 0))
		return rho(0)*ksi(0) / 4;
	else if ((i == N) && (j == 0))
		return rho(N - 1)*ksi(0) / 4;
	else if ((i == N) && (j == M))
		return rho(N - 1)*ksi(M - 1) / 4;
	else if ((i == 0) && (j == M))
		return rho(0)*ksi(M - 1) / 4;
	else if (i == N)
		return rho(N - 1)*(ksi(j - 1) + ksi(j)) / 4;
	else if (i == 0)
		return rho(0)*(ksi(j - 1) + ksi(j)) / 4;
	else if (j == 0)
		return (rho(i - 1) + rho(i))*ksi(0) / 4;
	else if (j == M)
		return (rho(i - 1) + rho(i))*ksi(M - 1) / 4;

	return (rho(1 - 1) + rho(1))*(ksi(j - 1) + ksi(j)) / 4;

	std::cout << "strange situation: i = " << i << ", j = " << j << std::endl;

}

inline double S(int i, int j, int i1, int j1){
	//return ((rho(i1 - 1) + rho(i1))*(ksi(j1 - 1) + ksi(j1))) / ((rho(i - 1) + rho(i))*(ksi(j - 1) + ksi(j)));
	return S(i1, j1) / S(i, j);
}

inline double Fr(int i, int j){ return 0.0; }

inline double Ft(int i, int j){ return 0.0; }

inline double Sigma(int j, int k){
	//return 1.0;
	if (k <= 1){
		return 1.0;
	}
	else{
		return 0.0;
	}
}

double absd(double x){
	if (x > 0) return x;
	else return -x;
}

int dF_dr(double* dF, double* F, params par){

	int N = par.N;
	int M = par.M;
	double eps = a.eps_der;

//#pragma omp parallel shared (dF,F) private (i)
	{
	#pragma omp parallel for
		for (int i = 0; i <= N; i++){

			//std::cout << "Hi i = " << i << std::endl;

			for (int j = 0; j <= M; j++){

				//if (absd(F[(i + 1)*(M + 1) + j]) >= eps){
					if ((i >= 1) && (i <= N - 1)){
						dF[i*(M + 1) + j] = (F[(i + 1)*(M + 1) + j] - F[(i - 1)*(M + 1) + j]) / par.grho/2.0;// (r(i + 1) - r(i - 1));
					}
					else if (i == 0){
						dF[i*(M + 1) + j] = (F[(i + 1)*(M + 1) + j] - F[(i)*(M + 1) + j]) / par.grho;//(r(1) - r(0));
					}
					else if (i == N){
						dF[i*(M + 1) + j] = (F[(i)*(M + 1) + j] - F[(i - 1)*(M + 1) + j]) / par.grho;//(r(N) - r(N - 1));
					}
				//}
				//else{
				//	dF[i*(M + 1) + j] = 0.0;
				//}

			}
		}
	}

	//std::cout << "Hi" << std::endl;

	return 0;
}

int dF_dt(double* dF, double* F, params par){

	int N = par.N;
	int M = par.M;
	double eps = par.eps_der;

#pragma omp parallel for
		for (int i = 0; i <= N; i++){
			for (int j = 0; j <= M; j++){

				//if (absd(F[(i)*(M + 1) + (j + 1)]) >= eps){
					if ((j >= 1) && (j <= M - 1)){
						dF[i*(M + 1) + j] = (F[(i)*(M + 1) + (j + 1)] - F[(i)*(M + 1) + (j - 1)]) / (thetta(i + 1) - thetta(i - 1));
					}
					else if (j == 0){
						dF[i*(M + 1) + j] = (F[(i)*(M + 1) + j + 1] - F[(i)*(M + 1) + j]) / (thetta(1) - thetta(0));
					}
					else if (j == M){
						dF[i*(M + 1) + j] = (F[(i)*(M + 1) + j] - F[(i)*(M + 1) + (j - 1)]) / (thetta(M) - thetta(M - 1));
					} 
				//}
				//else{
				//	dF[i*(M + 1) + j] = 0.0;
				//}

			}
		}
	

	return 0;
}


int elastcyl_2d(int mode){
	using namespace std;

	double *D, *Vr0, *Vt0;

	int myrank = 0, np = 1;

#if GO_MPI == 1
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	MPI_Comm_size(MPI_COMM_WORLD, &np);
#endif

	bool savesolution = false;
	bool printinfo = true;
	bool saveUrUt = false;
	bool saveTensors = false;
	bool saveInvariants = true;

	if (mode == EVERYDAY){
		savesolution = true;
		printinfo = true;
	}

	//params a;
	//init_params(a);
	if ((myrank == 0) && printinfo) print_params(a);

	double alpha = a.alpha;
	double betta = a.betta;
	double R = a.R;
	double epsilon = a.epsilon;
	double dense = a.dense;
	double mu = a.mu;
	double lambda = a.lambda;

	double T = a.T;

	int N = a.N;
	int M = a.M;

	double tau = a.tau;

	double dl1 = (M_PI - betta);
	double dl2 = (M_PI + betta);

	int sfreq = a.sfreq;

	//int l1 = (dl1 - grho*grho*N2)/grho + N2;
	//int l2 = (dl2 - grho*grho*N2)/grho + N2;

	//double l1 = (M_PI - betta - alpha) / gksi;
	//double l2 = (M_PI + betta - alpha) / gksi;

	int l1 = trunc_thetta(dl1);
	int l2 = trunc_thetta(dl2);

	if ((myrank == 0) && printinfo) cout << "l1 = " << l1 << endl;
	if ((myrank == 0) && printinfo) cout << "l2 = " << l2 << endl;

	D = new double[2 * (N + 1)*(M + 1)];
	Vr0 = new double[(N + 1)*(M + 1)];
	Vt0 = new double[(N + 1)*(M + 1)];
	Ur0 = new double[(N + 1)*(M + 1)];
	Ur1 = new double[(N + 1)*(M + 1)];
	Ur2 = new double[(N + 1)*(M + 1)];
	Ut0 = new double[(N + 1)*(M + 1)];
	Ut1 = new double[(N + 1)*(M + 1)];
	Ut2 = new double[(N + 1)*(M + 1)];

	double *Ur2test = new double[(N + 1)*(M + 1)];
	double *Ut2test = new double[(N + 1)*(M + 1)];


	double *Ur_dr = new double[(N + 1)*(M + 1)];
	double *Ur_dt = new double[(N + 1)*(M + 1)];
	double *Ut_dr = new double[(N + 1)*(M + 1)];
	double *Ut_dt = new double[(N + 1)*(M + 1)];
	double *Sigma_rr = new double[(N + 1)*(M + 1)];
	double *Sigma_tt = new double[(N + 1)*(M + 1)];
	double *Sigma_rt = new double[(N + 1)*(M + 1)];
	double *I1 = new double[(N + 1)*(M + 1)];
	double *Sigma_e = new double[(N + 1)*(M + 1)];





	// Domain
	for (int i = 0; i <= N; i++){
		for (int j = 0; j <= M; j++){
			D[(i*(M + 1) + j) * 2] = r(i);
			D[(i*(M + 1) + j) * 2 + 1] = thetta(j);
			//D[(i*(M + 1) + j) * 2] = i*grho + epsilon;
			//D[(i*(M + 1) + j) * 2 + 1] = j*gksi + alpha;
		}
	}


	// Initial conditions U0 = 0
	/*for (int i = 0; i < (M + 1)*(N + 1); i++){
	Ur0[i] = 0.0;
	Ut0[i] = 0.0;
	}*/
	for (int i = 0; i <= N; i++){
		for (int j = 0; j <= M; j++){
			Ur0[i*(M + 1) + j] = 0.0;// 0.001*cos(M_PI*(r(i) - epsilon) / (R - epsilon));
			Ut0[i*(M + 1) + j] = 0.0;
		}
	}


	// Initial conditions V0 = 0
	for (int i = 0; i < (M + 1)*(N + 1); i++){
		Vr0[i] = 0.0;
		Vt0[i] = 0.0;
	}


	/*	for (int i = 0; i < (M + 1)*(N + 1); i++){
	Ut1[i] = 0.0;
	Ur1[i] = 0.0;
	if (((i - (M + 1)*N) >= l1) && ((i - (M + 1)*N) <= l2))
	Ur1[i] = 0.0;
	else if (((i - (M + 1)*(N - 1)) >= l1) && ((i - (M + 1)*(N - 1)) <= l2))
	Ur1[i] = 0.0;
	else
	Ur1[i] = 0.0;
	}*/

	if ((myrank == 0) && savesolution){
		save_grafic_3d_onD(D, Ur0, (M + 1)*(N + 1), "step0r.txt");
		save_grafic_3d_onD(D, Ut0, (M + 1)*(N + 1), "step0t.txt");


		dF_dr(Ur_dr, Ur0, a);
		dF_dt(Ur_dt, Ur0, a);
		dF_dr(Ut_dr, Ut0, a);
		dF_dt(Ut_dt, Ut0, a);

		for (int i = 0; i < (N + 1)*(M + 1); i++)
			Sigma_rr[i] = (2 * mu + lambda)*Ur_dr[i] + lambda / r((int)(i*1.0 / (M + 1)))*(Ut_dt[i] + Ur0[i]);

		for (int i = 0; i < (N + 1)*(M + 1); i++)
			Sigma_tt[i] = (2 * mu + lambda) / r((int)(i*1.0 / (M + 1)))*(Ut_dt[i] + Ur0[i]) + lambda*Ur_dr[i];

		for (int i = 0; i < (N + 1)*(M + 1); i++)
			I1[i] = Sigma_rr[i] + Sigma_tt[i];

		for (int i = 0; i < (N + 1)*(M + 1); i++)
			Sigma_e[i] = absd(Sigma_rr[i] - Sigma_tt[i]);


		// saving Ur_dr
		//string fname_str_r_dr = "step" + itos(k) + "r_dr.txt";
		//save_grafic_3d_onD(D, Ur_dr, (M + 1)*(N + 1), fname_str_r_dr.c_str());
		//if ((myrank == 0) && printinfo) cout << "file " << fname_str_r_dr << " was successfully created" << endl;

		// saving Ur_dt
		//string fname_str_r_dt = "step" + itos(k) + "r_dt.txt";
		//save_grafic_3d_onD(D, Ur_dt, (M + 1)*(N + 1), fname_str_r_dt.c_str());
		//if ((myrank == 0) && printinfo) cout << "file " << fname_str_r_dt << " was successfully created" << endl;

		// saving Ut_dr
		//string fname_str_t_dr = "step" + itos(k) + "t_dr.txt";
		//save_grafic_3d_onD(D, Ut_dr, (M + 1)*(N + 1), fname_str_t_dr.c_str());
		//if ((myrank == 0) && printinfo) cout << "file " << fname_str_t_dr << " was successfully created" << endl;

		// saving Ut_dt
		//string fname_str_t_dt = "step" + itos(k) + "t_dt.txt";
		//save_grafic_3d_onD(D, Ut_dt, (M + 1)*(N + 1), fname_str_t_dt.c_str());
		//if ((myrank == 0) && printinfo) cout << "file " << fname_str_t_dt << " was successfully created" << endl;

		if (saveTensors){
			// saving Sigma_rr
			string fname_str_sigma_rr = "step" + itos(0) + "sigma_rr.txt";
			save_grafic_3d_onD(D, Sigma_rr, (M + 1)*(N + 1), fname_str_sigma_rr.c_str());
			if ((myrank == 0) && printinfo) cout << "file " << fname_str_sigma_rr << " was successfully created" << endl;

			// saving Sigma_tt
			string fname_str_sigma_tt = "step" + itos(0) + "sigma_tt.txt";
			save_grafic_3d_onD(D, Sigma_tt, (M + 1)*(N + 1), fname_str_sigma_tt.c_str());
			if ((myrank == 0) && printinfo) cout << "file " << fname_str_sigma_tt << " was successfully created" << endl;
		}


		if (saveInvariants){

			// saving I1
			string fname_str_I1 = "step" + itos(0) + "I1.txt";
			save_grafic_3d_onD(D, I1, (M + 1)*(N + 1), fname_str_I1.c_str());
			if ((myrank == 0) && printinfo) cout << "file " << fname_str_I1 << " was successfully created" << endl;

			// saving Sigma_e
			string fname_str_Sigma_e = "step" + itos(0) + "sigma_e.txt";
			save_grafic_3d_onD(D, Sigma_e, (M + 1)*(N + 1), fname_str_Sigma_e.c_str());
			if ((myrank == 0) && printinfo) cout << "file " << fname_str_Sigma_e << " was successfully created" << endl;

		}


	}

	//save_grafic_3d_onD(D, Ur1, (M + 1)*(N + 1), "step1r.txt");
	//save_grafic_3d_onD(D, Ut1, (M + 1)*(N + 1), "step1t.txt");

	for (int i = 0; i < (M + 1)*(N + 1); i++){
		Ur2[i] = 0.0;
		Ut2[i] = 0.0;
	}

	for (int i = 0; i < (M + 1)*(N + 1); i++){
		Ur1[i] = Ur0[i];
		Ut1[i] = Ut0[i];
	}


	// Main loop
	//	double ri, rip1, rim1, rim2;
	//	double thetta_j, thetta_jp1, thetta_jm1, thetta_jm2;
	//	double rhoi, rhoim1, rhoim2;
	//	double ksij, ksijm1, ksijm2;
	//	int p = i0;
	//	int l = i1;
	double timesumr, timesumt, timemul;
	int k = 1;
	double* tempU;
	double eps = 0.0;


	//	double aijrd, aijtd, bijrd, bijtd, aijrs, aijts, bijrs, bijts, aim1jrd, bim1jtd, aim1jtd, bim1jrd, bijm1td, aijm1rd, aijm1td, bijm1rd;
	//	double Addij, Addim1j, Bddij, Bddim1j, Bddijm1, Cddij, Cddijm1, Sijim1j, Sijijm1, Sij, Sijm1, Sim1j;
	//	double Frij, Ftij;

	int L = 1;
	int s = 1;
	int seq_iters_atstart = 2;
	int seq_iters_atfinish = T / tau - seq_iters_atstart;
	bool paral = false;

	int *rcs, *dls;
	int *recvcounts, *displs;

	rcs = new int[np];
	dls = new int[np];

	recvcounts = new int[np];
	displs = new int[np];


	for (int i = 0; i < np; i++)
		dls[i] = N*(i*1.0 / np);

	for (int i = 0; i < np - 1; i++)
		rcs[i] = dls[i + 1] - dls[i];
	rcs[np - 1] = N + 1 - dls[np - 1];


	//int ib = N*(myrank*1.0 / np);
	int ib = dls[myrank];
	int ie = rcs[myrank] + dls[myrank] - 1;
	//int ie = N*((myrank + 1.0) / np);
	//if ( myrank > 0 ) ie--;

	for (int i = 0; i < np; i++){
		displs[i] = dls[i] * (M + 1);
		recvcounts[i] = rcs[i] * (M + 1);
	}


	int i1, i2;
	int cs = s + 1;

	//cout << "myrank = " << myrank << " ib = " << ib << "; ie = " << ie << endl;
	//cout << "myrank = " << myrank << " recvcounts = " << recvcounts[myrank] << "; displs = " << displs[myrank] << endl;


	for (double t = 2 * tau; t < T; t += tau){

		if (k > seq_iters_atstart){

			cs--;
			if (cs == 0) cs = s;

			i1 = ib;
			if (myrank > 0)
				i1 -= (s - 1)*L;

			i2 = ie;
			if (myrank < np - 1)
				i2 += (s - 1)*L;

#if GO_MPI == 1
			if (np > 1) paral = true;
#endif

		}
		else{
			i1 = 0;
			i2 = N;
			paral = false;
		}

		for (int i = i1; i <= i2; i++){
			for (int j = 0; j <= M; j++){

				if (k == 1){
					timesumr = (tau*Vr0[i*(M + 1) + j]) + Ur1[i*(M + 1) + j];
					timesumt = (tau*Vt0[i*(M + 1) + j]) + Ut1[i*(M + 1) + j];
					timemul = tau*tau / 2;
				}
				else{
					timesumr = -Ur0[i*(M + 1) + j] + (2 * Ur1[i*(M + 1) + j]);
					timesumt = -Ut0[i*(M + 1) + j] + (2 * Ut1[i*(M + 1) + j]);
					timemul = tau*tau;
				}


				// Points N1
				if (((i >= 1) && (i <= N - 2)) && ((j >= 1) && (j <= M - 2))){

					Ur2[i*(M + 1) + j] = timesumr
						+ timemul*((
						+Add(i, j) / rho(i)
						- Add(i - 1, j)*S(i, j, i - 1, j) / rho(i - 1)
						+ Bdd(i, j) / ksi(j)
						- Bdd(i, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						- Cdd(i, j)) / (r(i)*dense)
						+ Fr(i, j));

					Ut2[i*(M + 1) + j] = timesumt
						+ timemul*((
						+r(i)*Bdd(i, j) / rho(i)
						- r(i - 1)*Bdd(i - 1, j)*S(i, j, i - 1, j) / rho(i - 1)
						+ Cdd(i, j) / ksi(j)
						- Cdd(i, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						+ Bdd(i, j)) / (r(i)*dense)
						+ Ft(i, j));

				}

				// Points N2, N13
				if ((i == N) && (((j >= 1) && (j <= l1 - 1)) || ((j >= l2 + 1) && (j <= M - 2)))){

					Ur2[i*(M + 1) + j] = timesumr
						+ timemul*((
						-Asd(N, j) / rho(N - 1)
						- Add(i - 1, j)*S(i, j, i - 1, j) / rho(i - 1)
						+ Bds(i, j) / ksi(j)
						- Bds(i, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						- Cds(i, j)) / (r(i)*dense)
						+ Fr(i, j));

					Ut2[i*(M + 1) + j] = timesumt
						+ timemul*((
						-r(i)*Bds(i, j) / rho(N - 1)
						- r(i - 1)*Bdd(i - 1, j)*S(i, j, i - 1, j) / rho(i - 1)
						+ Cds(i, j) / ksi(j)
						- Cds(i, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						+ Bds(i, j)) / (r(i)*dense)
						+ Ft(i, j));

				}

				// Points N3, N14
				if ((i == N - 1) && (j >= 1) && (j <= M - 2)){

					Ur2[i*(M + 1) + j] = timesumr
						+ timemul*((
						+Add(i, j) / rho(i)
						- Add(N - 2, j)*S(N - 1, j, N - 2, j) / rho(N - 2)
						+ Asd(N, j)*S(N - 1, j, N, j) / rho(N - 1)
						+ Bdd(N - 1, j) / ksi(j)
						- Bdd(N - 1, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						- Cdd(N - 1, j)) / (r(N - 1)*dense)
						+ Fr(i, j));

					Ut2[i*(M + 1) + j] = timesumt
						+ timemul*((
						+r(N - 1)*Bdd(N - 1, j) / rho(i)
						- r(N - 2)*Bdd(N - 2, j)*S(N - 1, j, N - 2, j) / rho(N - 2)
						+ r(N)*Bds(N, j)*S(i, j, N, j) / rho(N - 1)
						+ Cdd(i, j) / ksi(j)
						- Cdd(N - 1, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						+ Bdd(i, j)) / (r(N - 1)*dense)
						+ Ft(i, j));

				}

				// Points N4
				if ((i == 0) && (j >= 2) && (j <= M - 2)){

					Ur2[i*(M + 1) + j] = timesumr
						+ timemul*((
						+Add(i, j) / rho(i)
						+ Bdd(i, j) / ksi(j)
						- Bdd(0, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						- Cdd(i, j)) / (r(i)*dense)
						+ Fr(i, j));

					Ut2[i*(M + 1) + j] = timesumt
						+ timemul*((
						+r(i)*Bdd(i, j) / rho(i)
						+ Cdd(i, j) / ksi(j)
						- Cdd(i, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						+ Bdd(i, j)) / (r(i)*dense)
						+ Ft(i, j));
				}

				// Points N5
				if ((i >= 2) && (i <= N - 1) && (j == 0)){

					Ur2[i*(M + 1) + j] = timesumr
						+ timemul*((
						+Add(i, j) / rho(i)
						- Add(i - 1, j)*S(i, j, i - 1, j) / rho(i - 1)
						+ Bdd(i, j) / ksi(0)
						- Cdd(i, j)) / (r(i)*dense)
						+ Fr(i, j));

					Ut2[i*(M + 1) + j] = timesumt
						+ timemul*((
						+r(i)*Bdd(i, j) / rho(i)
						- r(i - 1)*Bdd(i - 1, j)*S(i, j, i - 1, j) / rho(i - 1)
						+ Cdd(i, j) / ksi(0)
						+ Bdd(i, j)) / (r(i)*dense)
						+ Ft(i, j));
				}

				// Points N6, N20
				if ((i >= 1) && (i <= N - 2) && (j == M)){

					Ur2[i*(M + 1) + j] = timesumr
						+ timemul*((
						+Ads(i, j) / rho(i)
						- Ads(i - 1, j)*S(i, j, i - 1, j) / rho(i - 1)
						- Bsd(i, j) / ksi(M - 1)
						- Bdd(i, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						- Csd(i, j)) / (r(i)*dense)
						+ Fr(i, j));

					Ut2[i*(M + 1) + j] = timesumt
						+ timemul*((
						+r(i)*Bsd(i, j) / rho(i)
						- r(i - 1)*Bsd(i - 1, j)*S(i, j, i - 1, j) / rho(i - 1)
						- Csd(i, j) / ksi(M - 1)
						- Cdd(i, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						+ Bsd(i, j)) / (r(i)*dense)
						+ Ft(i, j));

				}

				// Points N7, N21
				if ((i >= 1) && (i <= N - 2) && (j == M - 1)){

					Ur2[i*(M + 1) + j] = timesumr
						+ timemul*((
						+Add(i, j) / rho(i)
						- Add(i - 1, j)*S(i, j, i - 1, j) / rho(i - 1)
						+ Bdd(i, j) / ksi(M - 1)
						- Bdd(i, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						+ Bsd(i, M)*S(i, j, i, M) / ksi(j)
						- Cdd(i, j)) / (r(i)*dense)
						+ Fr(i, j));

					Ut2[i*(M + 1) + j] = timesumt
						+ timemul*((
						+r(i)*Bdd(i, j) / rho(i)
						- r(i - 1)*Bdd(i - 1, j)*S(i, j, i - 1, j) / rho(i - 1)
						+ Cdd(i, j) / ksi(M - 1)
						- Cdd(i, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						+ Csd(i, M)*S(i, j, i, j + 1) / ksi(j)
						+ Bdd(i, j)) / (r(i)*dense)
						+ Ft(i, j));

				}

				// Points N8
				if ((i == 0) && (j == 0)){

					Ur2[i*(M + 1) + j] = timesumr
						+ timemul*((
						+Add(i, j) / rho(i)
						+ Bdd(i, j) / ksi(0)
						- Cdd(i, j)) / (r(i)*dense)
						+ Fr(i, j));

					Ut2[i*(M + 1) + j] = timesumt
						+ timemul*((
						+r(i)*Bdd(i, j) / rho(i)
						+ Cdd(i, j) / ksi(0)
						+ Bdd(i, j)) / (r(i)*dense)
						+ Ft(i, j));

				}

				// Points N9
				if ((i == 1) && (j == 0)){

					Ur2[i*(M + 1) + j] = timesumr
						+ timemul*((
						+Add(i, j) / rho(i)
						- Add(i - 1, j)*S(i, j, i - 1, j) / rho(i - 1)
						+ Bdd(i, j) / ksi(0)
						- Cdd(i, j)) / (r(i)*dense)
						+ Fr(i, j));

					Ut2[i*(M + 1) + j] = timesumt
						+ timemul*((
						+r(i)*Bdd(i, j) / rho(i)
						- r(i - 1)*Bdd(i - 1, j)*S(i, j, i - 1, j) / rho(i - 1)
						+ Cdd(i, j) / ksi(0)
						+ Bdd(i, j)) / (r(i)*dense)
						+ Ft(i, j));

				}

				// Points N10
				if ((i == 0) && (j == 1)){

					Ur2[i*(M + 1) + j] = timesumr
						+ timemul*((
						+Add(i, j) / rho(i)
						+ Bdd(i, j) / ksi(1)
						- Bdd(i, j - 1)*S(i, j, 0, 0) / ksi(0)
						- Cdd(i, j)) / (r(i)*dense)
						+ Fr(i, j));

					Ut2[i*(M + 1) + j] = timesumt
						+ timemul*((
						+r(0)*Bdd(i, j) / rho(i)
						+ Cdd(i, j) / ksi(1)
						- Cdd(i, 0)*S(i, j, i, 0) / ksi(0)
						+ Bdd(i, j)) / (r(i)*dense)
						+ Ft(i, j));

				}

				// Points N11
				if ((i == 1) && (j == 1)){

					Ur2[i*(M + 1) + j] = timesumr
						+ timemul*((
						+Add(i, j) / rho(i)
						- Add(i - 1, j)*S(i, j, i - 1, j) / rho(i - 1)
						+ Bdd(i, j) / ksi(j)
						- Bdd(i, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						- Cdd(i, j)) / (r(i)*dense)
						+ Fr(i, j));

					Ut2[i*(M + 1) + j] = timesumt
						+ timemul*((
						+r(i)*Bdd(i, j) / rho(i)
						- r(i - 1)*Bdd(i - 1, j)*S(i, j, i - 1, j) / rho(i - 1)
						+ Cdd(i, j) / ksi(j)
						- Cdd(i, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						+ Bdd(i, j)) / (r(i)*dense)
						+ Ft(i, j));

				}

				// Points N12
				if ((i == N) && (j == 0)){

					Ur2[i*(M + 1) + j] = timesumr
						+ timemul*((
						-Asd(N, 0) / rho(N - 1)
						- Add(N - 1, 0)*S(i, 0, i - 1, j) / rho(i - 1)
						+ Bds(i, j) / ksi(j)
						- Cds(i, j)) / (r(i)*dense)
						+ Fr(i, j));

					Ut2[i*(M + 1) + j] = timesumt
						+ timemul*((
						-r(i)*Bds(i, j) / rho(i - 1)
						- r(i - 1)*Bdd(i - 1, j)*S(i, j, i - 1, j) / rho(i - 1)
						+ Cds(i, j) / ksi(j)
						+ Bds(i, j)) / (r(i)*dense)
						+ Ft(i, j));

				}

				// Points N15
				if ((i == N) && (j == M)){

					Ur2[i*(M + 1) + j] = timesumr
						+ timemul*((
						-Ass(i, j) / rho(i - 1)
						- Ads(i - 1, j)*S(i, j, i - 1, j) / rho(i - 1)
						- Bss(i, j) / ksi(j - 1)
						- Bds(i, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						- Css(i, j)) / (r(i)*dense)
						+ Fr(i, j));

					Ut2[i*(M + 1) + j] = timesumt
						+ timemul*((
						-r(i)*Bss(i, j) / rho(i - 1)
						- r(i - 1)*Bsd(i - 1, j)*S(i, j, i - 1, j) / rho(i - 1)
						- Css(i, j) / ksi(j - 1)
						- Cds(i, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						+ Bss(i, j)) / (r(i)*dense)
						+ Ft(i, j));

				}

				// Point N16
				if ((i == N - 1) && (j == M)){

					Ur2[i*(M + 1) + j] = timesumr
						+ timemul*((
						+Ads(i, j) / rho(i)
						- Ads(i - 1, j)*S(i, j, i - 1, j) / rho(i - 1)
						+ Ass(i + 1, j)*S(i, j, i + 1, j) / rho(i)
						- Bsd(i, j) / ksi(j - 1)
						- Bdd(i, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						- Csd(i, j)) / (r(i)*dense)
						+ Fr(i, j));

					Ut2[i*(M + 1) + j] = timesumt
						+ timemul*((
						+r(i)*Bsd(i, j) / rho(i)
						- r(i - 1)*Bsd(i - 1, j)*S(i, j, i - 1, j) / rho(i - 1)
						+ Bss(i + 1, j)*S(i, j, i + 1, j) / rho(i)
						- Csd(i, j) / ksi(j - 1)
						- Cdd(i, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						+ Bsd(i, j)) / (r(i)*dense)
						+ Ft(i, j));

				}

				// Points N17
				if ((i == N - 1) && (j == M - 1)){

					Ur2[i*(M + 1) + j] = timesumr
						+ timemul*((
						+Add(i, j) / rho(i)
						- Add(i - 1, j)*S(i, j, i - 1, j) / rho(i - 1)
						+ Asd(i + 1, j)*S(i, j, i + 1, j) / rho(i)
						+ Bdd(i, j) / ksi(j)
						- Bdd(i, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						+ Bsd(i, j + 1)*S(i, j, i, j + 1) / ksi(j)
						- Cdd(i, j)) / (r(i)*dense)
						+ Fr(i, j));

					Ut2[i*(M + 1) + j] = timesumt
						+ timemul*((
						+r(i)*Bdd(i, j) / rho(i)
						- r(i - 1)*Bdd(i - 1, j)*S(i, j, i - 1, j) / rho(i - 1)
						+ Bds(i + 1, j)*S(i, j, i + 1, j) / rho(i)
						+ Cdd(i, j) / ksi(j)
						- Cdd(i, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						+ Csd(i, j + 1)*S(i, j, i, j + 1) / ksi(j)
						+ Bdd(i, j)) / (r(i)*dense)
						+ Ft(i, j));

				}

				// Points N18
				if ((i == 0) && (j == M)){

					Ur2[i*(M + 1) + j] = timesumr
						+ timemul*((
						+Ads(i, j) / rho(i)
						- Bsd(i, j) / ksi(j - 1)
						- Bdd(i, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						- Csd(i, j)) / (r(i)*dense)
						+ Fr(i, j));

					Ut2[i*(M + 1) + j] = timesumt
						+ timemul*((
						+r(i)*Bsd(i, j) / rho(i)
						- Csd(i, j) / ksi(j - 1)
						- Cdd(i, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						+ Bsd(i, j)) / (r(i)*dense)
						+ Ft(i, j));

				}

				// Points N19
				if ((i == 0) && (j == M - 1)){

					Ur2[i*(M + 1) + j] = timesumr
						+ timemul*((
						+Add(i, j) / rho(i)
						+ Bdd(i, j) / ksi(j)
						- Bdd(i, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						+ Bsd(i, j + 1)*S(i, j, i, j + 1) / ksi(j)
						- Cdd(i, j)) / (r(i)*dense)
						+ Fr(i, j));

					Ut2[i*(M + 1) + j] = timesumt
						+ timemul*((
						+r(i)*Bdd(i, j) / rho(i)
						+ Cdd(i, j) / ksi(j)
						- Cdd(i, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						+ Csd(i, j + 1)*S(i, j, i, j + 1) / ksi(j)
						+ Bdd(i, j)) / (r(i)*dense)
						+ Ft(i, j));

				}

				// Points N22 - with a force on the boundary
				if ((i == N) && (j >= l1) && (j <= l2)){

					Ur2[i*(M + 1) + j] = timesumr
						+ timemul*((
						-Asd(N, j) / rho(N - 1)
						- Add(N - 1, j)*S(N, j, N - 1, j) / rho(N - 1)
						+ Bds(N, j) / ksi(j)
						- Bds(N, j - 1)*S(N, j, N, j - 1) / ksi(j - 1)
						- Cds(N, j)) / (r(N)*dense)
						+ Sigma(j, k)*(ksi(j) + ksi(j - 1)) / (2 * S(N, j)*dense)
						+ Fr(N, j));

					Ut2[i*(M + 1) + j] = timesumt
						+ timemul*((
						-r(N)*Bds(N, j) / rho(N - 1)
						- r(N - 1)*Bdd(N - 1, j)*S(N, j, N - 1, j) / rho(N - 1)
						+ Cds(N, j) / ksi(j)
						- Cds(N, j - 1)*S(N, j, N, j - 1) / ksi(j - 1)
						+ Bds(N, j)) / (r(N)*dense)
						+ Ft(N, j));

					// Points N23
					if ((i == N) && (j == M - 1)){

						Ur2[i*(M + 1) + j] = timesumr
							+ timemul*((
							-Asd(N, j) / rho(N - 1)
							- Add(N - 1, j)*S(N, j, N - 1, j) / rho(N - 1)
							+ Bds(N, j) / ksi(j)
							- Bds(N, j - 1)*S(N, j, N, j - 1) / ksi(j - 1)
							+ Bss(N, j + 1)*S(N, j, N, j + 1) / ksi(j)
							- Cds(N, j)) / (r(N)*dense)
							+ Fr(N, j));

						Ut2[i*(M + 1) + j] = timesumt
							+ timemul*((
							-r(N - 1)*Bdd(N - 1, j)*S(N, j, N - 1, j) / rho(N - 1)
							- r(N)*Bds(N, j) / rho(N - 1)
							- Cds(N, j - 1)*S(N, j, N, j - 1) / ksi(j - 1)
							+ Cds(N, j) / ksi(j)
							+ Css(N, j + 1)*S(N, j, N, j + 1) / ksi(j)
							+ Bds(N, j)) / (r(N)*dense)
							+ Ft(N, j));

					}
				}
			}
		}

		//cout << "thettat at 10142: " << Ut2[10142] << endl;

		/*
		for (int i = 0; i < (M + 1)*(N + 1); i++){
		if ((Ur2[i] > -eps) && (Ur2[i] < eps) || (isnan(Ur2[i]) == true)) Ur2[i] = 0.0;
		if ((Ut2[i] > -eps) && (Ut2[i] < eps) || (isnan(Ut2[i]) == true)) Ut2[i] = 0.0;
		}
		*/

		//double tempUr00 = Ur2[0];
		//Ur2[0] = 0.1;

		//cout << "k = " << k << endl;

		int sfreq_loc = sfreq;

		//if ((t>0.6-epsilon-5*tau-rho(1))&&(t<0.6-epsilon+5*tau))){
		//if ( (t>0.55) && (t<0.65) || (t < 0.05)){
		//	sfreq_loc = sfreq*10;
		//}

		if (savesolution && (((int)(t / tau)) % ((int)(T / tau / (sfreq_loc * T))) == 0)){

			if (myrank == 0) cout << (int)(t / T * 100 + 1) << "%  t = " << t << endl;

			// Saving solution to files
#if GO_MPI == 1

			MPI_Gatherv(Ur2 + displs[myrank], recvcounts[myrank], MPI_DOUBLE, Ur2test, recvcounts, displs, MPI_DOUBLE, 0, MPI_COMM_WORLD);
			if ((myrank == 0) && saveUrUt){
				string fname_str_r_mpi = "step" + itos(k) + "r_mpi.txt";
				save_grafic_3d_onD(D, Ur2test, (M + 1)*(N + 1), fname_str_r_mpi.c_str());
				if (printinfo) cout << "file " << fname_str_r_mpi << " was successfully created" << endl;
			}

			MPI_Gatherv(Ut2 + displs[myrank], recvcounts[myrank], MPI_DOUBLE, Ut2test, recvcounts, displs, MPI_DOUBLE, 0, MPI_COMM_WORLD);
			if ((myrank == 0) && saveUrUt){
				string fname_str_t_mpi = "step" + itos(k) + "t_mpi.txt";
				save_grafic_3d_onD(D, Ut2test, (M + 1)*(N + 1), fname_str_t_mpi.c_str());
				if (printinfo) cout << "file " << fname_str_t_mpi << " was successfully created" << endl;
			}

#else
			if (saveUrUt){

				if ((myrank == 0) && saveUrUt){
					string fname_str_r = "step" + itos(k) + "r.txt";
					save_grafic_3d_onD(D, Ur2, (M + 1)*(N + 1), fname_str_r.c_str());
					if (printinfo) cout << "file " << fname_str_r << " was successfully created" << endl;
				}


				if ((myrank == 0) && saveUrUt){
					string fname_str_t = "step" + itos(k) + "t.txt";
					save_grafic_3d_onD(D, Ut2, (M + 1)*(N + 1), fname_str_t.c_str());
					if (printinfo) cout << "file " << fname_str_t << " was successfully created" << endl;
				}
			}

#endif


			if (myrank == 0){
#if GO_MPI == 0
				Ur2test = Ur2; // Only without MPI
				Ut2test = Ut2; // Only without MPI
#endif		

				dF_dr(Ur_dr, Ur2test, a);
				dF_dt(Ur_dt, Ur2test, a);
				dF_dr(Ut_dr, Ut2test, a);
				dF_dt(Ut_dt, Ut2test, a);

				for (int i = 0; i < (N + 1)*(M + 1); i++)
					Sigma_rr[i] = (2 * mu + lambda)*Ur_dr[i] + lambda / r((int)(i*1.0 / (M + 1)))*(Ut_dt[i] + Ur2test[i]);

				for (int i = 0; i < (N + 1)*(M + 1); i++)
					Sigma_tt[i] = (2 * mu + lambda) / r((int)(i*1.0 / (M + 1)))*(Ut_dt[i] + Ur2test[i]) + lambda*Ur_dr[i];

				for (int i = 0; i < (N + 1)*(M + 1); i++)
					I1[i] = Sigma_rr[i] + Sigma_tt[i];

				for (int i = 0; i < (N + 1)*(M + 1); i++)
					Sigma_e[i] = absd(Sigma_rr[i] - Sigma_tt[i]);


				// saving Ur_dr
				//string fname_str_r_dr = "step" + itos(k) + "r_dr.txt";
				//save_grafic_3d_onD(D, Ur_dr, (M + 1)*(N + 1), fname_str_r_dr.c_str());
				//if ((myrank == 0) && printinfo) cout << "file " << fname_str_r_dr << " was successfully created" << endl;

				// saving Ur_dt
				//string fname_str_r_dt = "step" + itos(k) + "r_dt.txt";
				//save_grafic_3d_onD(D, Ur_dt, (M + 1)*(N + 1), fname_str_r_dt.c_str());
				//if ((myrank == 0) && printinfo) cout << "file " << fname_str_r_dt << " was successfully created" << endl;

				// saving Ut_dr
				//string fname_str_t_dr = "step" + itos(k) + "t_dr.txt";
				//save_grafic_3d_onD(D, Ut_dr, (M + 1)*(N + 1), fname_str_t_dr.c_str());
				//if ((myrank == 0) && printinfo) cout << "file " << fname_str_t_dr << " was successfully created" << endl;

				// saving Ut_dt
				//string fname_str_t_dt = "step" + itos(k) + "t_dt.txt";
				//save_grafic_3d_onD(D, Ut_dt, (M + 1)*(N + 1), fname_str_t_dt.c_str());
				//if ((myrank == 0) && printinfo) cout << "file " << fname_str_t_dt << " was successfully created" << endl;

				if (saveTensors){
					// saving Sigma_rr
					string fname_str_sigma_rr = "step" + itos(k) + "sigma_rr.txt";
					save_grafic_3d_onD(D, Sigma_rr, (M + 1)*(N + 1), fname_str_sigma_rr.c_str());
					if ((myrank == 0) && printinfo) cout << "file " << fname_str_sigma_rr << " was successfully created" << endl;

					// saving Sigma_tt
					string fname_str_sigma_tt = "step" + itos(k) + "sigma_tt.txt";
					save_grafic_3d_onD(D, Sigma_tt, (M + 1)*(N + 1), fname_str_sigma_tt.c_str());
					if ((myrank == 0) && printinfo) cout << "file " << fname_str_sigma_tt << " was successfully created" << endl;
				}

				double minv = a.grho*a.grho*a.grho;
				for (int i = 0; i < N*M; i++){
					if ((absd(Ur2test[i]) < minv) || (absd(Ut2test[i]) < minv)){
						//I1[i] = 0.0;
						//Sigma_e[i] = 0.0;
					}
				}

				if (saveInvariants){

					// saving I1
					string fname_str_I1 = "step" + itos(k) + "I1.txt";
					save_grafic_3d_onD(D, I1, (M + 1)*(N + 1), fname_str_I1.c_str());
					if ((myrank == 0) && printinfo) cout << "file " << fname_str_I1 << " was successfully created" << endl;

					// saving Sigma_e
					string fname_str_Sigma_e = "step" + itos(k) + "sigma_e.txt";
					save_grafic_3d_onD(D, Sigma_e, (M + 1)*(N + 1), fname_str_Sigma_e.c_str());
					if ((myrank == 0) && printinfo) cout << "file " << fname_str_Sigma_e << " was successfully created" << endl;

				}

			}

		}

		//Ur2[0] = tempUr00;


		tempU = Ur0;
		Ur0 = Ur1;
		Ur1 = Ur2;
		Ur2 = tempU;

		tempU = Ut0;
		Ut0 = Ut1;
		Ut1 = Ut2;
		Ut2 = tempU;


		// DATA EXCHANGE
		//
#if GO_MPI == 1
		if (paral && (cs == 1)){

			int count = L*s*(M + 1);
			MPI_Status status;

			// I
			if (myrank > 0){ // (1)
				MPI_Send(Ur1 + ib*(M + 1), count, MPI_DOUBLE, myrank - 1, 1, MPI_COMM_WORLD);
				MPI_Send(Ut1 + ib*(M + 1), count, MPI_DOUBLE, myrank - 1, 1, MPI_COMM_WORLD);
			}

			if (myrank < np - 1){ // (2)
				MPI_Recv(Ur1 + (ie + 1)*(M + 1), count, MPI_DOUBLE, myrank + 1, 1, MPI_COMM_WORLD, &status);
				MPI_Recv(Ut1 + (ie + 1)*(M + 1), count, MPI_DOUBLE, myrank + 1, 1, MPI_COMM_WORLD, &status);
			}

			// II
			if (myrank < np - 1){ // (3)
				MPI_Send(Ur1 + (ie - s*L + 1)*(M + 1), count, MPI_DOUBLE, myrank + 1, 2, MPI_COMM_WORLD);
				MPI_Send(Ut1 + (ie - s*L + 1)*(M + 1), count, MPI_DOUBLE, myrank + 1, 2, MPI_COMM_WORLD);
			}

			if (myrank > 0){ // (4)
				MPI_Recv(Ur1 + (ib - s*L)*(M + 1), count, MPI_DOUBLE, myrank - 1, 2, MPI_COMM_WORLD, &status);
				MPI_Recv(Ut1 + (ib - s*L)*(M + 1), count, MPI_DOUBLE, myrank - 1, 2, MPI_COMM_WORLD, &status);
			}

			MPI_Barrier(MPI_COMM_WORLD);

		}
#endif



		k++;
	}

	//save_grafic_3d_onD(D, U1, (M + 1)*(N + 1), "stepLast.txt");

	delete[](D);
	delete[](Vr0);
	delete[](Vt0);
	delete[](Ur0);
	delete[](Ur1);
	delete[](Ur2);
	delete[](Ut0);
	delete[](Ut1);
	delete[](Ut2);

	return 0;

}



int elast_directU_2d(int mode){
	using namespace std;

	double *D, *Vr0, *Vt0;

	int myrank = 0, np = 1;

#if GO_MPI == 1
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	MPI_Comm_size(MPI_COMM_WORLD, &np);
#endif

	bool savesolution = true;
	bool printinfo = true;
	bool saveUrUt = true;
	bool saveTensors = true;
	bool saveInvariants = true;
	bool saveeverystep = false;

	bool stepControl = false;
	int maxSteps = 100;

	if ( mode == EVERYDAY ){
		savesolution = true;
		printinfo = true;
	}

	//params a;
	//init_params(a);
	if ( ( myrank == 0 ) && printinfo ) print_params(a);

	double alpha = a.alpha;
	double betta = a.betta;
	double R = a.R;
	double epsilon = a.epsilon;
	double dense = a.dense;
	double mu = a.mu;
	double lambda = a.lambda;

	double T = a.T;

	int N = a.N;
	int M = a.M;

	double tau = a.tau;

	double dl1 = (M_PI - betta);
	double dl2 = (M_PI + betta);

	int sfreq = a.sfreq;
	int sfreq_loc = sfreq;

	//int l1 = (dl1 - grho*grho*N2)/grho + N2;
	//int l2 = (dl2 - grho*grho*N2)/grho + N2;

	//double l1 = (M_PI - betta - alpha) / gksi;
	//double l2 = (M_PI + betta - alpha) / gksi;

	int l1 = trunc_thetta( dl1 );
	int l2 = trunc_thetta( dl2 );

	if ((myrank == 0) && printinfo) cout << "l1 = " << l1 << endl;
	if ((myrank == 0) && printinfo) cout << "l2 = " << l2 << endl;

	D = new double[2 * (N + 1)*(M + 1)];
	Vr0 = new double[(N + 1)*(M + 1)];
	Vt0 = new double[(N + 1)*(M + 1)];
	Ur0 = new double[(N + 1)*(M + 1)];
	Ur1 = new double[(N + 1)*(M + 1)];
	Ur2 = new double[(N + 1)*(M + 1)];
	Ut0 = new double[(N + 1)*(M + 1)];
	Ut1 = new double[(N + 1)*(M + 1)];
	Ut2 = new double[(N + 1)*(M + 1)];

	double *Ur2test = new double[(N + 1)*(M + 1)];
	double *Ut2test = new double[(N + 1)*(M + 1)];


	double *Ur_dr = new double[(N + 1)*(M + 1)];
	double *Ur_dt = new double[(N + 1)*(M + 1)];
	double *Ut_dr = new double[(N + 1)*(M + 1)];
	double *Ut_dt = new double[(N + 1)*(M + 1)];
	double *Sigma_rr = new double[(N + 1)*(M + 1)];
	double *Sigma_tt = new double[(N + 1)*(M + 1)];
	double *Sigma_rt = new double[(N + 1)*(M + 1)];
	double *Sigma_rrr = new double[(N + 1)*(M + 1)];
	double *Sigma_rtt = new double[(N + 1)*(M + 1)];
	double *Sigma_rtr = new double[(N + 1)*(M + 1)];
	double *Sigma_ttt = new double[(N + 1)*(M + 1)];
	double *I1 = new double[(N + 1)*(M + 1)];
	double *Sigma_e = new double[(N + 1)*(M + 1)];

	double *Sigma1 = new double[(N + 1)*(M + 1)];

	// Domain
	for (int i = 0; i <= N; i++){
		for (int j = 0; j <= M; j++){
			D[(i*(M + 1) + j) * 2] = r(i);
			D[(i*(M + 1) + j) * 2 + 1] = thetta(j);
			//D[(i*(M + 1) + j) * 2] = i*grho + epsilon;
			//D[(i*(M + 1) + j) * 2 + 1] = j*gksi + alpha;
		}
	}


	// Initial force Sigma^1
	for (int i = 0; i < N + 1; i++){
		for (int j = 0; j < M + 1; j++){
			if (((i > N - 1) && (i <= N)) && ((j >= l1) && (j <= l2))){
				Sigma1[i*(M + 1) + j] = 1.0;
			}
			else
				Sigma1[i*(M + 1) + j] = 0.0;
		}
	}

	if ( myrank == 0 ){
		save_grafic_3d_onD(D, Sigma1, (M + 1)*(N + 1), "step0Sigma1.txt");
	}

	// Initial conditions U_0
	for (int i = 0; i < (M + 1)*(N + 1); i++){
		Ur0[i] = 0.0;
		Ut0[i] = 0.0;
	}
	for (int i = 0; i < (M + 1)*(N + 1); i++){
		Ur1[i] = 0.0;
		Ut1[i] = 0.0;
	}

	// Initial conditions \sigma
	for (int i = 0; i < (N + 1)*(M + 1); i++)
		Sigma_rr[i] = Sigma1[i];

	for (int i = 0; i < (N + 1)*(M + 1); i++)
		Sigma_tt[i] = 0.0;

	for (int i = 0; i < (N + 1)*(M + 1); i++)
		Sigma_rt[i] = 0.0;


	if ((myrank == 0) && savesolution){
		save_grafic_3d_onD(D, Ur0, (M + 1)*(N + 1), "step0r.txt");
		save_grafic_3d_onD(D, Ut0, (M + 1)*(N + 1), "step0t.txt");

		save_grafic_3d_onD(D, Ur1, (M + 1)*(N + 1), "step1r.txt");
		save_grafic_3d_onD(D, Ut1, (M + 1)*(N + 1), "step1t.txt");


		for (int i = 0; i < (N + 1)*(M + 1); i++)
			I1[i] = Sigma_rr[i] + Sigma_tt[i];

		for (int i = 0; i < (N + 1)*(M + 1); i++)
			Sigma_e[i] = absd(Sigma_rr[i] - Sigma_tt[i]);


		// saving Ur_dr
		//string fname_str_r_dr = "step" + itos(k) + "r_dr.txt";
		//save_grafic_3d_onD(D, Ur_dr, (M + 1)*(N + 1), fname_str_r_dr.c_str());
		//if ((myrank == 0) && printinfo) cout << "file " << fname_str_r_dr << " was successfully created" << endl;

		// saving Ur_dt
		//string fname_str_r_dt = "step" + itos(k) + "r_dt.txt";
		//save_grafic_3d_onD(D, Ur_dt, (M + 1)*(N + 1), fname_str_r_dt.c_str());
		//if ((myrank == 0) && printinfo) cout << "file " << fname_str_r_dt << " was successfully created" << endl;

		// saving Ut_dr
		//string fname_str_t_dr = "step" + itos(k) + "t_dr.txt";
		//save_grafic_3d_onD(D, Ut_dr, (M + 1)*(N + 1), fname_str_t_dr.c_str());
		//if ((myrank == 0) && printinfo) cout << "file " << fname_str_t_dr << " was successfully created" << endl;

		// saving Ut_dt
		//string fname_str_t_dt = "step" + itos(k) + "t_dt.txt";
		//save_grafic_3d_onD(D, Ut_dt, (M + 1)*(N + 1), fname_str_t_dt.c_str());
		//if ((myrank == 0) && printinfo) cout << "file " << fname_str_t_dt << " was successfully created" << endl;

		if (saveTensors){

			// saving Sigma_rr
			string fname_str_sigma_rr = "step" + itos(1) + "sigma_rr.txt";
			save_grafic_3d_onD(D, Sigma_rr, (M + 1)*(N + 1), fname_str_sigma_rr.c_str());
			if ((myrank == 0) && printinfo) cout << "file " << fname_str_sigma_rr << " was successfully created" << endl;

			// saving Sigma_tt
			string fname_str_sigma_tt = "step" + itos(1) + "sigma_tt.txt";
			save_grafic_3d_onD(D, Sigma_tt, (M + 1)*(N + 1), fname_str_sigma_tt.c_str());
			if ((myrank == 0) && printinfo) cout << "file " << fname_str_sigma_tt << " was successfully created" << endl;

			// saving Sigma_rt
			string fname_str_sigma_rt = "step" + itos(1) + "sigma_rt.txt";
			save_grafic_3d_onD(D, Sigma_rt, (M + 1)*(N + 1), fname_str_sigma_rt.c_str());
			if ((myrank == 0) && printinfo) cout << "file " << fname_str_sigma_rt << " was successfully created" << endl;

		}


		if (saveInvariants){

			// saving I1
			string fname_str_I1 = "step" + itos(0) + "I1.txt";
			save_grafic_3d_onD(D, I1, (M + 1)*(N + 1), fname_str_I1.c_str());
			if ((myrank == 0) && printinfo) cout << "file " << fname_str_I1 << " was successfully created" << endl;

			// saving Sigma_e
			string fname_str_Sigma_e = "step" + itos(0) + "sigma_e.txt";
			save_grafic_3d_onD(D, Sigma_e, (M + 1)*(N + 1), fname_str_Sigma_e.c_str());
			if ((myrank == 0) && printinfo) cout << "file " << fname_str_Sigma_e << " was successfully created" << endl;

		}


	}


	double timesumr, timesumt, timemul;
	int k = 2;
	double* tempU;
	double eps = 0.0;


	for (double t = 2 * tau; ((t < T)&&(!stepControl)) || ((k < maxSteps)&&stepControl); t += tau){

		// Step t-1
		dF_dr(Sigma_rrr, Sigma_rr, a);
		dF_dt(Sigma_ttt, Sigma_tt, a);
		dF_dr(Sigma_rtr, Sigma_rt, a);
		dF_dt(Sigma_rtt, Sigma_rt, a);

		for (int i = 0; i < (N + 1)*(M + 1); i++)
			Ur2[i] = (1.0 / dense)*Sigma_rrr[i] + 1.0 / (dense*r((int)(i*1.0 / (M + 1)))) * (Sigma_rtt[i] + Sigma_rr[i] - Sigma_tt[i]);

		for (int i = 0; i < (N + 1)*(M + 1); i++)
			Ut2[i] = (1.0 / dense)*Sigma_rtr[i] + 1.0 / (dense*r((int)(i*1.0 / (M + 1)))) * (Sigma_ttt[i] + 2*Sigma_rt[i]);


		if (savesolution && ((((((int)(t / tau)) % ((int)(T / tau / (sfreq_loc * T))) == 0))) || saveeverystep)){

			if (myrank == 0) cout << (int)(t / T * 100 + 1) << "%  t = " << t << endl;

			if ((myrank == 0)){
				string fname_str_urpp = "step" + itos(k) + "urpp.txt";
				save_grafic_3d_onD(D, Ur2, (M + 1)*(N + 1), fname_str_urpp.c_str());
				if (printinfo) cout << "file " << fname_str_urpp << " was successfully created" << endl;
			}

			if ((myrank == 0)){
				string fname_str_utpp = "step" + itos(k) + "utpp.txt";
				save_grafic_3d_onD(D, Ut2, (M + 1)*(N + 1), fname_str_utpp.c_str());
				if (printinfo) cout << "file " << fname_str_utpp << " was successfully created" << endl;
			}

		}


		for (int i = 0; i <= N; i++){
			for (int j = 0; j <= M; j++){

				//if (k == 1){
				//	timesumr = (tau*Vr0[i*(M + 1) + j]) + Ur1[i*(M + 1) + j];
				//	timesumt = (tau*Vt0[i*(M + 1) + j]) + Ut1[i*(M + 1) + j];
				//	timemul = tau*tau / 2;
				//}
				//else
				{
					timesumr = -Ur0[i*(M + 1) + j] + (2 * Ur1[i*(M + 1) + j]);
					timesumt = -Ut0[i*(M + 1) + j] + (2 * Ut1[i*(M + 1) + j]);
					timemul = 2*tau*tau;
				}


				// Points N1
				//if (((i >= 1) && (i <= N - 2)) && ((j >= 1) && (j <= M - 2))){

					Ur2[i*(M + 1) + j] = timesumr + timemul*Ur2[i*(M + 1) + j];

					Ut2[i*(M + 1) + j] = timesumt + timemul*Ut2[i*(M + 1) + j];

				//}

				}
			}
		

		dF_dr(Ur_dr, Ur2, a);
		dF_dt(Ur_dt, Ur2, a);
		dF_dr(Ut_dr, Ut2, a);
		dF_dt(Ut_dt, Ut2, a);

		for (int i = 0; i < (N + 1)*(M + 1); i++)
			Sigma_rr[i] = (2 * mu + lambda)*Ur_dr[i] + lambda / r((int)(i*1.0 / (M + 1)))*(Ut_dt[i] + Ur2[i]);

		for (int i = 0; i < (N + 1)*(M + 1); i++)
			Sigma_tt[i] = (2 * mu + lambda) / r((int)(i*1.0 / (M + 1)))*(Ut_dt[i] + Ur2[i]) + lambda*Ur_dr[i];

		for (int i = 0; i < (N + 1)*(M + 1); i++)
			Sigma_rt[i] = mu * (1.0 / r((int)(i*1.0 / (M + 1))) * Ur_dt[i] + Ut_dr[i] - 1.0 / r((int)(i*1.0 / (M + 1))) * Ut2[i]);



		if (savesolution && ( ( ((((int)(t / tau)) % ((int)(T / tau / (sfreq_loc * T))) == 0))) || saveeverystep ) ){

			//if (myrank == 0) cout << (int)(t / T * 100 + 1) << "%  t = " << t << endl;

			// Saving solution to files
#if GO_MPI == 1

			MPI_Gatherv(Ur2 + displs[myrank], recvcounts[myrank], MPI_DOUBLE, Ur2test, recvcounts, displs, MPI_DOUBLE, 0, MPI_COMM_WORLD);
			if ((myrank == 0) && saveUrUt){
				string fname_str_r_mpi = "step" + itos(k) + "r_mpi.txt";
				save_grafic_3d_onD(D, Ur2test, (M + 1)*(N + 1), fname_str_r_mpi.c_str());
				if (printinfo) cout << "file " << fname_str_r_mpi << " was successfully created" << endl;
			}

			MPI_Gatherv(Ut2 + displs[myrank], recvcounts[myrank], MPI_DOUBLE, Ut2test, recvcounts, displs, MPI_DOUBLE, 0, MPI_COMM_WORLD);
			if ((myrank == 0) && saveUrUt){
				string fname_str_t_mpi = "step" + itos(k) + "t_mpi.txt";
				save_grafic_3d_onD(D, Ut2test, (M + 1)*(N + 1), fname_str_t_mpi.c_str());
				if (printinfo) cout << "file " << fname_str_t_mpi << " was successfully created" << endl;
			}

#else
			if (saveUrUt){

				if ((myrank == 0) && saveUrUt){
					string fname_str_r = "step" + itos(k) + "r.txt";
					save_grafic_3d_onD(D, Ur2, (M + 1)*(N + 1), fname_str_r.c_str());
					if (printinfo) cout << "file " << fname_str_r << " was successfully created" << endl;
				}


				if ((myrank == 0) && saveUrUt){
					string fname_str_t = "step" + itos(k) + "t.txt";
					save_grafic_3d_onD(D, Ut2, (M + 1)*(N + 1), fname_str_t.c_str());
					if (printinfo) cout << "file " << fname_str_t << " was successfully created" << endl;
				}
			}

#endif


			if (myrank == 0){
#if GO_MPI == 0
				Ur2test = Ur2; // Only without MPI
				Ut2test = Ut2; // Only without MPI
#endif		

				
				for (int i = 0; i < (N + 1)*(M + 1); i++)
					I1[i] = Sigma_rr[i] + Sigma_tt[i];

				for (int i = 0; i < (N + 1)*(M + 1); i++)
					Sigma_e[i] = absd( Sigma_rr[i] - Sigma_tt[i] );

				if (saveTensors){
					// saving Sigma_rr
					string fname_str_sigma_rr = "step" + itos(k) + "sigma_rr.txt";
					save_grafic_3d_onD(D, Sigma_rr, (M + 1)*(N + 1), fname_str_sigma_rr.c_str());
					if ((myrank == 0) && printinfo) cout << "file " << fname_str_sigma_rr << " was successfully created" << endl;

					// saving Sigma_tt
					string fname_str_sigma_tt = "step" + itos(k) + "sigma_tt.txt";
					save_grafic_3d_onD(D, Sigma_tt, (M + 1)*(N + 1), fname_str_sigma_tt.c_str());
					if ((myrank == 0) && printinfo) cout << "file " << fname_str_sigma_tt << " was successfully created" << endl;

					// saving Sigma_rt
					string fname_str_sigma_rt = "step" + itos(k) + "sigma_rt.txt";
					save_grafic_3d_onD(D, Sigma_rt, (M + 1)*(N + 1), fname_str_sigma_rt.c_str());
					if ((myrank == 0) && printinfo) cout << "file " << fname_str_sigma_rt << " was successfully created" << endl;
				}

				double minv = a.grho*a.grho*a.grho;
				for (int i = 0; i < N*M; i++){
					if ((absd(Ur2test[i]) < minv) || (absd(Ut2test[i]) < minv)){
						//I1[i] = 0.0;
						//Sigma_e[i] = 0.0;
					}
				}

				if (saveInvariants){

					// saving I1
					string fname_str_I1 = "step" + itos(k) + "I1.txt";
					save_grafic_3d_onD(D, I1, (M + 1)*(N + 1), fname_str_I1.c_str());
					if ((myrank == 0) && printinfo) cout << "file " << fname_str_I1 << " was successfully created" << endl;

					// saving Sigma_e
					string fname_str_Sigma_e = "step" + itos(k) + "sigma_e.txt";
					save_grafic_3d_onD(D, Sigma_e, (M + 1)*(N + 1), fname_str_Sigma_e.c_str());
					if ((myrank == 0) && printinfo) cout << "file " << fname_str_Sigma_e << " was successfully created" << endl;

				}

			}

		}

		//Ur2[0] = tempUr00;


		tempU = Ur0;
		Ur0 = Ur1;
		Ur1 = Ur2;
		Ur2 = tempU;

		tempU = Ut0;
		Ut0 = Ut1;
		Ut1 = Ut2;
		Ut2 = tempU;


		for (int i = 0; i < N + 1; i++){
			for (int j = 0; j < M + 1; j++){
				
				if (((i > N - 1) && (i <= N)) && ((j >= l1) && (j <= l2))){
					Sigma_rr[i*(M + 1) + j] = 0.0;
				}
				

			}
		}



		// DATA EXCHANGE
		//
#if GO_MPI == 1
		if (paral && (cs == 1)){

			int count = L*s*(M + 1);
			MPI_Status status;

			// I
			if (myrank > 0){ // (1)
				MPI_Send(Ur1 + ib*(M + 1), count, MPI_DOUBLE, myrank - 1, 1, MPI_COMM_WORLD);
				MPI_Send(Ut1 + ib*(M + 1), count, MPI_DOUBLE, myrank - 1, 1, MPI_COMM_WORLD);
			}

			if (myrank < np - 1){ // (2)
				MPI_Recv(Ur1 + (ie + 1)*(M + 1), count, MPI_DOUBLE, myrank + 1, 1, MPI_COMM_WORLD, &status);
				MPI_Recv(Ut1 + (ie + 1)*(M + 1), count, MPI_DOUBLE, myrank + 1, 1, MPI_COMM_WORLD, &status);
			}

			// II
			if (myrank < np - 1){ // (3)
				MPI_Send(Ur1 + (ie - s*L + 1)*(M + 1), count, MPI_DOUBLE, myrank + 1, 2, MPI_COMM_WORLD);
				MPI_Send(Ut1 + (ie - s*L + 1)*(M + 1), count, MPI_DOUBLE, myrank + 1, 2, MPI_COMM_WORLD);
			}

			if (myrank > 0){ // (4)
				MPI_Recv(Ur1 + (ib - s*L)*(M + 1), count, MPI_DOUBLE, myrank - 1, 2, MPI_COMM_WORLD, &status);
				MPI_Recv(Ut1 + (ib - s*L)*(M + 1), count, MPI_DOUBLE, myrank - 1, 2, MPI_COMM_WORLD, &status);
			}

			MPI_Barrier(MPI_COMM_WORLD);

		}
#endif



		k++;
	}

	//save_grafic_3d_onD(D, U1, (M + 1)*(N + 1), "stepLast.txt");

	delete[](D);
	delete[](Vr0);
	delete[](Vt0);
	delete[](Ur0);
	delete[](Ur1);
	delete[](Ur2);
	delete[](Ut0);
	delete[](Ut1);
	delete[](Ut2);

	return 0;

}

int elast_directV_2d(int mode){
	using namespace std;

	double *D;

	int myrank = 0, np = 1;

#if GO_MPI == 1
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	MPI_Comm_size(MPI_COMM_WORLD, &np);
#endif

	bool savesolution = true;
	bool printinfo = true;
	bool saveUrUt = true;
	bool saveTensors = true;
	bool saveInvariants = true;
	bool saveeverystep = false;

	bool stepControl = false;
	int maxSteps = 10;

	if (mode == EVERYDAY){
		savesolution = true;
		printinfo = true;
	}

	//params a;
	//init_params(a);
	if ((myrank == 0) && printinfo) print_params(a);

	double alpha = a.alpha;
	double betta = a.betta;
	double R = a.R;
	double epsilon = a.epsilon;
	double dense = a.dense;
	double mu = a.mu;
	double lambda = a.lambda;

	double T = a.T;

	int N = a.N;
	int M = a.M;

	double tau = a.tau;

	double dl1 = (M_PI - betta);
	double dl2 = (M_PI + betta);

	int sfreq = a.sfreq;
	int sfreq_loc = sfreq;

	//int l1 = (dl1 - grho*grho*N2)/grho + N2;
	//int l2 = (dl2 - grho*grho*N2)/grho + N2;

	//double l1 = (M_PI - betta - alpha) / gksi;
	//double l2 = (M_PI + betta - alpha) / gksi;

	int l1 = trunc_thetta(dl1);
	int l2 = trunc_thetta(dl2);

	if ((myrank == 0) && printinfo) cout << "l1 = " << l1 << endl;
	if ((myrank == 0) && printinfo) cout << "l2 = " << l2 << endl;

	D = new double[2 * (N + 1)*(M + 1)];
	double* Vr0 = new double[(N + 1)*(M + 1)];
	double* Vt0 = new double[(N + 1)*(M + 1)];
	double* Vr1 = new double[(N + 1)*(M + 1)];
	double* Vt1 = new double[(N + 1)*(M + 1)];
	//Ur0 = new double[(N + 1)*(M + 1)];
	//Ur1 = new double[(N + 1)*(M + 1)];
	//Ur2 = new double[(N + 1)*(M + 1)];
	//Ut0 = new double[(N + 1)*(M + 1)];
	//Ut1 = new double[(N + 1)*(M + 1)];
	//Ut2 = new double[(N + 1)*(M + 1)];


	double *Ur2test = new double[(N + 1)*(M + 1)];
	double *Ut2test = new double[(N + 1)*(M + 1)];


	double *Vr_dr = new double[(N + 1)*(M + 1)];
	double *Vr_dt = new double[(N + 1)*(M + 1)];
	double *Vt_dr = new double[(N + 1)*(M + 1)];
	double *Vt_dt = new double[(N + 1)*(M + 1)];
	double *Sigma_rr0 = new double[(N + 1)*(M + 1)];
	double *Sigma_tt0 = new double[(N + 1)*(M + 1)];
	double *Sigma_rt0 = new double[(N + 1)*(M + 1)];
	double *Sigma_rr2 = new double[(N + 1)*(M + 1)];
	double *Sigma_tt2 = new double[(N + 1)*(M + 1)];
	double *Sigma_rt2 = new double[(N + 1)*(M + 1)];
	double *Sigma_rrr = new double[(N + 1)*(M + 1)];
	double *Sigma_rtt = new double[(N + 1)*(M + 1)];
	double *Sigma_rtr = new double[(N + 1)*(M + 1)];
	double *Sigma_ttt = new double[(N + 1)*(M + 1)];
	double *I1 = new double[(N + 1)*(M + 1)];
	double *Sigma_e = new double[(N + 1)*(M + 1)];

	double *Sigma1 = new double[(N + 1)*(M + 1)];

	// Domain
	for (int i = 0; i <= N; i++){
		for (int j = 0; j <= M; j++){
			D[(i*(M + 1) + j) * 2] = r(i);
			D[(i*(M + 1) + j) * 2 + 1] = thetta(j);
			//D[(i*(M + 1) + j) * 2] = i*grho + epsilon;
			//D[(i*(M + 1) + j) * 2 + 1] = j*gksi + alpha;
		}
	}


	// Initial force Sigma^1
	for (int i = 0; i < N + 1; i++){
		for (int j = 0; j < M + 1; j++){
			if (((i > N - 1) && (i <= N)) && ((j >= l1) && (j <= l2))){
				Sigma1[i*(M + 1) + j] = 1.0;
			}
			else
				Sigma1[i*(M + 1) + j] = 0.0;
		}
	}

	if (myrank == 0){
		save_grafic_3d_onD(D, Sigma1, (M + 1)*(N + 1), "step0Sigma1.txt");
	}

	// Initial conditions U_0
	for (int i = 0; i < (M + 1)*(N + 1); i++){
		Vr0[i] = 0.0;
		Vt0[i] = 0.0;
	}
	for (int i = 0; i < (M + 1)*(N + 1); i++){
		Vr1[i] = 0.0;
		Vt1[i] = 0.0;
	}

	// Initial conditions \sigma
	for (int i = 0; i < (N + 1)*(M + 1); i++)
		Sigma_rr0[i] = Sigma1[i];

	for (int i = 0; i < (N + 1)*(M + 1); i++)
		Sigma_tt0[i] = 0.0;

	for (int i = 0; i < (N + 1)*(M + 1); i++)
		Sigma_rt0[i] = 0.0;


	if ((myrank == 0) && savesolution){
		save_grafic_3d_onD(D, Vr0, (M + 1)*(N + 1), "step0r.txt");
		save_grafic_3d_onD(D, Vt0, (M + 1)*(N + 1), "step0t.txt");

		//save_grafic_3d_onD(D, Vr1, (M + 1)*(N + 1), "step1r.txt");
		//save_grafic_3d_onD(D, Vt1, (M + 1)*(N + 1), "step1t.txt");


		for (int i = 0; i < (N + 1)*(M + 1); i++)
			I1[i] = Sigma_rr0[i] + Sigma_tt0[i];

		for (int i = 0; i < (N + 1)*(M + 1); i++)
			Sigma_e[i] = absd(Sigma_rr0[i] - Sigma_tt0[i]);


		// saving Ur_dr
		//string fname_str_r_dr = "step" + itos(k) + "r_dr.txt";
		//save_grafic_3d_onD(D, Ur_dr, (M + 1)*(N + 1), fname_str_r_dr.c_str());
		//if ((myrank == 0) && printinfo) cout << "file " << fname_str_r_dr << " was successfully created" << endl;

		// saving Ur_dt
		//string fname_str_r_dt = "step" + itos(k) + "r_dt.txt";
		//save_grafic_3d_onD(D, Ur_dt, (M + 1)*(N + 1), fname_str_r_dt.c_str());
		//if ((myrank == 0) && printinfo) cout << "file " << fname_str_r_dt << " was successfully created" << endl;

		// saving Ut_dr
		//string fname_str_t_dr = "step" + itos(k) + "t_dr.txt";
		//save_grafic_3d_onD(D, Ut_dr, (M + 1)*(N + 1), fname_str_t_dr.c_str());
		//if ((myrank == 0) && printinfo) cout << "file " << fname_str_t_dr << " was successfully created" << endl;

		// saving Ut_dt
		//string fname_str_t_dt = "step" + itos(k) + "t_dt.txt";
		//save_grafic_3d_onD(D, Ut_dt, (M + 1)*(N + 1), fname_str_t_dt.c_str());
		//if ((myrank == 0) && printinfo) cout << "file " << fname_str_t_dt << " was successfully created" << endl;

		if (saveTensors){

			// saving Sigma_rr
			string fname_str_sigma_rr = "step" + itos(1) + "sigma_rr.txt";
			save_grafic_3d_onD(D, Sigma_rr0, (M + 1)*(N + 1), fname_str_sigma_rr.c_str());
			if ((myrank == 0) && printinfo) cout << "file " << fname_str_sigma_rr << " was successfully created" << endl;

			// saving Sigma_tt
			string fname_str_sigma_tt = "step" + itos(1) + "sigma_tt.txt";
			save_grafic_3d_onD(D, Sigma_tt0, (M + 1)*(N + 1), fname_str_sigma_tt.c_str());
			if ((myrank == 0) && printinfo) cout << "file " << fname_str_sigma_tt << " was successfully created" << endl;

			// saving Sigma_rt
			string fname_str_sigma_rt = "step" + itos(1) + "sigma_rt.txt";
			save_grafic_3d_onD(D, Sigma_rt0, (M + 1)*(N + 1), fname_str_sigma_rt.c_str());
			if ((myrank == 0) && printinfo) cout << "file " << fname_str_sigma_rt << " was successfully created" << endl;

		}


		if (saveInvariants){

			// saving I1
			string fname_str_I1 = "step" + itos(0) + "I1.txt";
			save_grafic_3d_onD(D, I1, (M + 1)*(N + 1), fname_str_I1.c_str());
			if ((myrank == 0) && printinfo) cout << "file " << fname_str_I1 << " was successfully created" << endl;

			// saving Sigma_e
			string fname_str_Sigma_e = "step" + itos(0) + "sigma_e.txt";
			save_grafic_3d_onD(D, Sigma_e, (M + 1)*(N + 1), fname_str_Sigma_e.c_str());
			if ((myrank == 0) && printinfo) cout << "file " << fname_str_Sigma_e << " was successfully created" << endl;

		}


	}


	double timesumr, timesumt, timemul;
	int k = 2;
	double* tempU;
	double eps = 0.0;


	for (double t = 2 * tau; ((t < T) && (!stepControl)) || ((k < maxSteps) && stepControl); t += tau){

		// Step t-1
		dF_dr(Sigma_rrr, Sigma_rr0, a);
		dF_dt(Sigma_ttt, Sigma_tt0, a);
		dF_dr(Sigma_rtr, Sigma_rt0, a);
		dF_dt(Sigma_rtt, Sigma_rt0, a);

		for (int i = 0; i < (N + 1)*(M + 1); i++)
			Vr1[i] = (1.0 / dense)*Sigma_rrr[i] + 1.0 / (dense*r((int)(i*1.0 / (M + 1)))) * (Sigma_rtt[i] + Sigma_rr0[i] - Sigma_tt0[i]);

		for (int i = 0; i < (N + 1)*(M + 1); i++)
			Vt1[i] = (1.0 / dense)*Sigma_rtr[i] + 1.0 / (dense*r((int)(i*1.0 / (M + 1)))) * (Sigma_ttt[i] + 2 * Sigma_rt0[i]);


		if (savesolution && ((((((int)(t / tau)) % ((int)(T / tau / (sfreq_loc * T))) == 0))) || saveeverystep)){

			if (myrank == 0) cout << (int)(t / T * 100 + 1) << "%  t = " << t << endl;

			if ((myrank == 0)){
				string fname_str_urpp = "step" + itos(k) + "urpp.txt";
				save_grafic_3d_onD(D, Vr1, (M + 1)*(N + 1), fname_str_urpp.c_str());
				if (printinfo) cout << "file " << fname_str_urpp << " was successfully created" << endl;
			}

			if ((myrank == 0)){
				string fname_str_utpp = "step" + itos(k) + "utpp.txt";
				save_grafic_3d_onD(D, Vt1, (M + 1)*(N + 1), fname_str_utpp.c_str());
				if (printinfo) cout << "file " << fname_str_utpp << " was successfully created" << endl;
			}

		}


		for (int i = 0; i <= N; i++){
			for (int j = 0; j <= M; j++){

				timesumr = Vr0[i*(M + 1) + j];
				timesumt = Vt0[i*(M + 1) + j];
				timemul = tau;
				
				// Points N1
				//if (((i >= 1) && (i <= N - 2)) && ((j >= 1) && (j <= M - 2))){

				Vr1[i*(M + 1) + j] = timesumr + timemul*Vr1[i*(M + 1) + j];

				Vt1[i*(M + 1) + j] = timesumt + timemul*Vt1[i*(M + 1) + j];

			}
		}


		dF_dr(Vr_dr, Vr1, a);
		dF_dt(Vr_dt, Vr1, a);
		dF_dr(Vt_dr, Vt1, a);
		dF_dt(Vt_dt, Vt1, a);

		for (int i = 0; i < (N + 1)*(M + 1); i++)
			Sigma_rr2[i] = (2 * mu + lambda)*Vr_dr[i] + lambda / r((int)(i*1.0 / (M + 1)))*(Vt_dt[i] + Vr1[i]);

		for (int i = 0; i < (N + 1)*(M + 1); i++)
			Sigma_tt2[i] = (2 * mu + lambda) / r((int)(i*1.0 / (M + 1)))*(Vt_dt[i] + Vr1[i]) + lambda*Vr_dr[i];

		for (int i = 0; i < (N + 1)*(M + 1); i++)
			Sigma_rt2[i] = mu * (1.0 / r((int)(i*1.0 / (M + 1))) * Vr_dt[i] + Vt_dr[i] - 1.0 / r((int)(i*1.0 / (M + 1))) * Vt1[i]);


		double timesum_sigma_rr, timesum_sigma_rt, timesum_sigma_tt;
		double timemul_sigma;

		for (int i = 0; i <= N; i++){
			for (int j = 0; j <= M; j++){

				timesum_sigma_rr = Sigma_rr0[i*(M + 1) + j];
				timesum_sigma_rt = Sigma_rt0[i*(M + 1) + j];
				timesum_sigma_tt = Sigma_tt0[i*(M + 1) + j];
				timemul_sigma = tau;
				
				Sigma_rr2[i*(M + 1) + j] = timesum_sigma_rr + timemul_sigma*Sigma_rr2[i*(M + 1) + j];
				Sigma_rt2[i*(M + 1) + j] = timesum_sigma_rt + timemul_sigma*Sigma_rt2[i*(M + 1) + j];
				Sigma_tt2[i*(M + 1) + j] = timesum_sigma_tt + timemul_sigma*Sigma_tt2[i*(M + 1) + j];

			}
		}



		if (savesolution && ((((((int)(t / tau)) % ((int)(T / tau / (sfreq_loc * T))) == 0))) || saveeverystep)){

			//if (myrank == 0) cout << (int)(t / T * 100 + 1) << "%  t = " << t << endl;

			// Saving solution to files
#if GO_MPI == 1

			MPI_Gatherv(Ur2 + displs[myrank], recvcounts[myrank], MPI_DOUBLE, Ur2test, recvcounts, displs, MPI_DOUBLE, 0, MPI_COMM_WORLD);
			if ((myrank == 0) && saveUrUt){
				string fname_str_r_mpi = "step" + itos(k) + "r_mpi.txt";
				save_grafic_3d_onD(D, Ur2test, (M + 1)*(N + 1), fname_str_r_mpi.c_str());
				if (printinfo) cout << "file " << fname_str_r_mpi << " was successfully created" << endl;
			}

			MPI_Gatherv(Ut2 + displs[myrank], recvcounts[myrank], MPI_DOUBLE, Ut2test, recvcounts, displs, MPI_DOUBLE, 0, MPI_COMM_WORLD);
			if ((myrank == 0) && saveUrUt){
				string fname_str_t_mpi = "step" + itos(k) + "t_mpi.txt";
				save_grafic_3d_onD(D, Ut2test, (M + 1)*(N + 1), fname_str_t_mpi.c_str());
				if (printinfo) cout << "file " << fname_str_t_mpi << " was successfully created" << endl;
			}

#else
			if (saveUrUt){

				if ((myrank == 0) && saveUrUt){
					string fname_str_r = "step" + itos(k) + "r.txt";
					save_grafic_3d_onD(D, Vr1, (M + 1)*(N + 1), fname_str_r.c_str());
					if (printinfo) cout << "file " << fname_str_r << " was successfully created" << endl;
				}


				if ((myrank == 0) && saveUrUt){
					string fname_str_t = "step" + itos(k) + "t.txt";
					save_grafic_3d_onD(D, Vt1, (M + 1)*(N + 1), fname_str_t.c_str());
					if (printinfo) cout << "file " << fname_str_t << " was successfully created" << endl;
				}
			}

#endif


			if (myrank == 0){
#if GO_MPI == 0
				Ur2test = Vr1; // Only without MPI
				Ut2test = Vt1; // Only without MPI
#endif		


				for (int i = 0; i < (N + 1)*(M + 1); i++)
					I1[i] = Sigma_rr2[i] + Sigma_tt2[i];

				for (int i = 0; i < (N + 1)*(M + 1); i++)
					Sigma_e[i] = absd(Sigma_rr2[i] - Sigma_tt2[i]);

				if (saveTensors){
					// saving Sigma_rr
					string fname_str_sigma_rr = "step" + itos(k) + "sigma_rr.txt";
					save_grafic_3d_onD(D, Sigma_rr2, (M + 1)*(N + 1), fname_str_sigma_rr.c_str());
					if ((myrank == 0) && printinfo) cout << "file " << fname_str_sigma_rr << " was successfully created" << endl;

					// saving Sigma_tt
					string fname_str_sigma_tt = "step" + itos(k) + "sigma_tt.txt";
					save_grafic_3d_onD(D, Sigma_tt2, (M + 1)*(N + 1), fname_str_sigma_tt.c_str());
					if ((myrank == 0) && printinfo) cout << "file " << fname_str_sigma_tt << " was successfully created" << endl;

					// saving Sigma_rt
					string fname_str_sigma_rt = "step" + itos(k) + "sigma_rt.txt";
					save_grafic_3d_onD(D, Sigma_rt2, (M + 1)*(N + 1), fname_str_sigma_rt.c_str());
					if ((myrank == 0) && printinfo) cout << "file " << fname_str_sigma_rt << " was successfully created" << endl;
				}

				double minv = a.grho*a.grho*a.grho;
				for (int i = 0; i < N*M; i++){
					if ((absd(Ur2test[i]) < minv) || (absd(Ut2test[i]) < minv)){
						//I1[i] = 0.0;
						//Sigma_e[i] = 0.0;
					}
				}

				if (saveInvariants){

					// saving I1
					string fname_str_I1 = "step" + itos(k) + "I1.txt";
					save_grafic_3d_onD(D, I1, (M + 1)*(N + 1), fname_str_I1.c_str());
					if ((myrank == 0) && printinfo) cout << "file " << fname_str_I1 << " was successfully created" << endl;

					// saving Sigma_e
					string fname_str_Sigma_e = "step" + itos(k) + "sigma_e.txt";
					save_grafic_3d_onD(D, Sigma_e, (M + 1)*(N + 1), fname_str_Sigma_e.c_str());
					if ((myrank == 0) && printinfo) cout << "file " << fname_str_Sigma_e << " was successfully created" << endl;

				}

			}

		}

		//Ur2[0] = tempUr00;


		tempU = Vr0;
		Vr0 = Vr1;
		//Ur1 = Ur2;
		Vr1 = tempU;

		tempU = Vt0;
		Vt0 = Vt1;
		//Ut1 = Ut2;
		Vt1 = tempU;

		tempU = Sigma_rr0;
		Sigma_rr0 = Sigma_rr2;
		Sigma_rr2 = tempU;

		tempU = Sigma_rt0;
		Sigma_rt0 = Sigma_rt2;
		Sigma_rt2 = tempU;

		tempU = Sigma_tt0;
		Sigma_tt0 = Sigma_tt2;
		Sigma_tt2 = tempU;



		for (int i = 0; i < N + 1; i++){
			for (int j = 0; j < M + 1; j++){

				if (((i > N - 1) && (i <= N)) && ((j >= l1) && (j <= l2))){
					Sigma_rr0[i*(M + 1) + j] = 0.0;
				}


			}
		}



		// DATA EXCHANGE
		//
#if GO_MPI == 1
		if (paral && (cs == 1)){

			int count = L*s*(M + 1);
			MPI_Status status;

			// I
			if (myrank > 0){ // (1)
				MPI_Send(Ur1 + ib*(M + 1), count, MPI_DOUBLE, myrank - 1, 1, MPI_COMM_WORLD);
				MPI_Send(Ut1 + ib*(M + 1), count, MPI_DOUBLE, myrank - 1, 1, MPI_COMM_WORLD);
			}

			if (myrank < np - 1){ // (2)
				MPI_Recv(Ur1 + (ie + 1)*(M + 1), count, MPI_DOUBLE, myrank + 1, 1, MPI_COMM_WORLD, &status);
				MPI_Recv(Ut1 + (ie + 1)*(M + 1), count, MPI_DOUBLE, myrank + 1, 1, MPI_COMM_WORLD, &status);
			}

			// II
			if (myrank < np - 1){ // (3)
				MPI_Send(Ur1 + (ie - s*L + 1)*(M + 1), count, MPI_DOUBLE, myrank + 1, 2, MPI_COMM_WORLD);
				MPI_Send(Ut1 + (ie - s*L + 1)*(M + 1), count, MPI_DOUBLE, myrank + 1, 2, MPI_COMM_WORLD);
			}

			if (myrank > 0){ // (4)
				MPI_Recv(Ur1 + (ib - s*L)*(M + 1), count, MPI_DOUBLE, myrank - 1, 2, MPI_COMM_WORLD, &status);
				MPI_Recv(Ut1 + (ib - s*L)*(M + 1), count, MPI_DOUBLE, myrank - 1, 2, MPI_COMM_WORLD, &status);
			}

			MPI_Barrier(MPI_COMM_WORLD);

		}
#endif



		k++;
	}

	//save_grafic_3d_onD(D, U1, (M + 1)*(N + 1), "stepLast.txt");

	delete[](D);
	delete[](Vr0);
	delete[](Vt0);
	delete[](Vr1);
	delete[](Vt1);
	delete[](Ur0);
	delete[](Ur1);
	delete[](Ur2);
	delete[](Ut0);
	delete[](Ut1);
	delete[](Ut2);

	return 0;

}

int tuvp_direct_2d(int mode){
	using namespace std;

	double *D;

	int myrank = 0, np = 1;

#if GO_MPI == 1
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	MPI_Comm_size(MPI_COMM_WORLD, &np);
#endif

	bool savesolution = true;
	bool printinfo = true;

	bool saveUrUt = true;
	bool saveTensors = true;
	bool savePlastiqueTensors = true;
	bool saveInvariants = true;
	bool saveTemperature = true;
	
	bool saveDerivatives = true;

	bool saveeverystep = false;
	
	bool stepControl = false;
	int maxSteps = 10;

	if (mode == EVERYDAY){
		savesolution = true;
		printinfo = true;
	}

	//params a;
	//init_params(a);
	if ((myrank == 0) && printinfo) print_params(a);

	double alpha = a.alpha;
	double betta = a.betta;
	double R = a.R;
	double epsilon = a.epsilon;
	double dense = a.dense;
	double mu = a.mu;
	double lambda = a.lambda;
	double const_tau = a.const_tau;
	double const_ks0 = a.const_ks0;
	double const_T0 = a.const_T0;
	double const_ce = a.const_ce;
	double const_k = a.const_k;
	double const_alpha = a.const_alpha;

	double T = a.T;

	int N = a.N;
	int M = a.M;

	double tau = a.tau;

	double dl1 = (M_PI - betta);
	double dl2 = (M_PI + betta);

	int sfreq = a.sfreq;
	int sfreq_loc = sfreq;

	//int l1 = (dl1 - grho*grho*N2)/grho + N2;
	//int l2 = (dl2 - grho*grho*N2)/grho + N2;

	//double l1 = (M_PI - betta - alpha) / gksi;
	//double l2 = (M_PI + betta - alpha) / gksi;

	int l1 = trunc_thetta(dl1);
	int l2 = trunc_thetta(dl2);

	if ((myrank == 0) && printinfo) cout << "l1 = " << l1 << endl;
	if ((myrank == 0) && printinfo) cout << "l2 = " << l2 << endl;
	if ((myrank == 0) && printinfo) cout << "k = " << const_k << endl;
	if ((myrank == 0) && printinfo) cout << "alpha = " << const_alpha << endl;


	D = new double[2 * (N + 1)*(M + 1)];

	// V, V_t
	double *Vr0 = new double[(N + 1)*(M + 1)];
	double *Vt0 = new double[(N + 1)*(M + 1)];
	double *Vr0_t = new double[(N + 1)*(M + 1)];
	double *Vt0_t = new double[(N + 1)*(M + 1)];
	double *Vr0_dr = new double[(N + 1)*(M + 1)];
	double *Vr0_dt = new double[(N + 1)*(M + 1)];
	double *Vt0_dr = new double[(N + 1)*(M + 1)];
	double *Vt0_dt = new double[(N + 1)*(M + 1)];
	double *rVr0 = new double[(N + 1)*(M + 1)];
	double *rVr0_dr = new double[(N + 1)*(M + 1)];

	double *Vr1 = new double[(N + 1)*(M + 1)];
	double *Vt1 = new double[(N + 1)*(M + 1)];

	// \sigma_ij
	double *Sigma_rr0 = new double[(N + 1)*(M + 1)];
	double *Sigma_tt0 = new double[(N + 1)*(M + 1)];
	double *Sigma_rt0 = new double[(N + 1)*(M + 1)];
	double *Sigma_rr0_t = new double[(N + 1)*(M + 1)];
	double *Sigma_tt0_t = new double[(N + 1)*(M + 1)];
	double *Sigma_rt0_t = new double[(N + 1)*(M + 1)];
	double *Sigma_rr0_dr = new double[(N + 1)*(M + 1)];
	double *Sigma_rt0_dt = new double[(N + 1)*(M + 1)];
	double *Sigma_rt0_dr = new double[(N + 1)*(M + 1)];
	double *Sigma_tt0_dt = new double[(N + 1)*(M + 1)];

	double *Sigma_rr1 = new double[(N + 1)*(M + 1)];
	double *Sigma_tt1 = new double[(N + 1)*(M + 1)];
	double *Sigma_rt1 = new double[(N + 1)*(M + 1)];

	// T, T_dt
	double *T0 = new double[(N + 1)*(M + 1)];
	double *T1 = new double[(N + 1)*(M + 1)];
	double *T0_t = new double[(N + 1)*(M + 1)];
	double *T0_dtdt = new double[(N + 1)*(M + 1)];
	double *T0_drrdr = new double[(N + 1)*(M + 1)];
	double *T0_dt = new double[(N + 1)*(M + 1)];
	double *T0_dr = new double[(N + 1)*(M + 1)];

	// \tau^p_ij
	double *taup_rr0 = new double[(N + 1)*(M + 1)];
	double *taup_tt0 = new double[(N + 1)*(M + 1)];
	double *taup_rt0 = new double[(N + 1)*(M + 1)];
	double *taup_rr0_t = new double[(N + 1)*(M + 1)];
	double *taup_rt0_t = new double[(N + 1)*(M + 1)];
	double *taup_tt0_t = new double[(N + 1)*(M + 1)];

	double *taup_rr1 = new double[(N + 1)*(M + 1)];
	double *taup_tt1 = new double[(N + 1)*(M + 1)];
	double *taup_rt1 = new double[(N + 1)*(M + 1)];

	double *s_rr = new double[(N + 1)*(M + 1)];
	double *s_rt = new double[(N + 1)*(M + 1)];
	double *s_tt = new double[(N + 1)*(M + 1)];
	double *S = new double[(N + 1)*(M + 1)];

	// I1, \sigma_e
	double *I1 = new double[(N + 1)*(M + 1)];
	double *Sigma_e = new double[(N + 1)*(M + 1)];

	// \Sigma^1
	double *Sigma1 = new double[(N + 1)*(M + 1)];

	//double *Sigma_rr2 = new double[(N + 1)*(M + 1)];
	//double *Sigma_tt2 = new double[(N + 1)*(M + 1)];
	//double *Sigma_rt2 = new double[(N + 1)*(M + 1)];
	//double* Vr1 = new double[(N + 1)*(M + 1)];
	//double* Vt1 = new double[(N + 1)*(M + 1)];
	//Ur0 = new double[(N + 1)*(M + 1)];
	//Ur1 = new double[(N + 1)*(M + 1)];
	//Ur2 = new double[(N + 1)*(M + 1)];
	//Ut0 = new double[(N + 1)*(M + 1)];
	//Ut1 = new double[(N + 1)*(M + 1)];
	//Ut2 = new double[(N + 1)*(M + 1)];


	//double *Ur2test = new double[(N + 1)*(M + 1)];
	//double *Ut2test = new double[(N + 1)*(M + 1)];



	// Domain
	for (int i = 0; i <= N; i++){
		for (int j = 0; j <= M; j++){
			D[(i*(M + 1) + j) * 2] = r(i);
			D[(i*(M + 1) + j) * 2 + 1] = thetta(j);
			//D[(i*(M + 1) + j) * 2] = i*grho + epsilon;
			//D[(i*(M + 1) + j) * 2 + 1] = j*gksi + alpha;
		}
	}


	// Initial force Sigma^1
	for (int i = 0; i < N + 1; i++){
		for (int j = 0; j < M + 1; j++){
			if (((i > N - 1) && (i <= N)) && ((j >= l1) && (j <= l2))){
				Sigma1[i*(M + 1) + j] = 1.0;
			}
			else
				Sigma1[i*(M + 1) + j] = 0.0;
		}
	}

	if (myrank == 0){
		save_grafic_3d_onD(D, Sigma1, (M + 1)*(N + 1), "step0Sigma1.txt");
	}

	// Initial conditions V
	for (int i = 0; i < (M + 1)*(N + 1); i++){
		Vr1[i] = 0.0;
		Vt1[i] = 0.0;

		Vr0[i] = 0.0;
		Vt0[i] = 0.0;
	}

	// Initial conditions \tau^p_ij
	for (int i = 0; i < (M + 1)*(N + 1); i++){
		taup_rr0[i] = 0.0;
		taup_rt0[i] = 0.0;
		taup_tt0[i] = 0.0;

		taup_rr1[i] = 0.0;
		taup_rt1[i] = 0.0;
		taup_tt1[i] = 0.0;
	}

	// Initial conditions \sigma_ij
	for (int i = 0; i < (N + 1)*(M + 1); i++){
		Sigma_rr1[i] = Sigma1[i];
		Sigma_tt1[i] = 0.0;
		Sigma_rt1[i] = 0.0;

		Sigma_rr0[i] = Sigma1[i];
		Sigma_tt0[i] = 0.0;
		Sigma_rt0[i] = 0.0;
	}

	// Initial conditions T
	for (int i = 0; i < (M + 1)*(N + 1); i++){
		T0[i] = const_T0;
		T1[i] = const_T0;
	}

	// Derivatives = 0
	for (int i = 0; i < (M + 1)*(N + 1); i++){
		Vr0_t[i] = 0.0;
		Vt0_t[i] = 0.0;
		taup_rr0_t[i] = 0.0;
		taup_rt0_t[i] = 0.0;
		taup_tt0_t[i] = 0.0;
		Sigma_rr0_t[i] = 0.0;
		Sigma_tt0_t[i] = 0.0;
		Sigma_rt0_t[i] = 0.0;
		T0_t[i] = 0.0;
	}

	if ( (myrank == 0) && savesolution ){
		//save_grafic_3d_onD(D, Vr0, (M + 1)*(N + 1), "step0r.txt");
		//save_grafic_3d_onD(D, Vt0, (M + 1)*(N + 1), "step0t.txt");
		
		save_function_scalar( Vr0, D, (M + 1)*(N + 1), "r", 0, printinfo );
		save_function_scalar( Vt0, D, (M + 1)*(N + 1), "t", 0, printinfo );

		if (saveTensors){

			save_function_scalar( Sigma_rr0, D, (M + 1)*(N + 1), "sigma_rr", 0, printinfo );
			save_function_scalar( Sigma_tt0, D, (M + 1)*(N + 1), "sigma_tt", 0, printinfo );
			save_function_scalar( Sigma_rt0, D, (M + 1)*(N + 1), "sigma_rt", 0, printinfo );

		}

		if (savePlastiqueTensors){

			save_function_scalar( taup_rr0, D, (M + 1)*(N + 1), "taup_rr", 0, printinfo );
			save_function_scalar( taup_tt0, D, (M + 1)*(N + 1), "taup_tt", 0, printinfo );
			save_function_scalar( taup_rt0, D, (M + 1)*(N + 1), "taup_rt", 0, printinfo );

		}

		if (saveInvariants){

			for (int i = 0; i < (N + 1)*(M + 1); i++)
				I1[i] = Sigma_rr0[i] + Sigma_tt0[i];

			for (int i = 0; i < (N + 1)*(M + 1); i++)
				Sigma_e[i] = absd(Sigma_rr0[i] - Sigma_tt0[i]);

			save_function_scalar( I1, D, (M + 1)*(N + 1), "I1", 0, printinfo );
			save_function_scalar( Sigma_e, D, (M + 1)*(N + 1), "Sigma_e", 0, printinfo );
		
		}

		if (saveTemperature)
			save_function_scalar( T0, D, (M + 1)*(N + 1), "T0", 0, printinfo );

	}


	double timesumr, timesumt, timemul;
	int k = 1;
	double* tempU;
	double eps = 0.0;

	

	for (double t = 2 * tau; ((t < T) && (!stepControl)) || ((k < maxSteps) && stepControl); t += tau){

		// Step t-1
		dF_dr(Sigma_rr0_dr, Sigma_rr0, a);
		dF_dt(Sigma_tt0_dt, Sigma_tt0, a);
		dF_dr(Sigma_rt0_dr, Sigma_rt0, a);
		dF_dt(Sigma_rt0_dt, Sigma_rt0, a);

		for (int i = 0; i < (N + 1)*(M + 1); i++)
			Vr0_t[i] = (1.0 / dense)*Sigma_rr0_dr[i] + 1.0 / r((int)(i*1.0 / (M + 1))) * (1.0/dense*Sigma_rt0_dt[i] + Sigma_rr0[i] - Sigma_tt0[i]);

		for (int i = 0; i < (N + 1)*(M + 1); i++)
			Vt0_t[i] = (1.0 / dense)*Sigma_rt0_dr[i] + 1.0 / r((int)(i*1.0 / (M + 1))) * (1.0/dense*Sigma_tt0_dt[i] + 2 * Sigma_rt0[i]);

		// Step t-2
		double tr, ks, PHI, arg;

		for (int i = 0; i < (N + 1)*(M + 1); i++){
			tr = Sigma_rr0[i] + taup_rr0[i] + Sigma_tt0[i] + taup_tt0[i];
			s_rr[i] = Sigma_rr0[i] + taup_rr0[i] - tr / 3.0;
			s_tt[i] = Sigma_tt0[i] + taup_tt0[i] - tr / 3.0;
			s_rt[i] = Sigma_rt0[i] + taup_rt0[i];
			S[i] = sqrt(s_rr[i] * s_rr[i] + s_tt[i] * s_tt[i] + 2 * s_rt[i] * s_rt[i]);

			// function k_s(T)
			ks = 0.2;// +f(T);
			
			// function PHI(S-k_s(T))
			arg = S[i] - ks;
			if (arg >= 0)
			  PHI = arg;
			else 
			  PHI = 0.0;

			if (S[i] > 0.00000000001){
				taup_rr0_t[i] = 2 * mu / const_tau * PHI / S[i] * s_rr[i];
				taup_rt0_t[i] = 2 * mu / const_tau * PHI / S[i] * s_rt[i];
				taup_tt0_t[i] = 2 * mu / const_tau * PHI / S[i] * s_tt[i];
			}else{
				taup_rr0_t[i] = 0.0;
				taup_rt0_t[i] = 0.0;
				taup_tt0_t[i] = 0.0;
			}
		}

		// Step t-3
		for (int i = 0; i < (N + 1)*(M + 1); i++)
			rVr0[i] = r((int)(i*1.0 / (M + 1))) * Vr0[i];
		dF_dr(rVr0_dr, rVr0, a);

		dF_dt(Vt0_dt, Vt0, a);

		dF_dr(T0_dr, T0, a);
		for (int i = 0; i < (N + 1)*(M + 1); i++)
			T0_dr[i] = r((int)(i*1.0 / (M + 1))) * T0_dr[i];
		dF_dr(T0_drrdr, T0_dr, a);

		dF_dt(T0_dt, T0, a);
		dF_dt(T0_dtdt, T0_dt, a);

		double rc;
		for (int i = 0; i < (N + 1)*(M + 1); i++){
			rc = r((int)(i*1.0 / (M + 1)));
			T0_t[i] = (1.0 / dense / const_ce) * (
				        const_k * (1.0 / rc * T0_drrdr[i] + 1.0/rc/rc*T0_dtdt[i]) 
				        - (3*lambda+2*mu) * const_T0 * const_alpha * (1.0/rc*rVr0_dr[i] + 1.0/rc*Vt0_dt[i])
					    + (Sigma_rr0[i] * taup_rr0_t[i] + Sigma_tt0[i] * taup_tt0_t[i] + 2.0*Sigma_rt0[i] * taup_rt0_t[i]) / 2.0 / mu
					  );
		}

		// Step t-4
		dF_dr(Vr0_dr, Vr0, a);
		dF_dt(Vr0_dt, Vr0, a);
		dF_dr(Vt0_dr, Vt0, a);

		for (int i = 0; i < (N + 1)*(M + 1); i++)
			Sigma_rr0_t[i] = (2 * mu + lambda)*Vr0_dr[i] + lambda / r((int)(i*1.0 / (M + 1)))*(Vt0_dt[i] + Vr0[i]) - (3 * lambda + 2 * mu)*const_alpha*T0_t[i] - taup_rr0_t[i];

		for (int i = 0; i < (N + 1)*(M + 1); i++)
			Sigma_rt0_t[i] = mu * (1.0 / r((int)(i*1.0 / (M + 1))) * Vr0_dt[i] + Vt0_dr[i] - 1.0 / r((int)(i*1.0 / (M + 1))) * Vt0[i]) - taup_rt0_t[i];

		for (int i = 0; i < (N + 1)*(M + 1); i++)
			Sigma_tt0_t[i] = (2 * mu + lambda) / r((int)(i*1.0 / (M + 1)))*(Vt0_dt[i] + Vr0[i]) + lambda*Vr0_dr[i] - (3 * lambda + 2 * mu)*const_alpha*T0_t[i] - taup_tt0_t[i];

		// Step by time
		for (int i = 0; i <= N; i++){
			for (int j = 0; j <= M; j++){

				// Points N1
				//if (((i >= 1) && (i <= N - 2)) && ((j >= 1) && (j <= M - 2))){

				Vr1[i*(M + 1) + j] = Vr1[i*(M + 1) + j] + 2*tau*Vr0_t[i*(M + 1) + j];
				Vt1[i*(M + 1) + j] = Vt1[i*(M + 1) + j] + 2*tau*Vt0_t[i*(M + 1) + j];

				Sigma_rr1[i*(M + 1) + j] = Sigma_rr1[i*(M + 1) + j] + 2*tau*Sigma_rr0_t[i*(M + 1) + j];
				Sigma_rt1[i*(M + 1) + j] = Sigma_rt1[i*(M + 1) + j] + 2*tau*Sigma_rt0_t[i*(M + 1) + j];
				Sigma_tt1[i*(M + 1) + j] = Sigma_tt1[i*(M + 1) + j] + 2*tau*Sigma_tt0_t[i*(M + 1) + j];

				taup_rr1[i*(M + 1) + j] = taup_rr1[i*(M + 1) + j] + 2*taup_rr0_t[i*(M + 1) + j];
				taup_rt1[i*(M + 1) + j] = taup_rt1[i*(M + 1) + j] + 2*taup_rt0_t[i*(M + 1) + j];
				taup_tt1[i*(M + 1) + j] = taup_tt1[i*(M + 1) + j] + 2*taup_tt0_t[i*(M + 1) + j];

				T1[i*(M + 1) + j] = T1[i*(M + 1) + j] + 2*tau*T0_t[i*(M + 1) + j];

			}
		}
		

		if (savesolution 
			  && ( !stepControl&&(((((int)(t / tau)) % ((int)(T / tau / (sfreq_loc * T))) == 0))) 
			    || stepControl&&(((((int)(t / tau)) % ((int)(maxSteps / sfreq_loc)) == 0))) 
			    || saveeverystep)){

			//if (myrank == 0) cout << (int)(t / T * 100 + 1) << "%  t = " << t << endl;

			// Saving solution to files
#if GO_MPI == 1

			MPI_Gatherv(Ur2 + displs[myrank], recvcounts[myrank], MPI_DOUBLE, Ur2test, recvcounts, displs, MPI_DOUBLE, 0, MPI_COMM_WORLD);
			if ((myrank == 0) && saveUrUt){
				string fname_str_r_mpi = "step" + itos(k) + "r_mpi.txt";
				save_grafic_3d_onD(D, Ur2test, (M + 1)*(N + 1), fname_str_r_mpi.c_str());
				if (printinfo) cout << "file " << fname_str_r_mpi << " was successfully created" << endl;
			}

			MPI_Gatherv(Ut2 + displs[myrank], recvcounts[myrank], MPI_DOUBLE, Ut2test, recvcounts, displs, MPI_DOUBLE, 0, MPI_COMM_WORLD);
			if ((myrank == 0) && saveUrUt){
				string fname_str_t_mpi = "step" + itos(k) + "t_mpi.txt";
				save_grafic_3d_onD(D, Ut2test, (M + 1)*(N + 1), fname_str_t_mpi.c_str());
				if (printinfo) cout << "file " << fname_str_t_mpi << " was successfully created" << endl;
			}

#else
			if (saveUrUt){
				if (myrank == 0){
					save_function_scalar(Vr1, D, (M + 1)*(N + 1), "r", k, printinfo);
					save_function_scalar(Vt1, D, (M + 1)*(N + 1), "t", k, printinfo);
				}
			}

#endif


			if (saveTensors){

				save_function_scalar(Sigma_rr1, D, (M + 1)*(N + 1), "sigma_rr", k, printinfo);
				save_function_scalar(Sigma_tt1, D, (M + 1)*(N + 1), "sigma_tt", k, printinfo);
				save_function_scalar(Sigma_rt1, D, (M + 1)*(N + 1), "sigma_rt", k, printinfo);

			}

			if (savePlastiqueTensors){

				save_function_scalar(taup_rr1, D, (M + 1)*(N + 1), "taup_rr", k, printinfo);
				save_function_scalar(taup_tt1, D, (M + 1)*(N + 1), "taup_tt", k, printinfo);
				save_function_scalar(taup_rt1, D, (M + 1)*(N + 1), "taup_rt", k, printinfo);

			}

			if (saveInvariants){

				for (int i = 0; i < (N + 1)*(M + 1); i++)
					I1[i] = Sigma_rr1[i] + Sigma_tt1[i];

				for (int i = 0; i < (N + 1)*(M + 1); i++)
					Sigma_e[i] = absd(Sigma_rr1[i] - Sigma_tt1[i]);

				save_function_scalar(I1, D, (M + 1)*(N + 1), "I1", k, printinfo);
				save_function_scalar(Sigma_e, D, (M + 1)*(N + 1), "Sigma_e", k, printinfo);

			}

			if (saveTemperature)
				save_function_scalar(T1, D, (M + 1)*(N + 1), "T0", k, printinfo);


			if (saveDerivatives){
				save_function_scalar(Vr0_t, D, (M + 1)*(N + 1), "r_t", k, printinfo);
				save_function_scalar(Vt0_t, D, (M + 1)*(N + 1), "t_t", k, printinfo);

				save_function_scalar(Sigma_rr0_t, D, (M + 1)*(N + 1), "sigma_rr_t", k, printinfo);
				save_function_scalar(Sigma_rt0_t, D, (M + 1)*(N + 1), "sigma_rt_t", k, printinfo);
				save_function_scalar(Sigma_tt0_t, D, (M + 1)*(N + 1), "sigma_tt_t", k, printinfo);

				save_function_scalar(taup_rr0_t, D, (M + 1)*(N + 1), "taup_rr_t", k, printinfo);
				save_function_scalar(taup_rt0_t, D, (M + 1)*(N + 1), "taup_rt_t", k, printinfo);
				save_function_scalar(taup_tt0_t, D, (M + 1)*(N + 1), "taup_tt_t", k, printinfo);

				save_function_scalar(S, D, (M + 1)*(N + 1), "S", k, printinfo);

				save_function_scalar(T0_t, D, (M + 1)*(N + 1), "T0_t", k, printinfo);

			}

		}

		// V swap
		tempU = Vr1;
		Vr1 = Vr0;
		Vr0 = tempU;

		tempU = Vt1;
		Vt1 = Vt0;
		Vt0 = tempU;

		// sigma_ij swap
		tempU = Sigma_rr1;
		Sigma_rr1 = Sigma_rr0;
		Sigma_rr0 = tempU;

		tempU = Sigma_rt1;
		Sigma_rt1 = Sigma_rt0;
		Sigma_rt0 = tempU;

		tempU = Sigma_tt1;
		Sigma_tt1 = Sigma_tt0;
		Sigma_tt0 = tempU;

		// tau^p_ij swap 
		tempU = taup_rr1;
		taup_rr1 = taup_rr0;
		taup_rr0 = tempU;

		tempU = taup_rt1;
		taup_rt1 = taup_rt0;
		taup_rt0 = tempU;

		tempU = taup_tt1;
		taup_tt1 = taup_tt0;
		taup_tt0 = tempU;

		// T swap
		tempU = T1;
		T1 = T0;
		T0 = tempU;

		// zero on boarder
		if (k > 1){
			for (int i = 0; i < N + 1; i++){
				for (int j = 0; j < M + 1; j++){

					//outer circle
					if ( (i == N) && (true||((j >= l1) && (j <= l2))) ){
						Sigma_rr0[i*(M + 1) + j] = 0.0;
						Sigma_rr1[i*(M + 1) + j] = 0.0;

						Sigma_rt0[i*(M + 1) + j] = 0.0;
						Sigma_rt1[i*(M + 1) + j] = 0.0;

						taup_rr0[i*(M + 1) + j] = 0.0;
						taup_rr1[i*(M + 1) + j] = 0.0;
					}

					// inner circle
					if (i == 0){
						Sigma_rr0[i*(M + 1) + j] = 0.0;
						Sigma_rr1[i*(M + 1) + j] = 0.0;

						Sigma_rt0[i*(M + 1) + j] = 0.0;
						Sigma_rt1[i*(M + 1) + j] = 0.0;

						//taup_rr0[i*(M + 1) + j] = 0.0;
						//taup_rr1[i*(M + 1) + j] = 0.0;
					}

					// side lines
					if ( (j == 0) && (j == M) ){
						Sigma_tt0[i*(M + 1) + j] = 0.0;
						Sigma_tt1[i*(M + 1) + j] = 0.0;

						Sigma_rt0[i*(M + 1) + j] = 0.0;
						Sigma_rt1[i*(M + 1) + j] = 0.0;

						//taup_rr0[i*(M + 1) + j] = 0.0;
						//taup_rr1[i*(M + 1) + j] = 0.0;
					}

				}
			}
		}

		// DATA EXCHANGE
		//
#if GO_MPI == 1
		if (paral && (cs == 1)){

			int count = L*s*(M + 1);
			MPI_Status status;

			// I
			if (myrank > 0){ // (1)
				MPI_Send(Ur1 + ib*(M + 1), count, MPI_DOUBLE, myrank - 1, 1, MPI_COMM_WORLD);
				MPI_Send(Ut1 + ib*(M + 1), count, MPI_DOUBLE, myrank - 1, 1, MPI_COMM_WORLD);
			}

			if (myrank < np - 1){ // (2)
				MPI_Recv(Ur1 + (ie + 1)*(M + 1), count, MPI_DOUBLE, myrank + 1, 1, MPI_COMM_WORLD, &status);
				MPI_Recv(Ut1 + (ie + 1)*(M + 1), count, MPI_DOUBLE, myrank + 1, 1, MPI_COMM_WORLD, &status);
			}

			// II
			if (myrank < np - 1){ // (3)
				MPI_Send(Ur1 + (ie - s*L + 1)*(M + 1), count, MPI_DOUBLE, myrank + 1, 2, MPI_COMM_WORLD);
				MPI_Send(Ut1 + (ie - s*L + 1)*(M + 1), count, MPI_DOUBLE, myrank + 1, 2, MPI_COMM_WORLD);
			}

			if (myrank > 0){ // (4)
				MPI_Recv(Ur1 + (ib - s*L)*(M + 1), count, MPI_DOUBLE, myrank - 1, 2, MPI_COMM_WORLD, &status);
				MPI_Recv(Ut1 + (ib - s*L)*(M + 1), count, MPI_DOUBLE, myrank - 1, 2, MPI_COMM_WORLD, &status);
			}

			MPI_Barrier(MPI_COMM_WORLD);

		}
#endif



		k++;
	}

	//save_grafic_3d_onD(D, U1, (M + 1)*(N + 1), "stepLast.txt");

	

	delete[](D);
	delete[](Vr0);
	delete[](Vt0);
	delete[](Ur0);
	delete[](Ur1);
	delete[](Ur2);
	delete[](Ut0);
	delete[](Ut1);
	delete[](Ut2);

	return 0;

}


int tuvp_direct_2d_now(int mode){
	using namespace std;

	double *D;

	int myrank = 0, np = 1;

#if GO_MPI == 1
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	MPI_Comm_size(MPI_COMM_WORLD, &np);
#endif


	bool savesolution = true;
	bool printinfo = true;

	bool saveUrUt = true;
	bool saveTensors = false;
	bool savePlastiqueTensors = false;
	bool saveInvariants = true;
	bool saveTemperature = false;

	bool saveDerivatives = false;

	bool saveeverystep = false;
	bool savelast = false;
	bool saveinit = false;

	bool stepControl = false;
	int maxSteps = 15;

	if (mode == EVERYDAY){
		savesolution = true;
		printinfo = true;
	}

	if (mode == NOSAVE){
		savesolution = false;
		printinfo = true;
		savelast = false;
		saveinit = false;
	}

	//savesolution = false;
	//printinfo = false;

	//params a;
	//init_params(a);
	if ((myrank == 0) && printinfo) print_params(a);

	double alpha = a.alpha;
	double betta = a.betta;
	double R = a.R;
	double epsilon = a.epsilon;
	double dense = a.dense;
	double mu = a.mu;
	double lambda = a.lambda;
	double const_tau = a.const_tau;
	double const_ks0 = a.const_ks0;
	double const_T0 = a.const_T0;
	double const_ce = a.const_ce;
	double const_k = a.const_k;
	double const_alpha = a.const_alpha;

	bool temperature_Newton = a.temperature_Newton;
	bool temperature_Dirihle = a.temperature_Dirihle;
	double const_gamma = a.const_gamma;

	bool nocut = a.param_nocut;
	//nocut = true;

	double T = a.T;

	int N = a.N;
	int M = a.M;

	double tau0 = a.const_tau0;
	double r0 = a.const_r0;

	double tau = a.tau;

	double const_Sigma10 = a.const_Sigma10;

	double dl1 = (M_PI - betta);
	double dl2 = (M_PI + betta);

	int sfreq = a.sfreq;
	int sfreq_loc = sfreq;

	long int k_total = (long int)(T / tau);
	cout << "k_total = " << k_total << endl;

	//int l1 = (dl1 - grho*grho*N2)/grho + N2;
	//int l2 = (dl2 - grho*grho*N2)/grho + N2;

	//double l1 = (M_PI - betta - alpha) / gksi;
	//double l2 = (M_PI + betta - alpha) / gksi;

	int l1 = trunc_thetta(dl1);
	int l2 = trunc_thetta(dl2);

	if ((myrank == 0) && printinfo) cout << "l1 = " << l1 << endl;
	if ((myrank == 0) && printinfo) cout << "l2 = " << l2 << endl;
	if ((myrank == 0) && printinfo) cout << "k = " << const_k << endl;
	if ((myrank == 0) && printinfo) cout << "alpha = " << const_alpha << endl;
	if ((myrank == 0) && printinfo) cout << "gamma = " << const_gamma << endl;
	if ((myrank == 0) && printinfo) cout << "Sigma10 = " << const_Sigma10 << endl;
	if ((myrank == 0) && printinfo) cout << "P0 = " << a.const_P0_razm / 1000000.0 << "MPa" << endl;
	if ((myrank == 0) && printinfo) cout << "tau0 = " << a.const_tau0_razm*1000000.0 << " mks" << endl;
	if ((myrank == 0) && printinfo) cout << "r0 = " << a.const_r0_razm << " m" << endl;
	if ((myrank == 0) && printinfo) cout << "F0 = " << a.const_F0_razm << " N" << endl;

	D = new double[2 * (N + 1)*(M + 1)];

	double* Dtime = new double[k_total + 2];
	double *Tmax = new double[k_total + 2];
	double *TmaxRT = new double[(k_total + 2) * 2];

	for (long int i = 0; i < k_total + 2; i++){
		Tmax[i] = 0.0;
		Dtime[i] = 0.0;
		Tmax[2 * i] = 0.0;
		Tmax[2 * i + 1] = 0.0;
	}
	cout << "Hello " << endl;

	// V, V_t
	double *Vr0 = new double[(N + 1)*(M + 1)];
	double *Vt0 = new double[(N + 1)*(M + 1)];
	double *Vr0_t = new double[(N + 1)*(M + 1)];
	double *Vt0_t = new double[(N + 1)*(M + 1)];
	double *Vr0_dr = new double[(N + 1)*(M + 1)];
	double *Vr0_dt = new double[(N + 1)*(M + 1)];
	double *Vt0_dr = new double[(N + 1)*(M + 1)];
	double *Vt0_dt = new double[(N + 1)*(M + 1)];
	double *rVr0 = new double[(N + 1)*(M + 1)];
	double *rVr0_dr = new double[(N + 1)*(M + 1)];

	double *Vr1 = new double[(N + 1)*(M + 1)];
	double *Vt1 = new double[(N + 1)*(M + 1)];

	// \sigma_ij
	double *Sigma_rr0 = new double[(N + 1)*(M + 1)];
	double *Sigma_tt0 = new double[(N + 1)*(M + 1)];
	double *Sigma_rt0 = new double[(N + 1)*(M + 1)];
	double *Sigma_rr0_t = new double[(N + 1)*(M + 1)];
	double *Sigma_tt0_t = new double[(N + 1)*(M + 1)];
	double *Sigma_rt0_t = new double[(N + 1)*(M + 1)];
	double *Sigma_rr0_dr = new double[(N + 1)*(M + 1)];
	double *Sigma_rt0_dt = new double[(N + 1)*(M + 1)];
	double *Sigma_rt0_dr = new double[(N + 1)*(M + 1)];
	double *Sigma_tt0_dt = new double[(N + 1)*(M + 1)];

	double *Sigma_rr1 = new double[(N + 1)*(M + 1)];
	double *Sigma_tt1 = new double[(N + 1)*(M + 1)];
	double *Sigma_rt1 = new double[(N + 1)*(M + 1)];

	// T, T_dt
	double *T0 = new double[(N + 1)*(M + 1)];
	double *T1 = new double[(N + 1)*(M + 1)];
	double *T0_t = new double[(N + 1)*(M + 1)];
	double *T0_dtdt = new double[(N + 1)*(M + 1)];
	double *T0_drrdr = new double[(N + 1)*(M + 1)];
	double *T0_dt = new double[(N + 1)*(M + 1)];
	double *T0_dr = new double[(N + 1)*(M + 1)];

	// \tau^p_ij
	double *taup_rr0 = new double[(N + 1)*(M + 1)];
	double *taup_tt0 = new double[(N + 1)*(M + 1)];
	double *taup_rt0 = new double[(N + 1)*(M + 1)];
	double *taup_rr0_t = new double[(N + 1)*(M + 1)];
	double *taup_rt0_t = new double[(N + 1)*(M + 1)];
	double *taup_tt0_t = new double[(N + 1)*(M + 1)];

	double *taup_rr1 = new double[(N + 1)*(M + 1)];
	double *taup_tt1 = new double[(N + 1)*(M + 1)];
	double *taup_rt1 = new double[(N + 1)*(M + 1)];

	double *s_rr = new double[(N + 1)*(M + 1)];
	double *s_rt = new double[(N + 1)*(M + 1)];
	double *s_tt = new double[(N + 1)*(M + 1)];
	double *S = new double[(N + 1)*(M + 1)];

	// I1, \sigma_e
	double *I1 = new double[(N + 1)*(M + 1)];
	double *Sigma_e = new double[(N + 1)*(M + 1)];

	// \Sigma^1
	double *Sigma1 = new double[(N + 1)*(M + 1)];

	//double *Sigma_rr2 = new double[(N + 1)*(M + 1)];
	//double *Sigma_tt2 = new double[(N + 1)*(M + 1)];
	//double *Sigma_rt2 = new double[(N + 1)*(M + 1)];
	//double* Vr1 = new double[(N + 1)*(M + 1)];
	//double* Vt1 = new double[(N + 1)*(M + 1)];
	//Ur0 = new double[(N + 1)*(M + 1)];
	//Ur1 = new double[(N + 1)*(M + 1)];
	//Ur2 = new double[(N + 1)*(M + 1)];
	//Ut0 = new double[(N + 1)*(M + 1)];
	//Ut1 = new double[(N + 1)*(M + 1)];
	//Ut2 = new double[(N + 1)*(M + 1)];


	//double *Ur2test = new double[(N + 1)*(M + 1)];
	//double *Ut2test = new double[(N + 1)*(M + 1)];



	// Domain
	for (int i = 0; i <= N; i++){
		for (int j = 0; j <= M; j++){
			D[(i*(M + 1) + j) * 2] = r(i);
			D[(i*(M + 1) + j) * 2 + 1] = thetta(j);
			//D[(i*(M + 1) + j) * 2] = i*grho + epsilon;
			//D[(i*(M + 1) + j) * 2 + 1] = j*gksi + alpha;
		}
	}


	// Initial force Sigma^1
	for (int i = 0; i < N + 1; i++){
		for (int j = 0; j < M + 1; j++){
			//if (((r(i) >= R - r0) && (i <= N)) && ((j >= l1) && (j <= l2))){
			if ((i >= N) && ((j >= l1) && (j <= l2))){
				Sigma1[i*(M + 1) + j] = const_Sigma10;
			}
			else
				Sigma1[i*(M + 1) + j] = 0.0;
		}
	}

	// Initial conditions V
	for (int i = 0; i < (M + 1)*(N + 1); i++){
		Vr1[i] = 0.0;
		Vt1[i] = 0.0;

		Vr0[i] = 0.0;
		Vt0[i] = 0.0;
	}

	// Initial conditions \tau^p_ij
	for (int i = 0; i < (M + 1)*(N + 1); i++){
		taup_rr0[i] = 0.0;
		taup_rt0[i] = 0.0;
		taup_tt0[i] = 0.0;

		taup_rr1[i] = 0.0;
		taup_rt1[i] = 0.0;
		taup_tt1[i] = 0.0;
	}

	// Initial conditions \sigma_ij
	for (int i = 0; i < (N + 1)*(M + 1); i++){
		Sigma_rr1[i] = Sigma1[i];// 0.0;// Sigma1[i] * 0.006 / tau * a.grho / 0.018;
		Sigma_tt1[i] = 0.0;
		Sigma_rt1[i] = 0.0;

		Sigma_rr0[i] = Sigma1[i];// Sigma1[i] * 0.006 / tau * a.grho / 0.018;
		Sigma_tt0[i] = 0.0;
		Sigma_rt0[i] = 0.0;
	}

	// Initial conditions T
	for (int i = 0; i < (M + 1)*(N + 1); i++){
		T0[i] = const_T0;
		T1[i] = const_T0;
	}

	// Derivatives = 0
	for (int i = 0; i < (M + 1)*(N + 1); i++){
		Vr0_t[i] = 0.0;
		Vt0_t[i] = 0.0;
		taup_rr0_t[i] = 0.0;
		taup_rt0_t[i] = 0.0;
		taup_tt0_t[i] = 0.0;
		Sigma_rr0_t[i] = 0.0;
		Sigma_tt0_t[i] = 0.0;
		Sigma_rt0_t[i] = 0.0;
		T0_t[i] = 0.0;
	}

	if ((myrank == 0) && (savesolution || saveinit)){

		//save_grafic_3d_onD(D, Sigma1, (M + 1)*(N + 1), "step0Sigma1.txt");

		save_function_scalar(Sigma1, D, (M + 1)*(N + 1), "Sigma1", 0, printinfo);

		//save_grafic_3d_onD(D, Vr0, (M + 1)*(N + 1), "step0r.txt");
		//save_grafic_3d_onD(D, Vt0, (M + 1)*(N + 1), "step0t.txt");

		save_function_scalar(Vr0, D, (M + 1)*(N + 1), "r", 0, printinfo);
		save_function_scalar(Vt0, D, (M + 1)*(N + 1), "t", 0, printinfo);

		if (saveTensors){

			save_function_scalar(Sigma_rr0, D, (M + 1)*(N + 1), "sigma_rr", 0, printinfo);
			save_function_scalar(Sigma_tt0, D, (M + 1)*(N + 1), "sigma_tt", 0, printinfo);
			save_function_scalar(Sigma_rt0, D, (M + 1)*(N + 1), "sigma_rt", 0, printinfo);

		}

		if (savePlastiqueTensors){

			save_function_scalar(taup_rr0, D, (M + 1)*(N + 1), "taup_rr", 0, printinfo);
			save_function_scalar(taup_tt0, D, (M + 1)*(N + 1), "taup_tt", 0, printinfo);
			save_function_scalar(taup_rt0, D, (M + 1)*(N + 1), "taup_rt", 0, printinfo);

		}

		if (saveInvariants){

			for (int i = 0; i < (N + 1)*(M + 1); i++)
				I1[i] = Sigma_rr0[i] + Sigma_tt0[i];

			for (int i = 0; i < (N + 1)*(M + 1); i++)
				Sigma_e[i] = absd(Sigma_rr0[i] - Sigma_tt0[i]);

			save_function_scalar(I1, D, (M + 1)*(N + 1), "I1", 0, printinfo);
			save_function_scalar(Sigma_e, D, (M + 1)*(N + 1), "Sigma_e", 0, printinfo);

		}

		if (saveTemperature)
			save_function_scalar(T0, D, (M + 1)*(N + 1), "T0", 0, printinfo);

	}


	double timesumr, timesumt, timemul;
	int k = 1;
	double* tempU;
	double eps = 0.0;

	omp_set_num_threads(1);

	for (double t = 0.0; ((t < T) && (!stepControl)) || ((k < maxSteps) && stepControl); t += tau){

		// Step t-1
		dF_dr(Sigma_rr0_dr, Sigma_rr0, a);
		dF_dt(Sigma_tt0_dt, Sigma_tt0, a);
		dF_dr(Sigma_rt0_dr, Sigma_rt0, a);
		dF_dt(Sigma_rt0_dt, Sigma_rt0, a);

		double art_visc = 1000.0;

		//		#pragma omp parallel shared (Vr0_t,Sigma_rr0_dr,Sigma_rt0_dt,Sigma_rr0,Sigma_tt0) private (i)
		{	
#pragma omp parallel for
			for (int i = 0; i < (N + 1)*(M + 1); i++){
				Vr0_t[i] = -art_visc*Vr0[i] + (1.0 / dense)*Sigma_rr0_dr[i] + 1.0 / dense / r((int)(i*1.0 / (M + 1))) * (Sigma_rt0_dt[i] + Sigma_rr0[i] - Sigma_tt0[i]);
				//if (t <= tau0) Vr0_t[i] += Sigma1[i];
			}
		}

		//#pragma omp parallel shared (Vt0_t,Sigma_rt0_dr,Sigma_tt0_dt,Sigma_rt0) private (i)		
		{
#pragma omp parallel for
			for (int i = 0; i < (N + 1)*(M + 1); i++){
				Vt0_t[i] = -art_visc*Vt0[i] + (1.0 / dense)*Sigma_rt0_dr[i] + 1.0 / dense / r((int)(i*1.0 / (M + 1))) * (Sigma_tt0_dt[i] + 2 * Sigma_rt0[i]);
			}
		}

		// Step by time

		//#pragma omp parallel shared (Vr0,Vt0,Vr0_t,Vt0_t) private (i)		
		{
#pragma omp parallel for
			for (int i = 0; i <= N; i++){
				for (int j = 0; j <= M; j++){
					Vr0[i*(M + 1) + j] = Vr0[i*(M + 1) + j] + tau*Vr0_t[i*(M + 1) + j];
					Vt0[i*(M + 1) + j] = Vt0[i*(M + 1) + j] + tau*Vt0_t[i*(M + 1) + j];
				}
			}
		}

		// Step t-2

		//#pragma omp parallel shared (rVr0,Vr0) private (i)
		{
#pragma omp parallel for
			for (int i = 0; i < (N + 1)*(M + 1); i++)
				rVr0[i] = r((int)(i*1.0 / (M + 1))) * Vr0[i];
		}

		dF_dr(rVr0_dr, rVr0, a);

		dF_dt(Vt0_dt, Vt0, a);

		dF_dr(T0_dr, T0, a);

		//#pragma omp parallel shared (T0_dr,T0_dr) private (i)
		{
#pragma omp parallel for
			for (int i = 0; i < (N + 1)*(M + 1); i++)
				T0_dr[i] = r((int)(i*1.0 / (M + 1))) * T0_dr[i];
		}
		dF_dr(T0_drrdr, T0_dr, a);

		dF_dt(T0_dt, T0, a);
		dF_dt(T0_dtdt, T0_dt, a);

		double rc;
		//#pragma omp parallel shared (T0_t,T0_drrdr,T0_dtdt, rVr0_dr, Vt0_dt,Sigma_rr0,taup_rr0_t,Sigma_tt0,taup_tt0_t,Sigma_rt0,taup_rt0_t) private (i)
		{
#pragma omp parallel for
		for (int i = 0; i < (N + 1)*(M + 1); i++){
			rc = r((int)(i*1.0 / (M + 1)));
			T0_t[i] = (1.0 / dense / const_ce) * (
				const_k * (1.0 / rc * T0_drrdr[i] + 1.0 / rc / rc*T0_dtdt[i])
				- (3 * lambda + 2 * mu) * const_T0 * const_alpha * (1.0 / rc*rVr0_dr[i] + 1.0 / rc*Vt0_dt[i])
				+ (Sigma_rr0[i] * taup_rr0_t[i] + Sigma_tt0[i] * taup_tt0_t[i] + 2.0*Sigma_rt0[i] * taup_rt0_t[i]) / 2.0 / mu
				);
		}
	

	double Tt_elast = 0.0;
	double Tt_plastic = 0.0;

	int itmax;
	double Tmax_cur = Fmax(T0, (N + 1)*(M + 1), &itmax);

	for (int i = itmax; i <= itmax; i++){
		rc = r((int)(i*1.0 / (M + 1)));
		double P1 = -(1.0 / dense / const_ce) *((3 * lambda + 2 * mu) * const_T0 * const_alpha * (1.0 / rc*rVr0_dr[i] + 1.0 / rc*Vt0_dt[i]));
		double P2 = (1.0 / dense / const_ce) *((Sigma_rr0[i] * taup_rr0_t[i] + Sigma_tt0[i] * taup_tt0_t[i] + 2.0*Sigma_rt0[i] * taup_rt0_t[i]) / 2.0 / mu);

		Tt_elast += P1;
		Tt_plastic += P2;
	}

	
	if( k % 1 == 0) cout << "step "<<k<< ": T = "<< Tmax_cur<<"Tt_elast = " << Tt_elast << "; Tt_plastic = " << Tt_plastic << endl;
}



		// Step by time

//#pragma omp parallel shared (T0,T0_t) private (i)
		{
#pragma omp parallel for
			for (int i = 0; i <= N; i++){
				for (int j = 0; j <= M; j++){
					T0[i*(M + 1) + j] = T0[i*(M + 1) + j] + 1.0 * tau*T0_t[i*(M + 1) + j];
				}
			}
		}

		// Step t-3
		double tr, ks, PHI, arg;

//#pragma omp parallel shared (Sigma_rr0,Sigma_tt0,Sigma_rt0,  s_rr,s_rt,s_tt, S, taup_rr0_t,taup_rt0_t,taup_tt0_t) private (i)
		{
#pragma omp parallel for
			for (int i = 0; i < (N + 1)*(M + 1); i++){
				//tr = Sigma_rr0[i] + taup_rr0[i] + Sigma_tt0[i] + taup_tt0[i];
				//s_rr[i] = Sigma_rr0[i] + taup_rr0[i] - tr / 3.0;
				//s_tt[i] = Sigma_tt0[i] + taup_tt0[i] - tr / 3.0;
				//s_rt[i] = Sigma_rt0[i] + taup_rt0[i];

				tr = Sigma_rr0[i] + Sigma_tt0[i];
				s_rr[i] = Sigma_rr0[i] - tr / 3.0;
				s_tt[i] = Sigma_tt0[i] - tr / 3.0;
				s_rt[i] = Sigma_rt0[i];


				S[i] = sqrt(s_rr[i] * s_rr[i] + s_tt[i] * s_tt[i] + 2 * s_rt[i] * s_rt[i]);

				// function k_s(T)
				ks = a.const_ks0;// +f(T);

				// function PHI(S-k_s(T))
				arg = S[i] - ks;
				if (arg >= 0)
					PHI = arg;
				else
					PHI = 0.0;

				if (S[i] > 0.00000000001){
					taup_rr0_t[i] = 2 * mu / const_tau * PHI / S[i] * s_rr[i];
					taup_rt0_t[i] = 2 * mu / const_tau * PHI / S[i] * s_rt[i];
					taup_tt0_t[i] = 2 * mu / const_tau * PHI / S[i] * s_tt[i];
				}
				else{
					taup_rr0_t[i] = 0.0;
					taup_rt0_t[i] = 0.0;
					taup_tt0_t[i] = 0.0;
				}
			}
		}

		// Step by time
//#pragma omp parallel shared (taup_rr0,taup_rt0,taup_tt0,taup_rr0_t,taup_rt0_t,taup_tt0_t) private (i)
		{
#pragma omp parallel for
			for (int i = 0; i <= N; i++){
				for (int j = 0; j <= M; j++){
					taup_rr0[i*(M + 1) + j] = taup_rr0[i*(M + 1) + j] + tau*taup_rr0_t[i*(M + 1) + j];
					taup_rt0[i*(M + 1) + j] = taup_rt0[i*(M + 1) + j] + tau*taup_rt0_t[i*(M + 1) + j];
					taup_tt0[i*(M + 1) + j] = taup_tt0[i*(M + 1) + j] + tau*taup_tt0_t[i*(M + 1) + j];
				}
			}
		}

		// Step t-4
		dF_dr(Vr0_dr, Vr0, a);
		dF_dt(Vr0_dt, Vr0, a);
		dF_dr(Vt0_dr, Vt0, a);
		dF_dt(Vt0_dt, Vt0, a);

//#pragma omp parallel shared (Sigma_rr0_t,Vr0_dr,Vt0_dt,Vr0,T0_t,taup_rr0_t) private (i)
		{
#pragma omp parallel for
			for (int i = 0; i < (N + 1)*(M + 1); i++)
				Sigma_rr0_t[i] = (2 * mu + lambda)*Vr0_dr[i] + lambda / r((int)(i*1.0 / (M + 1)))*(Vt0_dt[i] + Vr0[i]) - (3 * lambda + 2 * mu)*const_alpha*T0_t[i] - taup_rr0_t[i];
		}

//#pragma omp parallel shared (Sigma_rt0_t,Vr0_dt,Vt0_dr,Vt0,taup_rt0_t) private (i)
		{
#pragma omp parallel for
			for (int i = 0; i < (N + 1)*(M + 1); i++)
				Sigma_rt0_t[i] = mu * (1.0 / r((int)(i*1.0 / (M + 1))) * Vr0_dt[i] + Vt0_dr[i] - 1.0 / r((int)(i*1.0 / (M + 1))) * Vt0[i]) - taup_rt0_t[i];
		}

//#pragma omp parallel shared (Sigma_tt0_t,Vt0_dt,Vr0,Vr0_dr,T0_t,taup_tt0_t) private (i)
		{
#pragma omp parallel for
			for (int i = 0; i < (N + 1)*(M + 1); i++)
				Sigma_tt0_t[i] = (2 * mu + lambda) / r((int)(i*1.0 / (M + 1)))*(Vt0_dt[i] + Vr0[i]) + lambda*Vr0_dr[i] - (3 * lambda + 2 * mu)*const_alpha*T0_t[i] - taup_tt0_t[i];
		}

		// Step by time
//#pragma omp parallel shared (Sigma_rr0,Sigma_rt0,Sigma_tt0,Sigma_rr0_t,Sigma_rt0_t,Sigma_tt0_t) private (i)
		{
#pragma omp parallel for
			for (int i = 0; i <= N; i++){
				for (int j = 0; j <= M; j++){
					Sigma_rr0[i*(M + 1) + j] = Sigma_rr0[i*(M + 1) + j] + tau*Sigma_rr0_t[i*(M + 1) + j];
					Sigma_rt0[i*(M + 1) + j] = Sigma_rt0[i*(M + 1) + j] + tau*Sigma_rt0_t[i*(M + 1) + j];
					Sigma_tt0[i*(M + 1) + j] = Sigma_tt0[i*(M + 1) + j] + tau*Sigma_tt0_t[i*(M + 1) + j];
				}
			}
		}


		if (savesolution
			&& (!stepControl && (((((int)(t / tau)) % ((int)(T / tau / (sfreq_loc * T))) == 0)))
			  || stepControl && (((((int)(t / tau)) % ((int)(maxSteps / sfreq_loc)) == 0)))
			  || saveeverystep)
			||(savelast&&(k == k_total))){

			//if (myrank == 0) cout << (int)(t / T * 100 + 1) << "%  t = " << t << endl;

			// Saving solution to files
#if GO_MPI == 1

			MPI_Gatherv(Ur2 + displs[myrank], recvcounts[myrank], MPI_DOUBLE, Ur2test, recvcounts, displs, MPI_DOUBLE, 0, MPI_COMM_WORLD);
			if ((myrank == 0) && saveUrUt){
				string fname_str_r_mpi = "step" + itos(k) + "r_mpi.txt";
				save_grafic_3d_onD(D, Ur2test, (M + 1)*(N + 1), fname_str_r_mpi.c_str());
				if (printinfo) cout << "file " << fname_str_r_mpi << " was successfully created" << endl;
			}

			MPI_Gatherv(Ut2 + displs[myrank], recvcounts[myrank], MPI_DOUBLE, Ut2test, recvcounts, displs, MPI_DOUBLE, 0, MPI_COMM_WORLD);
			if ((myrank == 0) && saveUrUt){
				string fname_str_t_mpi = "step" + itos(k) + "t_mpi.txt";
				save_grafic_3d_onD(D, Ut2test, (M + 1)*(N + 1), fname_str_t_mpi.c_str());
				if (printinfo) cout << "file " << fname_str_t_mpi << " was successfully created" << endl;
			}

#else
			if (saveUrUt){
				if (myrank == 0){
					save_function_scalar(Vr0, D, (M + 1)*(N + 1), "r", k, printinfo);
					save_function_scalar(Vt0, D, (M + 1)*(N + 1), "t", k, printinfo);
				}
			}

#endif


			if (saveTensors){

				save_function_scalar(Sigma_rr0, D, (M + 1)*(N + 1), "sigma_rr", k, printinfo);
				save_function_scalar(Sigma_tt0, D, (M + 1)*(N + 1), "sigma_tt", k, printinfo);
				save_function_scalar(Sigma_rt0, D, (M + 1)*(N + 1), "sigma_rt", k, printinfo);

			}

			if (savePlastiqueTensors){
				save_function_scalar(S, D, (M + 1)*(N + 1), "S", k, printinfo);
				save_function_scalar(taup_rr0, D, (M + 1)*(N + 1), "taup_rr", k, printinfo);
				save_function_scalar(taup_tt0, D, (M + 1)*(N + 1), "taup_tt", k, printinfo);
				save_function_scalar(taup_rt0, D, (M + 1)*(N + 1), "taup_rt", k, printinfo);
			}

			if (saveInvariants){

				for (int i = 0; i < (N + 1)*(M + 1); i++)
					I1[i] = Sigma_rr0[i] + Sigma_tt0[i];

				for (int i = 0; i < (N + 1)*(M + 1); i++)
					Sigma_e[i] = absd(Sigma_rr0[i] - Sigma_tt0[i]);

				save_function_scalar(I1, D, (M + 1)*(N + 1), "I1", k, printinfo);
				save_function_scalar(Sigma_e, D, (M + 1)*(N + 1), "Sigma_e", k, printinfo);

			}

			if (saveTemperature)
				save_function_scalar(T0, D, (M + 1)*(N + 1), "T0", k, printinfo);


			if (saveDerivatives){
				save_function_scalar(Vr0_t, D, (M + 1)*(N + 1), "r_t", k, printinfo);
				save_function_scalar(Vt0_t, D, (M + 1)*(N + 1), "t_t", k, printinfo);

				save_function_scalar(Sigma_rr0_t, D, (M + 1)*(N + 1), "sigma_rr_t", k, printinfo);
				save_function_scalar(Sigma_rt0_t, D, (M + 1)*(N + 1), "sigma_rt_t", k, printinfo);
				save_function_scalar(Sigma_tt0_t, D, (M + 1)*(N + 1), "sigma_tt_t", k, printinfo);

				save_function_scalar(taup_rr0_t, D, (M + 1)*(N + 1), "taup_rr_t", k, printinfo);
				save_function_scalar(taup_rt0_t, D, (M + 1)*(N + 1), "taup_rt_t", k, printinfo);
				save_function_scalar(taup_tt0_t, D, (M + 1)*(N + 1), "taup_tt_t", k, printinfo);

				save_function_scalar(T0_t, D, (M + 1)*(N + 1), "T0_t", k, printinfo);

			}

		}

		// V swap
		//tempU = Vr1;
		//Vr1 = Vr0;
		//Vr0 = tempU;

		//tempU = Vt1;
		//Vt1 = Vt0;
		//Vt0 = tempU;

		// sigma_ij swap
		/*
		tempU = Sigma_rr1;
		Sigma_rr1 = Sigma_rr0;
		Sigma_rr0 = tempU;

		tempU = Sigma_rt1;
		Sigma_rt1 = Sigma_rt0;
		Sigma_rt0 = tempU;

		tempU = Sigma_tt1;
		Sigma_tt1 = Sigma_tt0;
		Sigma_tt0 = tempU;
		*/

		// tau^p_ij swap 
		//tempU = taup_rr1;
		//taup_rr1 = taup_rr0;
		//taup_rr0 = tempU;

		//tempU = taup_rt1;
		//taup_rt1 = taup_rt0;
		//taup_rt0 = tempU;

		//tempU = taup_tt1;
		//taup_tt1 = taup_tt0;
		//taup_tt0 = tempU;

		// T swap
		//tempU = T1;
		//T1 = T0;
		//T0 = tempU;

		// zero on boarder
		if (k > 1){
			for (int i = 0; i < N + 1; i++){
				for (int j = 0; j < M + 1; j++){

					//outer circle
					if ((i == N) && (true || ((j >= l1) && (j <= l2)))){

						// force only in the beginning
						//if( t > tau0 ) Sigma_rr0[i*(M + 1) + j] = 0.0;
						//else Sigma_rr0[i*(M + 1) + j] = Sigma1[i*(M + 1) + j];
						
						// constant force
						Sigma_rr0[i*(M + 1) + j] = Sigma1[i*(M + 1) + j];

						Sigma_rr1[i*(M + 1) + j] = 0.0;

						Sigma_rt0[i*(M + 1) + j] = 0.0;
						Sigma_rt1[i*(M + 1) + j] = 0.0;

						if (temperature_Newton) T0[i*(M + 1) + j] = (const_gamma*dense*const_T0 + T0[(i - 1)*(M + 1) + j]) / (1 + const_gamma*dense);

						if ( temperature_Dirihle ) T0[i*(M + 1) + j] = const_T0;

					}

					// inner circle
					if (i == 0){
						//Sigma_rr0[i*(M + 1) + j] = 0.0;
						//Sigma_rr1[i*(M + 1) + j] = 0.0;

						//Sigma_rt0[i*(M + 1) + j] = 0.0;
						//Sigma_rt1[i*(M + 1) + j] = 0.0;

						if (temperature_Newton) T0[i*(M + 1) + j] = (const_gamma*dense*const_T0 + T0[(i + 1)*(M + 1) + j]) / (1 + const_gamma*dense);

						if ( temperature_Dirihle ) T0[i*(M + 1) + j] = const_T0;

					}

					// side lines
					if (j == 0){

						if( !nocut ){
						  //Sigma_tt0[i*(M + 1) + j] = 0.0;
						  //Sigma_rt0[i*(M + 1) + j] = 0.0;
						}else{
  						  //Sigma_tt0[i*(M + 1) + j] = 0.0;
						  //Sigma_rt0[i*(M + 1) + j] = 0.0;

						}

						if( temperature_Newton ) T0[i*(M + 1) + j] = (const_gamma*dense*const_T0 + T0[i*(M + 1) + j+1]) / (1 + const_gamma*dense);

						if ( temperature_Dirihle ) T0[i*(M + 1) + j] = const_T0;

						if( nocut ) T0[i*(M + 1) + j] = const_T0;

					}

					if (j == M){

						if( !nocut ){
						  //Sigma_tt0[i*(M + 1) + j] = 0.0;
						  //Sigma_rt0[i*(M + 1) + j] = 0.0;
						}else{
						  //Sigma_tt0[i*(M + 1) + j] = 0.0;
						  //Sigma_rt0[i*(M + 1) + j] = 0.0;
						}

						if ( temperature_Newton ) T0[i*(M + 1) + j] = (const_gamma*dense*const_T0 + T0[(i - 1)*(M + 1) + j - 1]) / (1 + const_gamma*dense);

						if ( temperature_Dirihle ) T0[i*(M + 1) + j] = const_T0;

						if ( nocut ) T0[i*(M + 1) + j] = const_T0;

					}



/*
					if ( temperature_Dirihle ){
						if ((i == 0) || (i == N) || (j == 0) || (j == M)){
							T0[i*(M + 1) + j] = const_T0;
							T1[i*(M + 1) + j] = const_T0;
						}
					}
*/
				}
			}
		}

		// DATA EXCHANGE
		//
#if GO_MPI == 1
		if (paral && (cs == 1)){

			int count = L*s*(M + 1);
			MPI_Status status;

			// I
			if (myrank > 0){ // (1)
				MPI_Send(Ur1 + ib*(M + 1), count, MPI_DOUBLE, myrank - 1, 1, MPI_COMM_WORLD);
				MPI_Send(Ut1 + ib*(M + 1), count, MPI_DOUBLE, myrank - 1, 1, MPI_COMM_WORLD);
			}

			if (myrank < np - 1){ // (2)
				MPI_Recv(Ur1 + (ie + 1)*(M + 1), count, MPI_DOUBLE, myrank + 1, 1, MPI_COMM_WORLD, &status);
				MPI_Recv(Ut1 + (ie + 1)*(M + 1), count, MPI_DOUBLE, myrank + 1, 1, MPI_COMM_WORLD, &status);
			}

			// II
			if (myrank < np - 1){ // (3)
				MPI_Send(Ur1 + (ie - s*L + 1)*(M + 1), count, MPI_DOUBLE, myrank + 1, 2, MPI_COMM_WORLD);
				MPI_Send(Ut1 + (ie - s*L + 1)*(M + 1), count, MPI_DOUBLE, myrank + 1, 2, MPI_COMM_WORLD);
			}

			if (myrank > 0){ // (4)
				MPI_Recv(Ur1 + (ib - s*L)*(M + 1), count, MPI_DOUBLE, myrank - 1, 2, MPI_COMM_WORLD, &status);
				MPI_Recv(Ut1 + (ib - s*L)*(M + 1), count, MPI_DOUBLE, myrank - 1, 2, MPI_COMM_WORLD, &status);
			}

			MPI_Barrier(MPI_COMM_WORLD);

		}
#endif

		int imax;
		
		Tmax[k] = Fmax(T0,(N+1)*(M+1),&imax);
		TmaxRT[k*2] = r((int)(imax*1.0 / (M + 1)));
		TmaxRT[k * 2+1] = thetta(imax - (M + 1)*((int)(imax*1.0 / (M + 1))));
		Dtime[k] = t;

		k++;
	}

	cout << "k_end = " << k << endl;

	int imax;
	double maxT = Fmax(Tmax, k_total + 2, &imax);
	cout << "angle = " << alpha / 2 / M_PI * 360*2 << endl;
	cout << "Tmax = " << maxT << " in " << Dtime[imax] << " at (" << TmaxRT[2 * imax] << "," << TmaxRT[2 * imax+1]<< ")" << endl;

	if ((myrank == 0)&&savesolution)
		save_grafic_2d_on01(Tmax, k_total, "Tmax.txt");

	//save_grafic_1val_onD(Dtime, Tmax, k_total, "Tmax_onT.txt");
	//save_grafic_2val_onD(Dtime, TmaxRT, k_total, "Tmax_onT_RT.txt");

	//save_grafic_3d_onD(D, U1, (M + 1)*(N + 1), "stepLast.txt");


	/*
	delete[](D);
	delete[](Vr0);
	delete[](Vt0);
	delete[](Ur0);
	delete[](Ur1);
	delete[](Ur2);
	delete[](Ut0);
	delete[](Ut1);
	delete[](Ut2);
	*/

	return 0;

}


int tuvp_direct_2d_now_shar(int mode){
	using namespace std;

	double *D;

	int myrank = 0, np = 1;

#if GO_MPI == 1
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	MPI_Comm_size(MPI_COMM_WORLD, &np);
#endif

	bool savesolution = true;
	bool printinfo = true;

	bool saveUrUt = false;
	bool saveTensors = true;
	bool savePlastiqueTensors = true;
	bool saveInvariants = true;
	bool saveTemperature = true;

	bool saveDerivatives = true;

	bool saveeverystep = false;

	bool stepControl = false;
	int maxSteps = 15;

	if (mode == EVERYDAY){
		savesolution = true;
		printinfo = true;
	}

	//params a;
	//init_params(a);
	if ((myrank == 0) && printinfo) print_params(a);

	double alpha = a.alpha;
	double betta = a.betta;
	double R = a.R;
	double epsilon = a.epsilon;
	double dense = a.dense;
	double mu = a.mu;
	double lambda = a.lambda;
	double const_tau = a.const_tau;
	double const_ks0 = a.const_ks0;
	double const_T0 = a.const_T0;
	double const_ce = a.const_ce;
	double const_k = a.const_k;
	double const_alpha = a.const_alpha;

	bool temperature_Newton = a.temperature_Newton;
	bool temperature_Dirihle = a.temperature_Dirihle;
	double const_gamma = a.const_gamma;

	double T = a.T;

	int N = a.N;
	int M = a.M;

	double tau0 = a.const_tau0;
	double r0 = a.const_r0;

	double tau = a.tau;

	double const_Sigma10 = a.const_Sigma10;

	double dl1 = (M_PI - betta);
	double dl2 = (M_PI + betta);

	int sfreq = a.sfreq;
	int sfreq_loc = sfreq;

	//int l1 = (dl1 - grho*grho*N2)/grho + N2;
	//int l2 = (dl2 - grho*grho*N2)/grho + N2;

	//double l1 = (M_PI - betta - alpha) / gksi;
	//double l2 = (M_PI + betta - alpha) / gksi;

	int l1 = trunc_thetta(dl1);
	int l2 = trunc_thetta(dl2);

	if ((myrank == 0) && printinfo) cout << "l1 = " << l1 << endl;
	if ((myrank == 0) && printinfo) cout << "l2 = " << l2 << endl;
	if ((myrank == 0) && printinfo) cout << "k = " << const_k << endl;
	if ((myrank == 0) && printinfo) cout << "alpha = " << const_alpha << endl;
	if ((myrank == 0) && printinfo) cout << "gamma = " << const_gamma << endl;
	if ((myrank == 0) && printinfo) cout << "Sigma10 = " << const_Sigma10 << endl;
	if ((myrank == 0) && printinfo) cout << "P0 = " << a.const_P0_razm / 1000000.0 << "MPa" << endl;
	if ((myrank == 0) && printinfo) cout << "tau0 = " << a.const_tau0_razm*1000000.0 << " mks" << endl;
	if ((myrank == 0) && printinfo) cout << "r0 = " << a.const_r0_razm << " m" << endl;
	if ((myrank == 0) && printinfo) cout << "F0 = " << a.const_F0_razm << " N" << endl;



	D = new double[2 * (N + 1)*(M + 1)];

	// V, V_t
	double *Vr0 = new double[(N + 1)*(M + 1)];
	double *Vt0 = new double[(N + 1)*(M + 1)];
	double *Vr0_t = new double[(N + 1)*(M + 1)];
	double *Vt0_t = new double[(N + 1)*(M + 1)];
	double *Vr0_dr = new double[(N + 1)*(M + 1)];
	double *Vr0_dt = new double[(N + 1)*(M + 1)];
	double *Vt0_dr = new double[(N + 1)*(M + 1)];
	double *Vt0_dt = new double[(N + 1)*(M + 1)];
	double *rVr0 = new double[(N + 1)*(M + 1)];
	double *rVr0_dr = new double[(N + 1)*(M + 1)];
	double *VtSINt0 = new double[(N + 1)*(M + 1)];

	double *Vr1 = new double[(N + 1)*(M + 1)];
	double *Vt1 = new double[(N + 1)*(M + 1)];

	// \sigma_ij
	double *Sigma_rr0 = new double[(N + 1)*(M + 1)];
	double *Sigma_tt0 = new double[(N + 1)*(M + 1)];
	double *Sigma_rt0 = new double[(N + 1)*(M + 1)];
	double *Sigma_rr0_t = new double[(N + 1)*(M + 1)];
	double *Sigma_tt0_t = new double[(N + 1)*(M + 1)];
	double *Sigma_rt0_t = new double[(N + 1)*(M + 1)];
	double *Sigma_rr0_dr = new double[(N + 1)*(M + 1)];
	double *Sigma_rt0_dt = new double[(N + 1)*(M + 1)];
	double *Sigma_rt0_dr = new double[(N + 1)*(M + 1)];
	double *Sigma_tt0_dt = new double[(N + 1)*(M + 1)];

	double *Sigma_rr1 = new double[(N + 1)*(M + 1)];
	double *Sigma_tt1 = new double[(N + 1)*(M + 1)];
	double *Sigma_rt1 = new double[(N + 1)*(M + 1)];

	// T, T_dt
	double *T0 = new double[(N + 1)*(M + 1)];
	double *T1 = new double[(N + 1)*(M + 1)];
	double *T0_t = new double[(N + 1)*(M + 1)];
	double *T0_dtdt = new double[(N + 1)*(M + 1)];
	double *T0_drrdr = new double[(N + 1)*(M + 1)];
	double *T0_dt = new double[(N + 1)*(M + 1)];
	double *T0_dr = new double[(N + 1)*(M + 1)];

	// \tau^p_ij
	double *taup_rr0 = new double[(N + 1)*(M + 1)];
	double *taup_tt0 = new double[(N + 1)*(M + 1)];
	double *taup_rt0 = new double[(N + 1)*(M + 1)];
	double *taup_rr0_t = new double[(N + 1)*(M + 1)];
	double *taup_rt0_t = new double[(N + 1)*(M + 1)];
	double *taup_tt0_t = new double[(N + 1)*(M + 1)];

	double *taup_rr1 = new double[(N + 1)*(M + 1)];
	double *taup_tt1 = new double[(N + 1)*(M + 1)];
	double *taup_rt1 = new double[(N + 1)*(M + 1)];

	double *s_rr = new double[(N + 1)*(M + 1)];
	double *s_rt = new double[(N + 1)*(M + 1)];
	double *s_tt = new double[(N + 1)*(M + 1)];
	double *S = new double[(N + 1)*(M + 1)];

	// I1, \sigma_e
	double *I1 = new double[(N + 1)*(M + 1)];
	double *Sigma_e = new double[(N + 1)*(M + 1)];

	// \Sigma^1
	double *Sigma1 = new double[(N + 1)*(M + 1)];

	//double *Sigma_rr2 = new double[(N + 1)*(M + 1)];
	//double *Sigma_tt2 = new double[(N + 1)*(M + 1)];
	//double *Sigma_rt2 = new double[(N + 1)*(M + 1)];
	//double* Vr1 = new double[(N + 1)*(M + 1)];
	//double* Vt1 = new double[(N + 1)*(M + 1)];
	//Ur0 = new double[(N + 1)*(M + 1)];
	//Ur1 = new double[(N + 1)*(M + 1)];
	//Ur2 = new double[(N + 1)*(M + 1)];
	//Ut0 = new double[(N + 1)*(M + 1)];
	//Ut1 = new double[(N + 1)*(M + 1)];
	//Ut2 = new double[(N + 1)*(M + 1)];


	//double *Ur2test = new double[(N + 1)*(M + 1)];
	//double *Ut2test = new double[(N + 1)*(M + 1)];



	// Domain
	for (int i = 0; i <= N; i++){
		for (int j = 0; j <= M; j++){
			D[(i*(M + 1) + j) * 2] = r(i);
			D[(i*(M + 1) + j) * 2 + 1] = thetta(j);
			//D[(i*(M + 1) + j) * 2] = i*grho + epsilon;
			//D[(i*(M + 1) + j) * 2 + 1] = j*gksi + alpha;
		}
	}


	// Initial force Sigma^1
	for (int i = 0; i < N + 1; i++){
		for (int j = 0; j < M + 1; j++){
			//if (((r(i) >= R - r0) && (i <= N)) && ((j >= l1) && (j <= l2))){
			if ((i >= N) && ((j >= l1) && (j <= l2))){
				Sigma1[i*(M + 1) + j] = const_Sigma10;
			}
			else
				Sigma1[i*(M + 1) + j] = 0.0;
		}
	}

	// Initial conditions V
	for (int i = 0; i < (M + 1)*(N + 1); i++){
		Vr1[i] = 0.0;
		Vt1[i] = 0.0;

		Vr0[i] = 0.0;
		Vt0[i] = 0.0;
	}

	// Initial conditions \tau^p_ij
	for (int i = 0; i < (M + 1)*(N + 1); i++){
		taup_rr0[i] = 0.0;
		taup_rt0[i] = 0.0;
		taup_tt0[i] = 0.0;

		taup_rr1[i] = 0.0;
		taup_rt1[i] = 0.0;
		taup_tt1[i] = 0.0;
	}

	// Initial conditions \sigma_ij
	for (int i = 0; i < (N + 1)*(M + 1); i++){
		Sigma_rr1[i] = Sigma1[i];// 0.0;// Sigma1[i] * 0.006 / tau * a.grho / 0.018;
		Sigma_tt1[i] = 0.0;
		Sigma_rt1[i] = 0.0;

		Sigma_rr0[i] = Sigma1[i];// Sigma1[i] * 0.006 / tau * a.grho / 0.018;
		Sigma_tt0[i] = 0.0;
		Sigma_rt0[i] = 0.0;
	}

	// Initial conditions T
	for (int i = 0; i < (M + 1)*(N + 1); i++){
		T0[i] = const_T0;
		T1[i] = const_T0;
	}

	// Derivatives = 0
	for (int i = 0; i < (M + 1)*(N + 1); i++){
		Vr0_t[i] = 0.0;
		Vt0_t[i] = 0.0;
		taup_rr0_t[i] = 0.0;
		taup_rt0_t[i] = 0.0;
		taup_tt0_t[i] = 0.0;
		Sigma_rr0_t[i] = 0.0;
		Sigma_tt0_t[i] = 0.0;
		Sigma_rt0_t[i] = 0.0;
		T0_t[i] = 0.0;
	}

	if ((myrank == 0) && savesolution){

		//save_grafic_3d_onD(D, Sigma1, (M + 1)*(N + 1), "step0Sigma1.txt");

		save_function_scalar(Sigma1, D, (M + 1)*(N + 1), "Sigma1", 0, printinfo);

		//save_grafic_3d_onD(D, Vr0, (M + 1)*(N + 1), "step0r.txt");
		//save_grafic_3d_onD(D, Vt0, (M + 1)*(N + 1), "step0t.txt");

		save_function_scalar(Vr0, D, (M + 1)*(N + 1), "r", 0, printinfo);
		save_function_scalar(Vt0, D, (M + 1)*(N + 1), "t", 0, printinfo);

		if (saveTensors){

			save_function_scalar(Sigma_rr0, D, (M + 1)*(N + 1), "sigma_rr", 0, printinfo);
			save_function_scalar(Sigma_tt0, D, (M + 1)*(N + 1), "sigma_tt", 0, printinfo);
			save_function_scalar(Sigma_rt0, D, (M + 1)*(N + 1), "sigma_rt", 0, printinfo);

		}

		if (savePlastiqueTensors){

			save_function_scalar(taup_rr0, D, (M + 1)*(N + 1), "taup_rr", 0, printinfo);
			save_function_scalar(taup_tt0, D, (M + 1)*(N + 1), "taup_tt", 0, printinfo);
			save_function_scalar(taup_rt0, D, (M + 1)*(N + 1), "taup_rt", 0, printinfo);

		}

		if (saveInvariants){

			for (int i = 0; i < (N + 1)*(M + 1); i++)
				I1[i] = Sigma_rr0[i] + Sigma_tt0[i];

			for (int i = 0; i < (N + 1)*(M + 1); i++)
				Sigma_e[i] = absd(Sigma_rr0[i] - Sigma_tt0[i]);

			save_function_scalar(I1, D, (M + 1)*(N + 1), "I1", 0, printinfo);
			save_function_scalar(Sigma_e, D, (M + 1)*(N + 1), "Sigma_e", 0, printinfo);

		}

		if (saveTemperature)
			save_function_scalar(T0, D, (M + 1)*(N + 1), "T0", 0, printinfo);

	}


	double timesumr, timesumt, timemul;
	int k = 1;
	double* tempU;
	double eps = 0.0;

	for (double t = 0.0; ((t < T) && (!stepControl)) || ((k < maxSteps) && stepControl); t += tau){

		// Step t-1
		dF_dr(Sigma_rr0_dr, Sigma_rr0, a);
		dF_dt(Sigma_tt0_dt, Sigma_tt0, a);
		dF_dr(Sigma_rt0_dr, Sigma_rt0, a);
		dF_dt(Sigma_rt0_dt, Sigma_rt0, a);

		for (int i = 0; i < (N + 1)*(M + 1); i++){
			Vr0_t[i] = (1.0 / dense)*Sigma_rr0_dr[i] + 1.0 / dense / r((int)(i*1.0 / (M + 1))) * ( Sigma_rt0_dt[i] 
				+ 2 * Sigma_rr0[i] - Sigma_tt0[i] + Sigma_rr0[i] * ctg(thetta(i-(M+1)*((int)(i*1.0 / (M + 1))) )) );
		}

		for (int i = 0; i < (N + 1)*(M + 1); i++){
			Vt0_t[i] = (1.0 / dense)*Sigma_rt0_dr[i] + 1.0 / dense / r((int)(i*1.0 / (M + 1))) * (Sigma_tt0_dt[i] 
				+ Sigma_tt0[i] * ctg(thetta(i - (M + 1)*((int)(i*1.0 / (M + 1))))) + 3 * Sigma_rt0[i]);
		}

		// Step by time
		for (int i = 0; i <= N; i++){
			for (int j = 0; j <= M; j++){
				Vr0[i*(M + 1) + j] = Vr0[i*(M + 1) + j] + tau*Vr0_t[i*(M + 1) + j];
				Vt0[i*(M + 1) + j] = Vt0[i*(M + 1) + j] + tau*Vt0_t[i*(M + 1) + j];
			}
		}


		// Step t-2
		for (int i = 0; i < (N + 1)*(M + 1); i++)
			rVr0[i] = r((int)(i*1.0 / (M + 1)))*r((int)(i*1.0 / (M + 1))) * Vr0[i];
		dF_dr(rVr0_dr, rVr0, a);

		for (int i = 0; i < (N + 1)*(M + 1); i++)
			VtSINt0[i] = sin(thetta(i - (M + 1)*((int)(i*1.0 / (M + 1))))) * Vt0[i];
		dF_dt(Vt0_dt, VtSINt0, a);

		dF_dr(T0_dr, T0, a);
		for (int i = 0; i < (N + 1)*(M + 1); i++)
			T0_dr[i] = r((int)(i*1.0 / (M + 1)))*r((int)(i*1.0 / (M + 1))) * T0_dr[i];
		dF_dr(T0_drrdr, T0_dr, a);

		dF_dt(T0_dt, T0, a);
		for (int i = 0; i < (N + 1)*(M + 1); i++)
			T0_dt[i] = sin(thetta(i - (M + 1)*((int)(i*1.0 / (M + 1))))) * T0_dt[i];
		dF_dt(T0_dtdt, T0_dt, a);

		double rc, thettac;
		for (int i = 0; i < (N + 1)*(M + 1); i++){
			rc = r((int)(i*1.0 / (M + 1)));
			thettac = thetta(i - (M + 1)*((int)(i*1.0 / (M + 1))));
			T0_t[i] = (1.0 / dense / const_ce) * (
				const_k * (1.0 / rc/rc * T0_drrdr[i] + 1.0 / rc / rc/sin(thettac)*T0_dtdt[i])
				- (3 * lambda + 2 * mu) * const_T0 * const_alpha * (1.0 / rc/rc*rVr0_dr[i] + 1.0 / rc/sinn(thettac)*Vt0_dt[i])
				+ (Sigma_rr0[i] * taup_rr0_t[i] + Sigma_tt0[i] * taup_tt0_t[i] + 2.0*Sigma_rt0[i] * taup_rt0_t[i]) / 2.0 / mu
				);
		}

		// Step by time
		for (int i = 0; i <= N; i++){
			for (int j = 0; j <= M; j++){
				T0[i*(M + 1) + j] = T0[i*(M + 1) + j] + 1.0 * tau*T0_t[i*(M + 1) + j];
			}
		}


		// Step t-3
		double tr, ks, PHI, arg;

		for (int i = 0; i < (N + 1)*(M + 1); i++){
			tr = Sigma_rr0[i] + taup_rr0[i] + Sigma_tt0[i] + taup_tt0[i];
			s_rr[i] = Sigma_rr0[i] + taup_rr0[i] - tr / 3.0;
			s_tt[i] = Sigma_tt0[i] + taup_tt0[i] - tr / 3.0;
			s_rt[i] = Sigma_rt0[i] + taup_rt0[i];
			S[i] = sqrt(s_rr[i] * s_rr[i] + s_tt[i] * s_tt[i] + 2 * s_rt[i] * s_rt[i]);

			// function k_s(T)
			ks = a.const_ks0;// +f(T);

			// function PHI(S-k_s(T))
			arg = S[i] - ks;
			if (arg >= 0)
				PHI = arg;
			else
				PHI = 0.0;

			if (S[i] > 0.00000000001){
				taup_rr0_t[i] = 2 * mu / const_tau * PHI / S[i] * s_rr[i];
				taup_rt0_t[i] = 2 * mu / const_tau * PHI / S[i] * s_rt[i];
				taup_tt0_t[i] = 2 * mu / const_tau * PHI / S[i] * s_tt[i];
			}
			else{
				taup_rr0_t[i] = 0.0;
				taup_rt0_t[i] = 0.0;
				taup_tt0_t[i] = 0.0;
			}
		}

		// Step by time
		for (int i = 0; i <= N; i++){
			for (int j = 0; j <= M; j++){
				taup_rr0[i*(M + 1) + j] = taup_rr0[i*(M + 1) + j] + tau*taup_rr0_t[i*(M + 1) + j];
				taup_rt0[i*(M + 1) + j] = taup_rt0[i*(M + 1) + j] + tau*taup_rt0_t[i*(M + 1) + j];
				taup_tt0[i*(M + 1) + j] = taup_tt0[i*(M + 1) + j] + tau*taup_tt0_t[i*(M + 1) + j];
			}
		}


		// Step t-4
		dF_dr(Vr0_dr, Vr0, a);
		dF_dt(Vr0_dt, Vr0, a);
		dF_dr(Vt0_dr, Vt0, a);
		dF_dt(Vt0_dt, Vt0, a);

		for (int i = 0; i < (N + 1)*(M + 1); i++)
			Sigma_rr0_t[i] = (2 * mu + lambda)*Vr0_dr[i] + lambda / r((int)(i*1.0 / (M + 1)))*(Vt0_dt[i] + Vr0[i]) - (3 * lambda + 2 * mu)*const_alpha*T0_t[i] - taup_rr0_t[i];

		for (int i = 0; i < (N + 1)*(M + 1); i++)
			Sigma_rt0_t[i] = mu * (1.0 / r((int)(i*1.0 / (M + 1))) * Vr0_dt[i] + Vt0_dr[i] - 1.0 / r((int)(i*1.0 / (M + 1))) * Vt0[i]) - taup_rt0_t[i];

		for (int i = 0; i < (N + 1)*(M + 1); i++)
			Sigma_tt0_t[i] = (2 * mu + lambda) / r((int)(i*1.0 / (M + 1)))*(Vt0_dt[i] + Vr0[i]) + lambda*Vr0_dr[i] - (3 * lambda + 2 * mu)*const_alpha*T0_t[i] - taup_tt0_t[i];

		// Step by time
		for (int i = 0; i <= N; i++){
			for (int j = 0; j <= M; j++){
				Sigma_rr0[i*(M + 1) + j] = Sigma_rr0[i*(M + 1) + j] + tau*Sigma_rr0_t[i*(M + 1) + j];
				Sigma_rt0[i*(M + 1) + j] = Sigma_rt0[i*(M + 1) + j] + tau*Sigma_rt0_t[i*(M + 1) + j];
				Sigma_tt0[i*(M + 1) + j] = Sigma_tt0[i*(M + 1) + j] + tau*Sigma_tt0_t[i*(M + 1) + j];
			}
		}



		if (savesolution
			&& (!stepControl && (((((int)(t / tau)) % ((int)(T / tau / (sfreq_loc * T))) == 0)))
			|| stepControl && (((((int)(t / tau)) % ((int)(maxSteps / sfreq_loc)) == 0)))
			|| saveeverystep)){

			//if (myrank == 0) cout << (int)(t / T * 100 + 1) << "%  t = " << t << endl;

			// Saving solution to files
#if GO_MPI == 1

			MPI_Gatherv(Ur2 + displs[myrank], recvcounts[myrank], MPI_DOUBLE, Ur2test, recvcounts, displs, MPI_DOUBLE, 0, MPI_COMM_WORLD);
			if ((myrank == 0) && saveUrUt){
				string fname_str_r_mpi = "step" + itos(k) + "r_mpi.txt";
				save_grafic_3d_onD(D, Ur2test, (M + 1)*(N + 1), fname_str_r_mpi.c_str());
				if (printinfo) cout << "file " << fname_str_r_mpi << " was successfully created" << endl;
			}

			MPI_Gatherv(Ut2 + displs[myrank], recvcounts[myrank], MPI_DOUBLE, Ut2test, recvcounts, displs, MPI_DOUBLE, 0, MPI_COMM_WORLD);
			if ((myrank == 0) && saveUrUt){
				string fname_str_t_mpi = "step" + itos(k) + "t_mpi.txt";
				save_grafic_3d_onD(D, Ut2test, (M + 1)*(N + 1), fname_str_t_mpi.c_str());
				if (printinfo) cout << "file " << fname_str_t_mpi << " was successfully created" << endl;
			}

#else
			if (saveUrUt){
				if (myrank == 0){
					save_function_scalar(Vr0, D, (M + 1)*(N + 1), "r", k, printinfo);
					save_function_scalar(Vt0, D, (M + 1)*(N + 1), "t", k, printinfo);
				}
			}

#endif


			if (saveTensors){

				save_function_scalar(Sigma_rr0, D, (M + 1)*(N + 1), "sigma_rr", k, printinfo);
				save_function_scalar(Sigma_tt0, D, (M + 1)*(N + 1), "sigma_tt", k, printinfo);
				save_function_scalar(Sigma_rt0, D, (M + 1)*(N + 1), "sigma_rt", k, printinfo);

			}

			if (savePlastiqueTensors){
				save_function_scalar(S, D, (M + 1)*(N + 1), "S", k, printinfo);
				save_function_scalar(taup_rr0, D, (M + 1)*(N + 1), "taup_rr", k, printinfo);
				save_function_scalar(taup_tt0, D, (M + 1)*(N + 1), "taup_tt", k, printinfo);
				save_function_scalar(taup_rt0, D, (M + 1)*(N + 1), "taup_rt", k, printinfo);
			}

			if (saveInvariants){

				for (int i = 0; i < (N + 1)*(M + 1); i++)
					I1[i] = Sigma_rr0[i] + Sigma_tt0[i];

				for (int i = 0; i < (N + 1)*(M + 1); i++)
					Sigma_e[i] = absd(Sigma_rr0[i] - Sigma_tt0[i]);

				save_function_scalar(I1, D, (M + 1)*(N + 1), "I1", k, printinfo);
				save_function_scalar(Sigma_e, D, (M + 1)*(N + 1), "Sigma_e", k, printinfo);

			}

			if (saveTemperature)
				save_function_scalar(T0, D, (M + 1)*(N + 1), "T0", k, printinfo);


			if (saveDerivatives){
				save_function_scalar(Vr0_t, D, (M + 1)*(N + 1), "r_t", k, printinfo);
				save_function_scalar(Vt0_t, D, (M + 1)*(N + 1), "t_t", k, printinfo);

				save_function_scalar(Sigma_rr0_t, D, (M + 1)*(N + 1), "sigma_rr_t", k, printinfo);
				save_function_scalar(Sigma_rt0_t, D, (M + 1)*(N + 1), "sigma_rt_t", k, printinfo);
				save_function_scalar(Sigma_tt0_t, D, (M + 1)*(N + 1), "sigma_tt_t", k, printinfo);

				save_function_scalar(taup_rr0_t, D, (M + 1)*(N + 1), "taup_rr_t", k, printinfo);
				save_function_scalar(taup_rt0_t, D, (M + 1)*(N + 1), "taup_rt_t", k, printinfo);
				save_function_scalar(taup_tt0_t, D, (M + 1)*(N + 1), "taup_tt_t", k, printinfo);

				save_function_scalar(T0_t, D, (M + 1)*(N + 1), "T0_t", k, printinfo);

			}

		}

		// V swap
		//tempU = Vr1;
		//Vr1 = Vr0;
		//Vr0 = tempU;

		//tempU = Vt1;
		//Vt1 = Vt0;
		//Vt0 = tempU;

		// sigma_ij swap
		/*
		tempU = Sigma_rr1;
		Sigma_rr1 = Sigma_rr0;
		Sigma_rr0 = tempU;

		tempU = Sigma_rt1;
		Sigma_rt1 = Sigma_rt0;
		Sigma_rt0 = tempU;

		tempU = Sigma_tt1;
		Sigma_tt1 = Sigma_tt0;
		Sigma_tt0 = tempU;
		*/

		// tau^p_ij swap 
		//tempU = taup_rr1;
		//taup_rr1 = taup_rr0;
		//taup_rr0 = tempU;

		//tempU = taup_rt1;
		//taup_rt1 = taup_rt0;
		//taup_rt0 = tempU;

		//tempU = taup_tt1;
		//taup_tt1 = taup_tt0;
		//taup_tt0 = tempU;

		// T swap
		//tempU = T1;
		//T1 = T0;
		//T0 = tempU;

		// zero on boarder
		if (k > 1){
			for (int i = 0; i < N + 1; i++){
				for (int j = 0; j < M + 1; j++){

					//outer circle
					if ((i == N) && (true || ((j >= l1) && (j <= l2)))){
						if (t > tau0) Sigma_rr0[i*(M + 1) + j] = 0.0;
						else Sigma_rr0[i*(M + 1) + j] = Sigma1[i*(M + 1) + j];
						Sigma_rr1[i*(M + 1) + j] = 0.0;

						Sigma_rt0[i*(M + 1) + j] = 0.0;
						Sigma_rt1[i*(M + 1) + j] = 0.0;

						if (temperature_Newton) T0[i*(M + 1) + j] = (const_gamma*dense*const_T0 + T0[(i - 1)*(M + 1) + j]) / (1 + const_gamma*dense);
					}

					// inner circle
					if (i == 0){
						Sigma_rr0[i*(M + 1) + j] = 0.0;
						Sigma_rr1[i*(M + 1) + j] = 0.0;

						Sigma_rt0[i*(M + 1) + j] = 0.0;
						Sigma_rt1[i*(M + 1) + j] = 0.0;

						if (temperature_Newton) T0[i*(M + 1) + j] = (const_gamma*dense*const_T0 + T0[(i + 1)*(M + 1) + j]) / (1 + const_gamma*dense);
					}

					// side lines
					if (j == 0){
						Sigma_tt0[i*(M + 1) + j] = 0.0;
						Sigma_tt1[i*(M + 1) + j] = 0.0;

						Sigma_rt0[i*(M + 1) + j] = 0.0;
						Sigma_rt1[i*(M + 1) + j] = 0.0;

						if (temperature_Newton) T0[i*(M + 1) + j] = (const_gamma*dense*const_T0 + T0[i*(M + 1) + j + 1]) / (1 + const_gamma*dense);
					}

					if (j == M){
						Sigma_tt0[i*(M + 1) + j] = 0.0;
						Sigma_tt1[i*(M + 1) + j] = 0.0;

						Sigma_rt0[i*(M + 1) + j] = 0.0;
						Sigma_rt1[i*(M + 1) + j] = 0.0;

						if (temperature_Newton) T0[i*(M + 1) + j] = (const_gamma*dense*const_T0 + T0[(i - 1)*(M + 1) + j - 1]) / (1 + const_gamma*dense);
					}


					if (temperature_Dirihle){
						if ((i == 0) || (i == N) || (j == 0) || (j == M)){
							T0[i*(M + 1) + j] = const_T0;
							T1[i*(M + 1) + j] = const_T0;
						}
					}

				}
			}
		}

		// DATA EXCHANGE
		//
#if GO_MPI == 1
		if (paral && (cs == 1)){

			int count = L*s*(M + 1);
			MPI_Status status;

			// I
			if (myrank > 0){ // (1)
				MPI_Send(Ur1 + ib*(M + 1), count, MPI_DOUBLE, myrank - 1, 1, MPI_COMM_WORLD);
				MPI_Send(Ut1 + ib*(M + 1), count, MPI_DOUBLE, myrank - 1, 1, MPI_COMM_WORLD);
			}

			if (myrank < np - 1){ // (2)
				MPI_Recv(Ur1 + (ie + 1)*(M + 1), count, MPI_DOUBLE, myrank + 1, 1, MPI_COMM_WORLD, &status);
				MPI_Recv(Ut1 + (ie + 1)*(M + 1), count, MPI_DOUBLE, myrank + 1, 1, MPI_COMM_WORLD, &status);
			}

			// II
			if (myrank < np - 1){ // (3)
				MPI_Send(Ur1 + (ie - s*L + 1)*(M + 1), count, MPI_DOUBLE, myrank + 1, 2, MPI_COMM_WORLD);
				MPI_Send(Ut1 + (ie - s*L + 1)*(M + 1), count, MPI_DOUBLE, myrank + 1, 2, MPI_COMM_WORLD);
			}

			if (myrank > 0){ // (4)
				MPI_Recv(Ur1 + (ib - s*L)*(M + 1), count, MPI_DOUBLE, myrank - 1, 2, MPI_COMM_WORLD, &status);
				MPI_Recv(Ut1 + (ib - s*L)*(M + 1), count, MPI_DOUBLE, myrank - 1, 2, MPI_COMM_WORLD, &status);
			}

			MPI_Barrier(MPI_COMM_WORLD);

		}
#endif



		k++;
	}

	//save_grafic_3d_onD(D, U1, (M + 1)*(N + 1), "stepLast.txt");



	delete[](D);
	delete[](Vr0);
	delete[](Vt0);
	delete[](Ur0);
	delete[](Ur1);
	delete[](Ur2);
	delete[](Ut0);
	delete[](Ut1);
	delete[](Ut2);

	return 0;

}




int mpi_estimate(){
  using namespace std;

int myrank = 0, np = 1;

#if GO_MPI == 1
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	MPI_Comm_size(MPI_COMM_WORLD, &np);
#endif

  double aclock1, aclock2;

  for( int i = 100; i <= 1000; i = i + 100){

    init_params( a, i);

    aclock1 = mclock();

    elastcyl_2d( MPIESTIMATE );

    aclock2 = mclock();
    if (myrank == 0) cout << "Total time: " << diffclock(aclock1, aclock2) << "s" << endl << endl;

  }

  return 0;

}

int main(int argc, char* argv[])
{
	using namespace std;

	int myrank = 0, np = 1;

#if GO_MPI == 1
	MPI_Init(&argc, &argv);
#endif

#if GO_MPI == 1
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	MPI_Comm_size(MPI_COMM_WORLD, &np);
#endif

	if (myrank == 0) cout << "number of processes = " << np << endl;

	double aclock1, aclock2;

	init_params( a );

	aclock1 = mclock();

	// EVERYDAY - everyday use
	// FILEOUT - full file output
	// TEXTOUT - full comments output
	// MPIESTIMATE - MPI performance estimation
	//elastcyl_2d( EVERYDAY );
	//elast_direct_2d( EVERYDAY );
	//tuvp_direct_2d( EVERYDAY );
	//tuvp_direct_2d_now( EVERYDAY );

	//for (int i = 0; i < 31; i++){
		//init_params(a);
		//a.alpha = i*10.0 * M_PI / 180.0 / 2.0 ;
	tuvp_direct_2d_now(EVERYDAY); //NOSAVE
		//cout << "Step " << i << " completed" << endl;
	//} 

	//mpi_estimate();

	aclock2 = mclock();
	if (myrank == 0) cout << "Total time: " << diffclock(aclock1, aclock2) << "s" << endl << endl;


#if GO_MPI == 1
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Finalize();
#endif

	return 0;
}


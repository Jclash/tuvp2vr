CC = mpic++
MPIINCLUDE=-I/usr/include/mpich
CFLAGS = $(MPIINCLUDE) -fopenmp
LCLAGS = $(CFLAGS)
EXETARGET = tuvp2vr.exe

#
# Video encoder:
#   avconv - for linux
#   ffmpeg.exe - for windows
#
AVICONV = ffmpeg.exe 
AVIRATE = 3
AVIOPT = -framerate 25 -r $(AVIRATE) -i

.PHONY: plots, clean, clobber

main.o : main.cpp stdafx.h
	$(CC) $(CFLAGS) -c main.cpp
	
init.o : init.cpp init.h
	$(CC) $(CFLAGS) -c init.cpp	

run: main.o init.o
	$(CC) $(LCLAGS) main.o init.o -o run

tuvp2vr.exe: ./Release/tuvp2vr.exe 
	rm -f tuvp2vr.exe
	cp ./Release/tuvp2vr.exe ./

solution.txt: $(EXETARGET)
	@echo 
	@echo Running code...
	rm -f step*.txt
	tuvp2vr.exe
	cat ha > solution.txt

plots.png: solution.txt plot_solution.py
	@echo 
	@echo Plotting results...
	rm -f plot*.png
	rm -f centerline*.png
	python plot_solution.py
	cat ha > plots.png

#
# copy from animate target:
#
#python animate_solution.py	
#avconv -i plot_r%d.png solution_r.avi
#avconv -i plot_r_mpi%d.png solution_r_mpi.avi
#avconv -i plocm_I1%d.png solution_I1.avi
#avconv -i plocm_sigma_e%d.png solution_sigma_e.avi
#avconv -f image2 -i plotcm_I1%d.png -vcodec h264 -crf 1 -r 24 out.mov
#avconv -i plotcm_I1%d.png -s solution_I1.avi
#avconv -i plotcm_sigma_e%d.png solution_sigma_e.avi

	
animate: solution_T0.avi solution_I1.avi solution_taup_rr_t.avi solution_sigma_e.avi solution_S.avi solution_T0_t.avi
	@echo 
	@echo Animating results...
#	rm -f *.avi

solution_T0.avi: plots.png	
	rm -f solution_T0.avi	
	$(AVICONV) $(AVIOPT) plotcm_T0%d.png solution_T0.avi
	
solution_I1.avi: plots.png	
	rm -f solution_I1.avi
	$(AVICONV) $(AVIOPT) plotcm_I1%d.png solution_I1.avi

solution_taup_rr_t.avi:	plots.png
	rm -f solution_taup_rr_t.avi
	$(AVICONV) $(AVIOPT) plotcm_taup_rr_t%d.png solution_taup_rr_t.avi

solution_sigma_e.avi: plots.png
	rm -f solution_sigma_e.avi
	$(AVICONV) $(AVIOPT) plotcm_Sigma_e%d.png solution_sigma_e.avi

solution_T0_t.avi: plots.png	
	rm -f solution_T0_t.avi
	$(AVICONV) $(AVIOPT) plotcm_T0_t%d.png solution_T0_t.avi	

solution_S.avi:	plots.png
	rm -f solution_S.avi	
	$(AVICONV) $(AVIOPT) plotcm_S%d.png solution_S.avi	

clean:
	rm -f *.o run

clobber: clean
	rm -f solution.txt *.png

resclean:
	rm -f *.png step*.txt *.avi solution.txt plots Tmax*.txt

imgclean:
	rm -f *.png 	
	
allclean: clean resclean
	rm *~
	


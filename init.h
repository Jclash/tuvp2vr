
struct params{

	int N;
	int M;

	double alpha;
	double betta;
	double R;
	double epsilon;
	double dense;
	double mu;
	double lambda;
	double const_tau;
	double const_ks0;
	double const_T0;
	double const_ce;
	double const_k;
	double const_alpha;
	double xi;


	bool param_nocut;

	double dense_phys;
	double R_phys;
	double mu_phys;
	double lambda_phys;
	double const_ce_phys;
	double const_k_phys;
	double const_alpha_phys;
	double const_tau_phys;
	double xi_phys;
	double const_T0_phys;
	double const_ks0_phys;

	double const_tau0_phys;
	double T_phys;
	double const_Sigma10_phys;

	bool temperature_Newton;
	bool temperature_Dirihle;
	double const_gamma;
	double const_alpha_tilda;

	double L;

	double const_F0;
	double const_tau0;
	double const_r0;
	double const_P0;

	double const_F0_razm;
	double const_tau0_razm;
	double const_r0_razm;
	double const_P0_razm;

	double const_Sigma10;

	double T;

	double tau;

	double *Ur0, *Ur1, *Ur2, *Ut0, *Ut1, *Ut2;

	double *r_i, *thetta_j;

	int sfreq;

	int N2;
	int M2;

	double grho;
	double gksi;

	double eps_der;

};

int init_params( params& );

int init_params( params&, int );

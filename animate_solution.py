import matplotlib 
matplotlib.use('Agg')
from matplotlib import pyplot as plt
from matplotlib import cm               # colormaps

import numpy as np
#import Tkinter
import sys

import Image
import glob

fig = plt.figure()
#ax = fig.add_subplot(111)

def animate():
    filenames=sorted(glob.glob('*.png'))
    im=plt.imshow(Image.open(filenames[0]))
    for filename in filenames[1:]:
        image=Image.open(filename)
        im.set_data(image)
        fig.canvas.draw()
	fig.savefig("fig.jpg") 

animate()

win = fig.canvas.manager.window
fig.canvas.manager.window.after(100, animate)
plt.show()
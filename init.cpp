
#define _USE_MATH_DEFINES
#include <math.h>

#include "init.h"

#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <string>

int init_params(params& par){

	int N = 50;

	par.N = N;
	par.M = N;

	par.sfreq = 11;

	par.alpha = 0 * M_PI/180.0/2.0;
	par.epsilon = 0.005;
	par.L = 1.0;

	if( par.alpha == 0 )
		par.param_nocut = true;
	else
		par.param_nocut = false;
	par.param_nocut = true;

	par.grho = (1.0 - par.epsilon) / par.N;
	par.gksi = (2 * M_PI - 2 * par.alpha) / par.M;

	par.tau = par.grho / 60.0; // *par.grho / 2.0;

	par.betta = M_PI / 6;
	par.const_F0 = 10;
	par.const_r0 = par.grho;

	par.const_tau0_phys = 1.0E-7; // s

	par.dense_phys = 1.7E3; // kg/m^3
	par.R_phys = 3E-3; // m
	par.mu_phys = 1.22E10;// 1.016E8; // Pa
	par.lambda_phys = 1.83E10;// 0.8657E8; // Pa
	par.const_ce_phys = 1.766E3; // Dj/kg*K
	par.const_k_phys = 0.419; // Wt/m*K

	//par.const_alpha_phys = 3.0E-4; // 1/K
	par.const_alpha_phys = 0.0; // 1/K

	par.const_tau_phys = 1.635;// 2.0E-3; // Pa*s
	par.xi_phys = 10; // Wt/m*m*K
	par.const_T0_phys = 303.15; // K
	par.const_ks0_phys = 1.14E9; // Pa

	double vp = sqrt( (par.lambda_phys + 2 * par.mu_phys)/par.dense_phys );
	double vs = sqrt((par.mu_phys) / par.dense_phys);
	par.T_phys = 7.8*par.R_phys / vp;

	double kg = 0.2179E5;
	double m = 1.0/3.0*1000;
	double s = 1.0E6;
	double K = 1.0;
	double Pa = kg/m/s/s;
	double Dj = kg*m*m/s/s;
	double Wt = kg*m*m/s/s/s;

	par.dense = par.dense_phys * kg / m / m / m;
	par.R = par.R_phys * m;

	// real Lame
	par.mu = par.mu_phys * Pa;
	par.lambda = par.lambda_phys * Pa;

	// experimental Lame
	//par.mu = 1.0;
	//par.lambda = -1.0;


	par.const_ce = par.const_ce_phys * Dj / kg / K;
	par.const_k = par.const_k_phys * Wt / m / K;
	par.const_alpha = par.const_alpha_phys / K;
	par.const_tau = par.const_tau_phys * Pa * s;
	par.xi = par.xi_phys * Wt / m*m*K;
	par.const_T0 = par.const_T0_phys * K;
	par.const_ks0 = par.const_ks0_phys * Pa;

	par.const_tau0 = par.const_tau0_phys * s;

	/*
	par.dense = 1.0;
	par.R = 1;
	par.mu = 1.98;
	par.lambda = 1.68;
	par.const_ce = 0.01962;
	par.const_k = 0.001257*0.000007222;
	par.const_alpha = 0.000008;
	par.const_tau = 113; 
	par.xi = 0.00009*0.000007222;
	par.const_T0 = 303.15;
	*/
	
	par.const_P0 = par.const_F0 / 2.0 / par.betta / par.R / par.L;
	//par.const_Sigma10 = par.const_F0 / par.dense / par.L / par.betta / (2 * par.R*par.const_r0 - par.const_r0*par.const_r0);
	par.const_Sigma10 = 0.02;

	par.const_Sigma10_phys = par.const_Sigma10/Pa;

     std::cout << "Pa (perevod) = " << Pa << std::endl;

	par.const_alpha_tilda = par.xi;

	par.const_tau0_razm = par.const_tau0*0.00001;
	par.const_F0_razm = par.const_F0*460.8;
	par.const_r0_razm = par.const_r0 * 3 * 0.001;
	par.const_P0_razm = par.const_P0*0.513 * 100000000;

	par.T = par.T_phys * s;

	par.temperature_Newton = true;
	par.temperature_Dirihle = false;
	par.const_gamma = 0.0;// par.const_alpha_tilda / par.const_k;

	par.eps_der = 0.0;

	//par.r_i = new double[par.N];
	//par.thetta_j = new double[par.M];

	return 0;
}


int init_params_140923_nottrue(params& par){

	int N = 50;

	par.N = N;
	par.M = N;

	par.sfreq = 10;

	par.R = 1;
	par.alpha = M_PI / 4;
	par.epsilon = 0.1;
	par.L = 1.0;

	par.grho = (par.R - par.epsilon) / par.N;
	par.gksi = (2 * M_PI - 2 * par.alpha) / par.M;

	par.tau = par.grho / 30.0; // *par.grho / 2.0;

	par.betta = M_PI / 6;
	par.const_F0 = 10;
	par.const_tau0 = 0.06;
	par.const_r0 = par.grho;

	par.dense = 1.0;
	par.mu = 1.98;// 237.0;// 1.98;
	par.lambda = 1.68;// 356.0;// 1.688;
	par.const_k = 0.001257*0.000007222;
	par.const_alpha_tilda = 0.00009*0.000007222;
	par.const_ce = 0.01962;
	par.const_T0 = 303.15;
	par.const_tau = 113; //0.0001 113;

	par.const_ks0 = 0.3;
	par.const_alpha = 0.000008;// 0.0003;//0.000008;

	par.const_P0 = par.const_F0 / 2.0 / par.betta / par.R / par.L;
	//par.const_Sigma10 = par.const_F0 / par.dense / par.L / par.betta / (2 * par.R*par.const_r0 - par.const_r0*par.const_r0);
	par.const_Sigma10 = 0.1;

	par.const_tau0_razm = par.const_tau0*0.00001;
	par.const_F0_razm = par.const_F0*460.8;
	par.const_r0_razm = par.const_r0 * 3 * 0.001;
	par.const_P0_razm = par.const_P0*0.513 * 100000000;

	par.T = 0.64;

	par.temperature_Newton = true;
	par.temperature_Dirihle = false;
	par.const_gamma = par.const_alpha_tilda / par.const_k;

	par.eps_der = 0.0;

	//par.r_i = new double[par.N];
	//par.thetta_j = new double[par.M];

	return 0;
}



int init_params(params& par, int N){
	return init_params(par);
}
